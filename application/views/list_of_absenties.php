<?php 
require_once BASEPATH . '/helpers/url_helper.php'; 
$this->load->view('theme/header');
$this->load->view('theme/sidebar');
$getParam="";
if (isset($_GET['to']) && !isset($_GET['from'])) {
   $getParam="to=".$_GET['to'];
}
else if (isset($_GET['from']) && isset($_GET['to'])) {
   $getParam="to=".$_GET['to']."&from=".$_GET['from'];
}


?>
      <!-- START Main section-->
      <section>
         <!-- START Page content-->
         <div class="main-content">
            <!-- START row-->
            <div class="row">
               <div class="col-md-12">
                     <!-- START panel-->
                     	<select name="account" class="form-control m-b" onchange="location = this.value;">
                        <option value="">Select</option>
                        <option  value="<?php echo base_url('EmployeeLeaveStatus'); ?>">Employee Leaves Status</option>
                        <option selected value="<?php echo base_url('ListOfAbsenties'); ?>">List Of Absentees</option>
                        <option  value="<?php echo base_url('EmployeeOnDuty'); ?>">List Of On Duty Employees</option>
                      </select>
                      <div class="panel panel-default">
                     <div class="panel-heading form-heading" >List Of Absentees</div>
                     <div class="panel-body" style="overflow-x: scroll;">
                     <form>
                          <div class="col-md-12">
                           <div class="form-group col-md-4 bgc-5">
                              <p class="text-center">LINE OF ACTIVITY</p>
                              <select name="account" class="form-control m-b" id="activity_id" onchange="location = this.value;">
                                 <option value="">Select</option>
                                 <option value="<?php echo base_url('EmployeeLeaveStatus/All'); ?>" <?php if ($_SERVER['REQUEST_URI']=="/hrpayroll/EmployeeLeaveStatus/All") { $getParam="type=All"; ?> selected <?php } ?> >All</option>
                                 <?php if (isset($activities)) {
                                    foreach ($activities as $activity) { ?>
                                       <option value="<?php  echo base_url('EmployeeLeaveStatus/Activity?type='.$activity["dept_name"]); ?>" <?php if (isset($_GET['type']) && $_GET['type']==$activity['dept_name']) { $getParam="type=".$_GET['type']; ?> selected <?php } ?> > <?php echo $activity['dept_name'] ?></option>
                                  <?php   } } ?>
                              </select>
                           </div>
                            <div class="form-group col-md-4 bgc-4">
                              <p style="margin: 0px;">
                              </p><div class="col-md-12 p-0">
                              <p class="text-center">Search BY B ID</p>
                              <select name="account" class="form-control m-b" id="activity_id1" onchange="location = this.value;" >
                                 <option value="">Select</option>
                                 <option value="<?php echo  base_url('EmployeeLeaveStatus/All'); ?>" <?php if ($_SERVER['REQUEST_URI']=="/hrpayroll/EmployeeLeaveStatus/All") { $getParam="type=All"; ?> selected <?php } ?> >All</option>
                                 <?php if (isset($bids)) {
                                    foreach ($bids as $bid) { ?>
                                       <option value="<?php echo base_url('EmployeeLeaveStatus/Activity?bid='.$bid['emp_bid']); ?>" <?php if (isset($_GET['bid']) && $_GET['bid']==$bid['emp_bid']) { $getParam="bid=".$_GET['emp_bid']; ?> selected <?php } ?>><?php echo $bid['emp_bid']; ?></option>
                                   <?php  }
                                 } ?>
                              </select>
                              </div>
                          </div>
                          <div class="form-group col-md-4 bgc-2">
                               <div class="col-md-12 p-0">
                              <p class="text-center">BY E.Name</p>

                              <select name="account" class="form-control m-b" id="activity_id3" onchange="location = this.value;"> 
                                 <option value="">Select</option>
                                 <option value="<?php echo base_url('EmployeeLeaveStatus/All'); ?>" <?php if ($_SERVER['REQUEST_URI']=="/hrpayroll/EmployeeLeaveStatus/All") { $getParam="type=All"; ?> selected <?php } ?> >All</option>
                                 <?php if (isset($names)) {
                                    foreach ($names as $name) { ?>
                                       <option value="<?php echo base_url('EmployeeLeaveStatus/Activity?ename='.$name['emp_name']); ?>" <?php if (isset($_GET['ename']) && $_GET['ename']==$name['emp_name']) { $getParam="ename=".$_GET['ename']; ?> selected <?php } ?>><?php echo $name['emp_name']; ?></option>
                                   <?php  }
                                 } ?>
                              </select>
                              </div>
                              <p></p>
                           </div>
                           <!-- <div class="form-group col-md-2 bgc-2">
                              <p class="text-center">LINE OF ACTIVITY</p>
                              <select name="account" class="form-control m-b">
                                 <option> All</option>
                              </select>
                           </div> -->
                   
                           </div>
                        </form>

                        <!-- START table-responsive-->
                     <div class="table-responsive">
                       <table class="table  table-bordered table-hover">
                        <thead>
                          <tr>
                           <th>SL.NO</th>
                           <th>EID</th> 
                            <th>BID</th>
                            <th>E.Name</th>
                            <th>Department</th>
                            <th>Designation</th>
                            <th>Leave Type</th>
                            <th>Total</th>
                            <th>Used</th>
                            <!-- <?php if ($leave_type) {
                              foreach ($leave_type as $leaves) { ?> 
                              <th><?php echo $leaves['leave_type'];?></th>
                            <?php } }?> -->
                         </tr>
                        </thead>
                        <tbody class="mytable"> 
                         
                         <?php if (isset($no_of_leaves)) {
                           $i=1;
                           foreach ($no_of_leaves as $leaves1) { 
                           ?>
                        <tr>
                           <td><?php echo $i++; ?></td>
                           <td><?php echo $leaves1['emp_id'] ?></td>
                           <td><?php echo $leaves1['emp_bid'] ?></td>
                           <td><?php echo $leaves1['emp_name'] ?></td>
                           <td><?php echo $leaves1['dept_name'] ?></td>
                           <td><?php echo $leaves1['designation_name'] ?></td>
                           <td><?php echo $leaves1['leave_type'] ?></td>
                           <td><?php echo $leaves1['no_leaves'] ?></td>
                           <td><?php echo $leaves1['used_count'] ?></td>
                           <!-- <?php if ($leave_type) {
                            	foreach ($leave_type as $leaves) { 
                            		if ($leaves['leave_type']==$leaves1['leave_type']) { ?>
                            			<td><?php echo $leaves1['used_count']."/".$leaves1['no_leaves'];?></td>
                            		<?php } else{
                            			echo "<td>0</td>";
                            		}
                            		} }?> -->
                           	
                         </tr>
                        <?php }} else{ ?>
                         <tr>
                            
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                         </tr>

                         <tr>
                           <td></td>
                           <td></td>
                           <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            
                         </tr>
                      <?php } ?>
                        </tbody>
                  </table>
                        </div>
                        <!-- END table-responsive-->
                        <!-- <div class="col-md-12 btn-sec text-center">
                  <ul class="btn-row"> -->
                     <!-- <li><a href="" class="btn-form"> Print </a></li> -->
                    <!--  <li><button class="btn-form btn-undo" onclick="exportToExcel();"> Export to Excel </button></li> -->
                     <!-- <li><a href="" class="btn-form btn-exit"> EXIT </a></li> -->
                  <!-- </ul>
                  </div> -->
                     </div>
                  </div>
               </div>
            </div>
            <!-- END row-->
         </div>
         <!-- END Page content-->
         <!-- START Page footer-->
         <footer class="text-center">&copy; 2018 - HR Payroll</footer>
         <!-- END Page Footer-->
      </section>
      <!-- END Main section-->
   </div>
   <!-- END Main wrapper-->
   <!-- START Scripts-->
   <!-- Main vendor Scripts-->
   <script src="<?php echo base_url();?>vendor/jquery/jquery.min.js"></script>
   <script src="<?php echo base_url();?>vendor/bootstrap/js/bootstrap.min.js"></script>
   <!-- Plugins-->
   <script src="<?php echo base_url();?>vendor/chosen/chosen.jquery.min.js"></script>
   <script src="<?php echo base_url();?>vendor/slider/js/bootstrap-slider.js"></script>
   <script src="<?php echo base_url();?>vendor/filestyle/bootstrap-filestyle.min.js"></script>
   <!-- Animo-->
   <script src="<?php echo base_url();?>vendor/animo/animo.min.js"></script>
   <!-- Sparklines-->
   <script src="<?php echo base_url();?>vendor/sparklines/jquery.sparkline.min.js"></script>
   <!-- Slimscroll-->
   <script src="<?php echo base_url();?>vendor/slimscroll/jquery.slimscroll.min.js"></script>
   <!-- Store + JSON-->
   <script src="<?php echo base_url();?>vendor/store/store%2bjson2.min.js"></script>
   <!-- Classyloader-->
   <script src="<?php echo base_url();?>vendor/classyloader/js/jquery.classyloader.min.js"></script>
   <!-- START Page Custom Script-->
   <!-- Form Validation-->
   <script src="<?php echo base_url();?>vendor/parsley/parsley.min.js"></script>
   <!-- END Page Custom Script-->
   <!-- App Main-->
   <script src="<?php echo base_url();?>app/js/app.js"></script>
   <!-- END Scripts-->
   <script type="text/javascript" >
      var base_url="<?php echo base_url(); ?>";
      $("#end_date").on("change",function(){
         var start_date= $("#start_date").val();
         var end_date= $("#end_date").val();
         if (start_date!="") {
            window.location.replace(base_url+"EmployeeLeaveStatus/activity?from="+start_date+"&to="+end_date);
         }
         else{
            window.location.href=base_url+"EmployeeLeaveStatus/activity?to="+end_date;
         }
         
      })
      var employee_data=<?php if( isset($employees) && count($employees)){ echo json_encode($employees); } else{echo "{}"; } ?>;
      function exportToExcel() {
         window.location.href=base_url+"Excel_export/employeesData?"+"<?php echo $getParam; ?>";
         /*$.ajax({
         type: 'post',
           url: base_url+"Excel_export/action",
           data: {employees_data:employee_data},
           success: function (data) {
            console.log(data);
            },
          error:function(error){
              console.log(error);
              $("#login_error_id").html(error);
          }
       });*/
      }
      $("#master_ul li").removeClass();
      $("#assin_ul li").removeClass();
      $("#empm").addClass("active");

      /*var $rows = $('#mytable tr');

        $('#activity_id').on("change",function() {
          console.log($(this).val());
            var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();

            $rows.show().filter(function() {
                var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
                return !~text.indexOf(val);
            }).hide();


        });*/
   </script>
    <script>
$(document).ready(function(){
  $("#activity_id").on("change", function() {
    var value = $(this).val().toLowerCase();
    if (value=='all') {
      window.location.reload();
    }
    $(".mytable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
  $("#activity_id1").on("change", function() {
    var value = $(this).val().toLowerCase();
    if (value=='all') {
      window.location.reload();
    }
      $(".mytable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
  $("#activity_id3").on("change", function() {
    var value = $(this).val().toLowerCase();
    if (value=='all') {
      window.location.reload();
    }
      $(".mytable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
  
});
      $("#reports_ul").addClass("nav collapse in");
      $("#els_li").addClass("active");
</script> 
</body>


</html>