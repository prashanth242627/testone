<?php 
require_once BASEPATH . '/helpers/url_helper.php'; 
$getParam="";
if (isset($_GET['to']) && !isset($_GET['from'])) {
   $getParam="to=".$_GET['to'];
}
else if (isset($_GET['from']) && isset($_GET['to'])) {
   $getParam="to=".$_GET['to']."&from=".$_GET['from'];
}
if (isset($_GET['type']) && !isset($_GET['payment_mode'])) {
   $getParam="type=".$_GET['type'];
}
else if (isset($_GET['type']) && isset($_GET['payment_mode'])) {
   $getParam="type=".$_GET['type']."&payment_mode=".$_GET['payment_mode'];
}

?>

      <!-- START Main section-->
      <section>
         <!-- START Page content-->
         <div class="main-content">
            <!-- START row-->
            <div class="row">
               <div class="col-md-12">
                     <!-- START panel-->

                      <div class="panel panel-default">
                     <div class="panel-heading form-heading">Employee Salary Details </div>
                     <div class="panel-body" style="overflow-x: scroll;">
                     <form>
                          <div class="col-md-12">
                           <div class="form-group col-md-3 bgc-2">
                              <p class="text-center">Department</p>
                              <select name="account" class="form-control m-b" id="line_of_activity_id">
                                 <option value="">Select</option>
                                 <option value="All" <?php if ($_SERVER['REQUEST_URI']=="/applications/hrpayroll/Salaries/All") { $getParam="type=All"; ?> selected <?php } ?> >All</option>
                                 <?php if (isset($activities)) {
                                    foreach ($activities as $activity) { ?>
                                       <option value="<?php echo $activity["dept_name"]; ?>" <?php if (isset($_GET['type']) && $_GET['type']==$activity['dept_name']) { $getParam="type=".$_GET['type']; ?> selected <?php } ?> > <?php echo $activity['dept_name'] ?></option>
                                  <?php   } } ?>
                              </select>
                           </div>
                           <div class="form-group col-md-3 bgc-3">
                              <p class="text-center">Payment Mode</p>
                              <select name="account" class="form-control m-b" id="payment_mode_id">
                                 <option value="">Select</option>
                                 <option value="All" <?php if ($_SERVER['REQUEST_URI']=="/applications/hrpayroll/Salaries/All") { $getParam="type=All"; ?> selected <?php } ?> >All</option>
                                 <?php if (isset($activities)) {
                                    foreach ($payment_modes as $payment_mode) { ?>
                                       <option value="<?php echo $payment_mode["payment_type"]; ?>" <?php if (isset($_GET['payment_mode']) && $_GET['payment_mode']==$payment_mode['payment_type']) { $getParam="payment_mode=".$_GET['payment_mode']; ?> selected <?php } ?> > <?php echo $payment_mode['payment_type'] ?></option>
                                  <?php   } } ?>
                              </select>
                           </div>
                            <div class="form-group col-md-3 bgc-4">
                          <div class="col-md-12 p-0">
                              <p class="text-center">Search BY B ID</p>
                              <select name="account" class="form-control m-b" onchange="location = this.value;">
                                 <option value="">Select</option>
                                 <option value="<?php echo base_url('Salaries/All'); ?>" <?php if ($_SERVER['REQUEST_URI']=="/applications/hrpayroll/Salaries/All") { $getParam="type=All"; ?> selected <?php } ?> >All</option>
                                 <?php if (isset($bids)) {
                                    foreach ($bids as $bid) { ?>
                                       <option value="<?php echo base_url('Salaries/Activity?bid='.$bid['emp_bid']); ?>" <?php if (isset($_GET['bid']) && $_GET['bid']==$bid['emp_bid']) { $getParam="bid=".$_GET['bid']; ?> selected <?php } ?>><?php echo $bid['emp_bid']; ?></option>
                                   <?php  }
                                 } ?>
                              </select>
                              </div>
                            </div>
                            <div class="form-group col-md-3 bgc-5">
                               <div class="col-md-12 p-0">
                              <p class="text-center">BY E.Name</p>

                              <select name="account" class="form-control m-b" onchange="location = this.value;"> 
                                 <option value="">Select</option>
                                 <option value="<?php echo base_url('Salaries/All'); ?>" <?php if ($_SERVER['REQUEST_URI']=="/applications/hrpayroll/Salaries/All") { $getParam="type=All"; ?> selected <?php } ?> >All</option>
                                 <?php if (isset($names)) {
                                    foreach ($names as $name) { ?>
                                       <option value="<?php echo base_url('Salaries/Activity?ename='.$name['emp_name']); ?>" <?php if (isset($_GET['ename']) && $_GET['ename']==$name['emp_name']) { $getParam="ename=".$_GET['ename']; ?> selected <?php } ?>><?php echo $name['emp_name']; ?></option>
                                   <?php  }
                                 } ?>
                              </select>
                              </div>
                              <p></p>
                           </div>
                   
                           </div>
                        </form>

                        <!-- START table-responsive-->
                     <div class="table-responsive fixtable">
                       <table class="table  table-bordered table-hover" id="fixTable">
                        <tbody class="mytable"> 
                         <tr>
                            <td>S.No</td>
                            <td>E ID</td>
                            <td>E BID</td>
                            <td>E Name</td>
                            <td>D.O.J</td>
                            <td>Designation</td>
                            <td>Department</td>
                            <td>Bank A/c Number</td>
                            <td>Payment Mode</td>
                            <td>UAN</td>
                            <td>PF Number</td>
                            <td>ESI Number</td>
                            <td>Basic</td>
                            <td>INCR</td>
                            <td>D.A</td>
                            <td>H.R.A</td>
                            <td>C.C.A</td>
                            <td>Other allowances</td>
                            <td>Head/HOD/Coordinator allowances</td>
                            <td>Special Allowances</td>
                            <td>Bonus</td>
                            <td>PF Deducted</td>
                            <td>Professional Tax</td>
                            <td>Gross Pay</td>
                            <td>Net Pay</td>
                            <td>Status</td>
                         </tr>
                         <?php if (isset($employees)) {
                           $i=1;
                           foreach ($employees as $employee) { 
                           ?>
                        <tr>
                           <td><?php echo $i++; ?></td>
                           <td><?php echo $employee['emp_id'] ?></td>
                           <td><?php echo $employee['emp_bid'] ?></td>
                           <td><?php echo $employee['emp_name'] ?></td>
                           <td><?php echo $employee['date_of_join'] ?></td>
                           <td><?php echo $employee['designation'] ?></td>
                           <td><?php echo $employee['dept_name'] ?></td>
                           <td><?php echo $employee['acc_no'] ?></td>
                           <td><?php echo $employee['payment_type'] ?></td>
                           <td><?php echo $employee['uan'] ?></td>
                            <td><?php echo $employee['pf_no'] ?></td>
                            <td><?php echo $employee['esi_no'] ?></td>
                            <td><?php echo $employee['basic'] ?></td>
                            <td><?php echo $employee['incr'] ?></td>
                            <td><?php echo $employee['da'] ?></td>
                            <td><?php echo $employee['hra'] ?></td>
                            <td><?php echo $employee['cca'] ?></td>
                            <td><?php echo $employee['other_allowance'] ?></td>
                            <td><?php echo $employee['co_allowance'] ?></td>
                            <td><?php echo $employee['special_allowance'] ?></td>
                            <td><?php echo $employee['bonus'] ?></td>
                            <td><?php echo $employee['pf_consider'] ?></td>
                            <td>Professional Tax</td>
                            <td>Gross Pay</td>
                            <td>Net Pay</td>
                           <?php if (isset($employee['flag'])) { ?>
                              <td>InActive</td>
                           <?php }else{ ?>
                           <td>Active</td> <?php } ?>
                         </tr>
                        <?php }} else{ ?>
                         <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                         </tr>

                         <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                         </tr>
                      <?php } ?>
                        </tbody>
                  </table>
                        </div>
                        <!-- END table-responsive-->
                        <div class="col-md-12 btn-sec text-center">
                  <ul class="btn-row">
                     <!-- <li><a href="" class="btn-form"> Print </a></li> -->
                     <li><button class="btn-form btn-undo" onclick="exportToExcel();"> Export to Excel </button></li>
                     <!-- <li><a href="" class="btn-form btn-exit"> EXIT </a></li> -->
                  </ul>
                  </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- END row-->
         </div>

   <script type="text/javascript" >
      var base_url="<?php echo base_url(); ?>";
      $("#payment_mode_id").on("change",function(){
      var line_of_activity= $("#line_of_activity_id").val();
      var payment_mode= $("#payment_mode_id").val();
      if (line_of_activity=="All" && payment_mode!="All") {
        window.location.replace(base_url+"Salaries/All");
      }
      else if (line_of_activity=="All" && payment_mode!="") {
        window.location.href=base_url+"Salaries/activity?status="+status;
      }
      else if (line_of_activity!="") {
        window.location.replace(base_url+"Salaries/activity?type="+line_of_activity+"&payment_mode="+payment_mode);
      }
      else{
        window.location.href=base_url+"Salaries/activity?status="+status;
      }
         
      })
      $("#line_of_activity_id").on("change",function(){
        var line_of_activity= $("#line_of_activity_id").val();
        if (line_of_activity=="All") {
          window.location.replace(base_url+"Salaries/All");
        }
        else{
          window.location.href=base_url+"Salaries/activity?type="+line_of_activity;
        }       
      })
      $("#end_date").on("change",function(){
         var start_date= $("#start_date").val();
         var end_date= $("#end_date").val();
         if (start_date!="") {
            window.location.replace(base_url+"Salaries/activity?from="+start_date+"&to="+end_date);
         }
         else{
            window.location.href=base_url+"Salaries/activity?to="+end_date;
         }
         
      })
      function exportToExcel() {
         window.location.href=base_url+"Excel_export/salaryDetails?"+"<?php echo $getParam; ?>";
         /*$.ajax({
         type: 'post',
           url: base_url+"Excel_export/action",
           data: {employees_data:employee_data},
           success: function (data) {
            console.log(data);
            },
          error:function(error){
              console.log(error);
              $("#login_error_id").html(error);
          }
       });*/
      }
      $("#reports_ul").addClass("nv collapse in");
    $("#sald_li").addClass("active");

   </script>
