<?php 
require_once BASEPATH . '/helpers/url_helper.php'; 
$getParam="";
if (isset($_GET['month']) && isset($_GET['year'])) {
   
   if (strpos($_SERVER['REQUEST_URI'], "/applications/hrpayroll/ProbationPeriod/All") !==false) {  
    $getParam="type=All&month=".$_GET['month']."&year=".$_GET['year'];
    }
    else if (isset($_GET['type'])) {
      $getParam="type=".$_GET['type']."&month=".$_GET['month']."&year=".$_GET['year'];
    }
}
?>
      <!-- START Main section-->
      <section>
         <!-- START Page content-->
         <div class="main-content">
            <!-- START row-->

            <div class="row">
               <div class="col-md-12">
                     <!-- START panel-->
                    
                      <div class="panel panel-default">
                     <div class="panel-heading form-heading" >Probation Period Details</div>
                     <div class="panel-body" style="overflow-x: scroll;">                       

                        <!-- START table-responsive-->
                     <div class="table-responsive">
                       <table class="table  table-bordered table-hover">
                        <thead>
                          <tr >
                            <th>S.No</th>
                            <th>E ID</th>
                            <th>E BID</th>
                            <th>E Name</th>
                            <th>Designation</th>
                            <th>Department</th>
                            <th>D.O.J</th>
                            <th>Probation date</th>
                            <th>Regularize</th>                            
                         </tr>
                        </thead>
                        <tbody class="mytable"> 
                         
                         <?php if (isset($employees)) {
                           $i=1;
                           foreach ($employees as $employee) { 
                            $new_date = strtotime('+ 1 year', strtotime($employee['date_of_join']));
                            $probation_date= date('Y-m-d', $new_date);
                           ?>
                        <tr>
                           <td><?php echo $i++; ?></td>
                           <td><?php echo $employee['temp_emp_id'] ?></td>
                           <td><?php echo $employee['emp_bid'] ?></td>
                           <td><?php echo $employee['emp_name'] ?></td>
                           <td><?php echo $employee['designation_name'] ?></td>
                           <td><?php echo $employee['dept_name'] ?></td>
                           <td><?php echo $employee['date_of_join'] ?></td>
                           <td><?php echo $probation_date ?></td>
                           <td><a style="color: red" href="<?php echo base_url('ProbationPeriod/Regularize/'.$employee['emp_bid']) ?>">Regularize</a></td>
                           	
                         </tr>
                        <?php 
                        $i++;
                      }} else{ ?>
                         <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                         </tr>

                         <tr>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                            <td></td>
                         </tr>
                      <?php } ?>
                        </tbody>
                  </table>
                        </div>
                        <!-- END table-responsive-->
                        <div class="col-md-12 btn-sec text-center">
                  <ul class="btn-row">
                     <!-- <li><a href="" class="btn-form"> Print </a></li> -->
                     <li style="display: none;"><button class="btn-form btn-undo" onclick="exportToExcel();"> Export to Excel </button></li>
                     <!-- <li><a href="" class="btn-form btn-exit"> EXIT </a></li> -->
                  </ul>
                  </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- END row-->
         </div>

   <script type="text/javascript">
      var base_url="<?php echo base_url(); ?>";
      $("#employee_count").val(<?php echo $i-1; ?>);
      $("#line_of_activity_id").on("change",function(){
        $("#month_id").val("");
        $("#year_id").val("");
      });
      $("#year_id").on("change",function(){
        $("#month_id").val("");
      });
      $("#month_id").on("change",function(){
        var year=$("#year_id").val();
        var month=$("#month_id").val();
        var dept=$("#line_of_activity_id").val();

        if (year!="" && month!="") {
          if (dept=="All") {
            window.location.replace(base_url+"DepartmentSalaries/All?month="+month+"&year="+year);
          }
          else if (dept!="") {
            window.location.replace(base_url+"DepartmentSalaries/activity?type="+dept+"&month="+month+"&year="+year);
          }
        }
      });
      
      var employee_data=<?php if( isset($employees) && count($employees)){ echo json_encode($employees); } else{echo "{}"; } ?>;
      function exportToExcel() {
         window.location.href=base_url+"Excel_export/DepartmentSalaries?"+"<?php echo $getParam; ?>";
      }
      $("#assin_ul").addClass("nav collapse in");
      $("#pd_li").addClass("active");
</script> 