 <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
   <!-- START Main section-->
      <section>
         <!-- START Page content-->
         <div class="main-content">
            <!-- START row-->
            <div class="row">
               <div class="col-md-8 col-md-offset-2">
                  <form action="#"  novalidate="" class="form-horizontal" method="post">
                     <!-- START panel-->
                      <div class="panel panel-default">
                     <div class="panel-heading form-heading">EMPLOYEE LINE OF ACTIVITY ENTRY SCREEN</div>
                     <div class="panel-body">
                        <!-- START table-responsive-->
                        <div class="table-responsive">
                           <table id="gradeX" class="table  table-bordered table-hover">
                        <thead>
                           <tr>
							  <th>S.No</th>
							  <th>&nbsp;</th>
                              <th>Dept</th>
                              <th>ABBR</th>
                           </tr>
                        </thead>
                        <tbody class="mytable">
                           
                        </tbody>
                  </table>
                  <div class="col-md-12 btn-sec">
                  <div class="btn-row">
						<input type="submit" class="btn-form btn-delete" value="SUBMIT"> 
						<button class="btn-form btn-edit" id="add"> ADD </button>
						<button class="btn-form" id="save">save</button>
						<button class="btn-form btn-undo"> UNDO </button>
						<button class="btn-form btn-edit"> EDIT </button>
						<button class="btn-form btn-delete delete"> DELETE </button>
						<button class="btn-form btn-exit"> EXIT </button>
                  </div>
                  </div>
                        </div>
                        <!-- END table-responsive-->
					<div>
					<?php
					$lineofactivitydata="";
					$i=1;
					foreach($get_lineofactivity as $row)
					{
						$lineofactivitydata .= "<tr><td>".$i++."</td><td><input type='checkbox' name='list[]' /></td><td><input type='text' name='dept_name[]'  value='".$row->dept_name."' placeholder='".$row->dept_name."'></td><td><input type='text' name='dept_id[]' placeholder='".$row->dept_id."' value='".$row->dept_id."'></td></tr>"; 
					}
					?>
					
					</div>
                     </div>
                  </div>
                     <!-- END panel-->
                  </form>
               </div>
            </div>
            <!-- END row-->
         </div>
         <!-- END Page content-->
		 
<script>
	var rw= <?php echo $i; ?>;
	$( document ).ready(function() {
		var tabledata = "<?php echo $lineofactivitydata; ?>";
		$('table#gradeX>tbody').append(tabledata);
		addTable(1);
		$("#add").on('click',function(){
			addTable(1);
		});	
		$("#gradeX").on('click', '.delete', function () {
			$(this).closest('tr').remove();
		});
	});
	function addTable(num)
	{
		var a=0;
		for(a=0;a<num;a++){
			
			$('table#gradeX>tbody').append("<tr><td>"+rw+"</td><td><input type='checkbox' name='list[]' /></td><td><input type='text' value='' name='dept_name[]' placeholder='ADMIN'></td><td><input type='text' placeholder='ADM' value='' name='dept_id[]'></td></tr>");
		rw++;
		}
	}
	
	</script>