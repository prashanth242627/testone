 <?php
 
 $emp_bid = ($getAttendanceMarkingEdit->emp_bid)?$getAttendanceMarkingEdit->emp_bid:'';
 $holiday_id = ($getAttendanceMarkingEdit->holiday_id)?$getAttendanceMarkingEdit->holiday_id:'';
  $leave_type_id = ($getAttendanceMarkingEdit->leave_type_id)?$getAttendanceMarkingEdit->leave_type_id:'';
 $leave_date = ($getAttendanceMarkingEdit->leave_date)?$getAttendanceMarkingEdit->leave_date:'';
 $no_leaves = ($getAttendanceMarkingEdit->no_leaves)?$getAttendanceMarkingEdit->no_leaves:'';
?>
   <!-- START Main section-->
      <section>
         <!-- START Page content-->
         <div class="main-content">

            <!-- START row-->
            <div class="row">
                  <form action="" onsubmit="return false"  id="attendance_marking_form"  novalidate="" class="form-horizontal" method="post">
                     <!-- START panel-->
                      <div class="panel panel-default">
                     <div class="panel-heading form-heading"><?php echo $emp_bid; ?>Leave Form</div>
                     <div class="panel-body bgc-1">
                        <!-- START table-responsive-->
						
	                         <div class="form-group col-md-3">
	                         <div class="col-md-12 p-0">
	                              <p class="text-center">Leave Date From</p>
								 <input  class="form-control" style="line-height: 20px;" required type="date" name="leave_date_from" value="<?php echo date('Y-m-d') ?>" >
                              </div>
                          	</div>
	                         <div class="form-group col-md-3">
	                         <div class="col-md-12 p-0">
	                              <p class="text-center">Leave Date To</p>
								 <input  class="form-control" style="line-height: 20px;" required  type="date" name="leave_date_to" value="<?php echo date('Y-m-d') ?>" >
                              </div>
                          	</div>
                          	<div class="form-group col-md-3">
								<div class="col-md-12 p-0">
	                              <p class="text-center">Employee BID</p>
                                <select name="emp_bid" class='form-control' id="emp_bid" required>
                                	<option value="">Select</option>
								<?php foreach($get_employees as $row) {  ?>
								   
								 <option value='<?php echo $row['emp_bid']; ?>' <?php echo (($row['emp_bid'] == $emp_bid)?"selected":"");?>> <?php echo $row['emp_bid']; ?></option>
								<?php } ?>
								</select>
                              </div>
							</div>							  
							  <div class="form-group col-md-2">
								<div class="col-md-12 p-0">
	                              <p class="text-center">Employee Name</p>
                                <select name="emp_name" class='form-control' id="emp_name" required>
                                	<option value="">Select</option>
								<?php foreach($names as $row) {  ?>
								   
								 <option value='<?php echo $row['emp_name']; ?>' <?php echo (($row['emp_name'] == $emp_name)?"selected":"");?>> <?php echo $row['emp_name']; ?></option>
								<?php } ?>
								</select>
                              </div>
							</div>
                           <div class="form-group col-md-2">
							   <p style="margin: 0px;">
	                              </p><div class="col-md-12 p-0">
	                              <p class="text-center">Leave Type</p>
                                 <select name="leave_type_id" id="leave_type_id" class='form-control' required>
								</select>
                              </div>
                           </div>
                       </div>
                   </div>
                       </div>
                       <div class="row">
							<div class="form-group col-md-12">
								<div class="table-responsive">
									<table id="gradeX"  class="table  table-bordered table-hover" >
					                    <thead>
					                       <tr >
											  <th colspan="3" style="text-align: center;" >CL</th>
											  <th colspan="3" style="text-align: center;" >EL</th>
											  <th colspan="3" style="text-align: center;" >SL</th>
					                       </tr>
					                    </thead>
					                    <tbody>
					                       	<tr class="custom_centered" >
											  	<td>OPB</td>
					                          	<td>AVAILED</td>
											 	<td>CLB</td>
											  	<td>OPB</td>
					                          	<td>AVAILED</td>
											 	<td>CLB</td>
											  	<td>OPB</td>
					                          	<td>AVAILED</td>
											 	<td>CLB</td>
					                       </tr>
					                       <tr class="custom_centered" >
											  	<td id="cl_opb">0</td>
					                          	<td id="cl_used">0</td>
											 	<td id="cl_clb">0</td>
											  	<td id="el_opb">0</td>
											  	<td id="el_used">0</td>
											  	<td id="el_clb">0</td>
											  	<td id="sl_opb">0</td>
											  	<td id="sl_used">0</td>
											  	<td id="sl_clb">0</td>
					                       </tr>
					                    </tbody>
					                </table>
								</div>
							</div>
						</div>
						<div id="error_id"></div>
					   <div class="row">
						  <div class="col-md-offset-5 btn-sec center">

						  <div class="btn-row">
								<input type="submit" class="btn-form btn-edit" value="Generate Leave Form"> 
								<input type="reset" class="btn-form btn-delete" value="CANCEL"> 
						  </div>
						  </div>
					   </div>
                        <!-- END table-responsive-->
                     </div>
                  </div>
                     <!-- END panel-->
                </form>
            </div>

		 
<script>
	$("#emp_bid").change(function(){ 
		$("#emp_name").val("");
	});
	$("#emp_name").change(function(){ 
		$("#emp_bid").val("");
	});
$(document).ready(function(){
	var val = $("#emp_bid").val();
    $("#emp_bid").change(function(){ 
		$.ajax({
			url: '<?php echo base_url();?>LeaveForm/getEmployeeDetails',
			data: {emp_bid:$(this).val()},
			method:"POST",
			success:function(data){
				console.log(data);
				var obj=jQuery.parseJSON(data);
				$('#emp_name').val(obj.emp_name);
				$('#activity_id').val(obj.dept_name); 
				$('#sub_name').val(obj.sub_name); 
				$('#leave_type_id').html(obj.get_emp_leavemaster);
				$('#emp_id').val(obj.emp_id); 
				$("#cl_opb").html(obj.cl.opb);
				$("#cl_used").html(obj.cl.used);
				$("#cl_clb").html(obj.cl.clb);

				$("#sl_clb").html(obj.sl.clb);
				$("#sl_used").html(obj.sl.used);
				$("#sl_opb").html(obj.sl.opb);

				$("#el_clb").html(obj.el.clb);
				$("#el_used").html(obj.el.used);
				$("#el_opb").html(obj.el.opb);

				$("#total_leaves_id").html("");
				$("#total_leaves_id").html(obj.total_leaves_id);
				$("#remaining_leaves_id").html("");
				$("#remaining_leaves_id").html(obj.remaining_leaves_id);
			},
			error:function(error){
				console.log(error);
			}
		});
    });
    $("#emp_name").change(function(){ 
		$.ajax({
			url: '<?php echo base_url();?>LeaveForm/getEmployeeDetails',
			data: {emp_name:$(this).val()},
			method:"POST",
			success:function(data){
				console.log(data);
				var obj=jQuery.parseJSON(data);
				$('#emp_name').val(obj.emp_name);
				$("#emp_bid").val(obj.emp_bid);
				$('#activity_id').val(obj.dept_name); 
				$('#sub_name').val(obj.sub_name); 
				$('#leave_type_id').html(obj.get_emp_leavemaster);
				$('#emp_id').val(obj.emp_id); 
				$("#cl_opb").html(obj.cl.opb);
				$("#cl_used").html(obj.cl.used);
				$("#cl_clb").html(obj.cl.clb);

				$("#sl_clb").html(obj.sl.clb);
				$("#sl_used").html(obj.sl.used);
				$("#sl_opb").html(obj.sl.opb);

				$("#el_clb").html(obj.el.clb);
				$("#el_used").html(obj.el.used);
				$("#el_opb").html(obj.el.opb);

				$("#total_leaves_id").html("");
				$("#total_leaves_id").html(obj.total_leaves_id);
				$("#remaining_leaves_id").html("");
				$("#remaining_leaves_id").html(obj.remaining_leaves_id);
			},
			error:function(error){
				console.log(error);
			}
		});
    });
	$("#emp_bid").trigger('change');
	$("#attendance_marking_form").submit(function() {
	 	$("#error_id").html("");
	 	var emp_bid=$("#emp_bid").val();
	 	if (emp_bid!="") {
	 		$.ajax({
		 		url: '<?php echo base_url();?>LeaveForm/CheckAttendaceMarking',
		 		data : new FormData(this),
		 		method: "POST",
		 		processData: false,
				contentType: false,
		 		success : function(data) {
		 			console.log(data);
		 			var obj=jQuery.parseJSON(data);
		 			if (obj.status) {
		 				window.location.href="<?php echo base_url();?>GeneratePdf/LeaveFormPdf?"+obj.getparam;
		 				//window.location.reload();
		 			}
		 			else{
		 				$("#error_id").html(obj.msg);
		 			}
		 			
		 		},
		 		error:function (error) {
		 			console.log(error);	
		 		}
		 	})
	 	}
	 	
	 })
});
$("#assin_ul").addClass("nav collapse in");
$("#lf_li").addClass("active");
</script>
<style type="text/css">
	#attendence_form .form-group {

    	margin-left: -10px;
    	margin-right: -10px;
	}
	tr.custom_centered th,td{
		text-align: center;
	}

</style>
<style>
.wrapper:after {
    content:'';
    display:block;
    clear: both;
}
#gradeX td, #gradeX th {
      border: 1px solid #ddd;
    padding: 8px;
    font-weight: 600;
    color: #656565;
}
#gradeX tr:nth-child(even){background-color: #f2f2f2;}

#gradeX tr:hover {background-color: #ddd;}

#gradeX th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color:#8590e5;
  color: white;
}
</style>