 <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
 <?php
 
 $emp_bid = ($getAttendanceMarkingEdit->emp_bid)?$getAttendanceMarkingEdit->emp_bid:'';
 $holiday_id = ($getAttendanceMarkingEdit->holiday_id)?$getAttendanceMarkingEdit->holiday_id:'';
  $leave_type_id = ($getAttendanceMarkingEdit->leave_type_id)?$getAttendanceMarkingEdit->leave_type_id:'';
 $leave_date = ($getAttendanceMarkingEdit->leave_date)?$getAttendanceMarkingEdit->leave_date:'';
 $no_leaves = ($getAttendanceMarkingEdit->no_leaves)?$getAttendanceMarkingEdit->no_leaves:'';
 
 $empattendance_markings="";
$i=1;
foreach($get_attendance_marking as $row)
{
	$empattendance_markings .= "<tr><td>".$i++."</td><td>".$row->emp_bid."</td><td>".$row->emp_name."</td><td>".$row->dept_name."</td><td>".$row->leave_type."</td><td>".$row->no_leaves."</td><td><p><a href='".base_url()."attendance_marking/edit/".base64_encode($row->leave_master_id)."' class='btn-form btn-edit'> EDIT </a>&nbsp;<a href='".base_url()."attendance_marking/delete/".base64_encode($row->leave_master_id)."' class='btn-form btn-delete'> DELETE </a>&nbsp;<a href='".base_url()."attendance_marking/undo/".base64_encode($row->leave_master_id)."' class='btn-form btn-undo'> UNDO </a></p></td></tr>"; 
}
foreach($get_attendance_marking_log as $row)
{
	$empattendance_markings .= "<tr><td>".$i++."</td><td>".$row->emp_bid."</td><td>".$row->emp_name."</td><td>".$row->dept_name."</td><td>".$row->leave_type."</td><td>".$row->no_leaves."</td><td>&nbsp;</td></tr>"; 
}
?>
   <!-- START Main section-->
      <section>
         <!-- START Page content-->
         <div class="main-content">

            <!-- START row-->
            <div class="row">
                  <form action="" onsubmit="return false" id="attendance_marking_form"  novalidate="" class="form-horizontal" method="post">
                     <!-- START panel-->
                      <div class="panel panel-default">
                     <div class="panel-heading form-heading"><?php echo $emp_bid; ?>Attendance Marking/ <a href="<?php echo base_url();?>BioMetricBulkUpload"  target="_blank" class="txt-wht"> Bio Metric Reports Upload </a></div>
                     <div class="panel-body bgc-1">
                        <!-- START table-responsive-->
                        <div class="row col-md-4 ">
						   <div class="form-group">
	                              <label class="col-lg-6 control-label p-0">Leave From Date</label>
	                              <div class="col-lg-6 p-0 pl-6">
	                                 <input  class="form-control" style="line-height: 20px;"  type="date" name="leave_date_from" value="<?php echo date('Y-m-d') ?>" >
	                              </div>
	                         </div>
                         </div>
                         <div class="row col-md-4">
						   <div class="form-group">
	                              <label class="col-lg-6 control-label p-0">Leave To Date</label>
	                              <div class="col-lg-6 p-0 pl-6">
	                                 <input  class="form-control" style="line-height: 20px;"  type="date" name="leave_date_to" value="<?php echo date('Y-m-d') ?>" >
	                              </div>
	                         </div>
                         </div>
						<div class="row" id="attendence_form">
							<div class="col-md-12" >
							<div class="form-group col-md-2">
								<p style="margin: 0px;">
	                              </p><div class="col-md-12 p-0">
	                              <p class="text-center">Employee BID</p>
                                <select name="emp_bid" class='form-control' id="emp_bid" required >
                                	<option value="">Select</option>
								<?php foreach($get_employees as $row) {  ?>
								   
								 <option value='<?php echo $row['emp_bid']; ?>' <?php echo (($row['emp_bid'] == $emp_bid)?"selected":"");?>> <?php echo $row['emp_bid']; ?></option>
								<?php } ?>
								</select>
                              </div>
							</div>							  
							  <div class="form-group col-md-2">
							   <p style="margin: 0px;">
	                              </p><div class="col-md-12 p-0">
	                              <p class="text-center">Employee Name</p>
                                 <input type='text' class="form-control" name="emp_name" id="emp_name" readonly />
								 <input type='hidden' class="form-control" name="emp_id" id="emp_id" readonly />
                              </div>
                           </div>
						   <div class="form-group  col-md-2">
							   <p style="margin: 0px;">
	                              </p><div class="col-md-12 p-0">
	                              <p class="text-center">Line Of Activity</p>
                                 <input type='text' class="form-control" name="activity_id" id="activity_id" readonly />
                              </div>
                           </div>
						  <div class="form-group col-md-2" >
							   <p style="margin: 0px;">
	                              </p><div class="col-md-12 p-0">
	                              <p class="text-center">Teaching Subject</p>
                                 <input type='text' class="form-control" name="sub_name" id="sub_name" readonly />
                              </div>
                           </div>
						   <div class="form-group col-md-2">
							   <p style="margin: 0px;">
	                              </p><div class="col-md-12 p-0">
	                              <p class="text-center">Holiday</p>
                                 <select name="holiday_id" class='form-control'>
								 <option>No Holiday</option>
								<?php foreach($get_holidays as $row) {  ?>
								   
								 <option value='<?php echo $row->holiday_id; ?>' <?php echo (($row->holiday_id == $holiday_id)?"selected":"");?>> <?php echo $row->holiday_name; ?></option>
								<?php } ?>
								</select>
                              </div>
                           </div>
                           <div class="form-group col-md-3">
							   <p style="margin: 0px;">
	                              </p><div class="col-md-12 p-0">
	                              <p class="text-center">Leave Type(OPB/USED/CLB)</p>
                                 <select name="leave_type_id" id="leave_type_id" class='form-control' required="">
								</select>
                              </div>
                           </div>
					    	<!-- <div class="form-group col-md-2">
							   <p style="margin: 0px;"></p>
							   <div class="col-md-12 p-0">
	                              <p class="text-center">No Leaves</p>
								 <input type='text' class="form-control" name="no_leaves" placeholder='No Leaves' />
                              	</div>
                           	</div> -->
                       </div>
                       </div>
                       <div class="row">
							<div class="form-group col-md-12">
								<div class="table-responsive">
									<table id="gradeX"  class="table  table-bordered table-hover" >
					                    <thead>
					                       <tr class="custom_centered" >
											  <th colspan="3">CL</th>
											  <th colspan="3">EL</th>
											  <th colspan="3">SL</th>
					                       </tr>
					                    </thead>
					                    <tbody>
					                       	<tr class="custom_centered" >
											  	<td>OPB</td>
					                          	<td>USED</td>
											 	<td>CLB</td>
											  	<td>OPB</td>
					                          	<td>USED</td>
											 	<td>CLB</td>
											  	<td>OPB</td>
					                          	<td>USED</td>
											 	<td>CLB</td>
					                       </tr>
					                       <tr class="custom_centered" >
											  	<td id="cl_opb">0</td>
					                          	<td id="cl_used">0</td>
											 	<td id="cl_clb">0</td>
											  	<td id="el_opb">0</td>
											  	<td id="el_used">0</td>
											  	<td id="el_clb">0</td>
											  	<td id="sl_opb">0</td>
											  	<td id="sl_used">0</td>
											  	<td id="sl_clb">0</td>
					                       </tr>
					                    </tbody>
					                </table>
								</div>
							</div>
						</div>
						<div id="error_id"></div>
					   <div class="row">
						  <div class="col-md-offset-5 btn-sec center">

						  <div class="btn-row">
								<input type="submit" class="btn-form btn-edit" value="SUBMIT"> 
								<input type="reset" class="btn-form btn-delete" value="CANCEL"> 
						  </div>
						  </div>
					   </div>
                        <!-- END table-responsive-->
                     </div>
                  </div>
                     <!-- END panel-->
                </form>
            </div>
            <!-- END row-->
            <div class="row">
				<div class="panel panel-default">
					<div class="panel-body " style="background: #edffd2">
						<h4 class="text-center">Summary Leave Report</h4>
					
                            <div class="row p-0" style="margin:20px 0px">
	                         

                         <div class="row col-md-4">
						   <div class="form-group">
	                              <label class="col-lg-6 control-label p-0">From Date</label>
	                              <div class="col-lg-6 p-0 pl-6">
	                                 <input type='date' id="summary_date_from" class="form-control col-lg-4" style="line-height: 20px;" name="summary_date_from" value="<?php echo date('Y-m-d') ?>" >
	                              </div>
	                         </div>
                         </div>
                         <div class="row col-md-4 ">
						   <div class="form-group">
	                              <label class="col-lg-6 control-label p-0">To Date</label>
	                              <div class="col-lg-6 p-0 pl-6">
	                                 <input type='date' id="summary_date_to" class="form-control col-lg-4" style="line-height: 20px;" name="summary_date_to" value="<?php echo date('Y-m-d') ?>" >
	                              </div>
	                         </div>
                         </div>
                         </div>

						<div class="row">
							<div class="form-group col-md-12">
								<div class="table-responsive">
									<?php if (count($get_leave_types)) { ?>
									<table id="gradeX"  class="table  table-bordered table-hover">
					                    <thead>
					                       <tr class="custom_centered">
											  <?php foreach ($get_leave_types as $get_leave_type) {
											  	echo "<th>".$get_leave_type->leave_type."</th>";
											  } ?>
					                       </tr>
					                    </thead>
					                    <tbody>
					                       <tr class="custom_centered" id="leaves_count_summary_id">
					                       	 <?php echo $get_current_date_leaves_string; ?> 
											  <!-- <?php
											  	$total_leaves=0;
											   foreach ($get_leave_types as $get_leave_type) {
											   	if (count($get_current_date_leaves)) {
												  	foreach ($get_current_date_leaves as $get_current_date_leave) {
												  		if ($get_leave_type->leave_type===$get_current_date_leave->leave_type) {
												  			$total_leaves+=$get_current_date_leave->leaves_count;
												  			//echo "<td>".$get_current_date_leave->leaves_count."</td>";
												  		}
												  		else{
												  			//echo "<td>"."0"."</td>";
												  		}
												  	}
											  	}
											  	else{
											  		//echo "<td>"."0"."</td>";
											  	}
											  } 
											  ?> -->
					                       </tr>
					                    </tbody>
					                </table>
					            <?php } ?>
								</div>
							</div>
						</div>
						<h5><b>View Details</b></h5>
						<div class="row">
							<div class="form-group col-md-12">
								<?php if(!empty($empattendance_markings)) {  ?>
								<div class="table-responsive">
					                <table id="gradeX" class="table  table-bordered table-hover">
					                    <thead>
					                       <tr>
											  <th>S.No</th>
					                          <th>Employee BID</th>
											  <th>Employee Name</th>
											  <th>Employee Id</th>
											  <th>Line of Activity</th>
											  <th>Teaching Subject</th>
											  <th>Leave date</th>
											  <th>Leave Type</th>
					                       </tr>
					                    </thead>
					                    <tbody class="mytable" id="leaved_employees_data_id">
					                       
											  <?php
											  if (count($get_current_date_leave_emps)>0) {
											  	$l=1;
											  	foreach ($get_current_date_leave_emps as $get_current_date_leave_emp) {
											  		echo "<tr><td>".$l++."</td><td>".$get_current_date_leave_emp->emp_bid."</td><td>".$get_current_date_leave_emp->emp_name."</td><td>".$get_current_date_leave_emp->emp_id."</td><td>".$get_current_date_leave_emp->dept_name."</td><td>".$get_current_date_leave_emp->sub_name."</td><td>".$get_current_date_leave_emp->leave_date."</td><td>".$get_current_date_leave_emp->leave_type."</td></tr>";
											  	}
											  }
											  else{
											  	echo "<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr><tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
											  }
											   ?>
					                       
					                    </tbody>
					                </table>
					            </div>
								<?php } ?>
							</div>
						</div>
         				</div>
         			</div>
         		</div>
         	</div>
         <!-- END Page content-->
     

		 
<script>
$(document).ready(function(){
	var val = $("#emp_bid").val();
    $("#emp_bid").change(function(){ 
		$.ajax({
			url: '<?php echo base_url();?>attendance_marking/getEmployeeDetails',
			data: {emp_bid:$(this).val()},
			method:"POST",
			success:function(data){
				console.log(data);
				var obj=jQuery.parseJSON(data);
				$('#emp_name').val(obj.emp_name);
				$('#activity_id').val(obj.dept_name); 
				$('#sub_name').val(obj.sub_name); 
				$('#leave_type_id').html(obj.get_emp_leavemaster);
				$('#emp_id').val(obj.emp_id); 
				$("#cl_opb").html(obj.cl.opb);
				$("#cl_used").html(obj.cl.used);
				$("#cl_clb").html(obj.cl.clb);

				$("#sl_clb").html(obj.sl.clb);
				$("#sl_used").html(obj.sl.used);
				$("#sl_opb").html(obj.sl.opb);

				$("#el_clb").html(obj.el.clb);
				$("#el_used").html(obj.el.used);
				$("#el_opb").html(obj.el.opb);

				$("#total_leaves_id").html("");
				$("#total_leaves_id").html(obj.total_leaves_id);
				$("#remaining_leaves_id").html("");
				$("#remaining_leaves_id").html(obj.remaining_leaves_id);
			},
			error:function(error){
				console.log(error);
			}
		});
    });
	$("#emp_bid").trigger('change');
	$("#summary_date_from").on("change",function(){
        $("#summary_date_to").val("");
      });
	$("#summary_date_to").on("change",function(){
        var summary_date_to=$("#summary_date_to").val();
        var summary_date_from=$("#summary_date_from").val();
        console.log(summary_date_from>summary_date_to);
        if (summary_date_to!="") {
        	if (summary_date_from!="" && summary_date_to!="" && (summary_date_to==summary_date_from || summary_date_from<summary_date_to)) {
	        	$.ajax({
					url: '<?php echo base_url();?>attendance_marking/SummaryDetails',
					data: {summary_date_from:summary_date_from,summary_date_to:summary_date_to},
					method:"POST",
					success:function(data){
						console.log(data);
						var obj=jQuery.parseJSON(data);
						$("#leaved_employees_data_id").html("");
						$("#leaved_employees_data_id").html(obj.get_current_date_leaves);
						$("#leaves_count_summary_id").html("");
						$("#leaves_count_summary_id").html(obj.get_current_date_leave_emps);

					},
					error:function (error) {
						console.log(error);
					}
				});
	        }
	        else{
	        	alert('Please Check the Given Dates');
	        }
        
        }
        
      });
	 //$("#summary_date").trigger('change');
	 $("#attendance_marking_form").submit(function() {
	 	$("#error_id").html("");
	 	var emp_bid=$("#emp_bid").val();
	 	if (emp_bid!="") {
	 		$.ajax({
		 		url: '<?php echo base_url();?>attendance_marking/AddAttendaceMarking',
		 		data : new FormData(this),
		 		method: "POST",
		 		processData: false,
				contentType: false,
		 		success : function(data) {
		 			console.log(data);
		 			var obj=jQuery.parseJSON(data);
		 			if (obj.status) {
		 				window.location.reload();
		 			}
		 			else{
		 				$("#error_id").html(obj.msg);
		 			}
		 			
		 		}
		 	})
	 	}
	 	
	 })
});
$("#assin_ul").addClass("nav collapse in");
$("#atten_li").addClass("active");
</script>
<style type="text/css">
	#attendence_form .form-group {

    	margin-left: -10px;
    	margin-right: -10px;
	}
	tr.custom_centered th,td{
		text-align: center;
	}

</style>