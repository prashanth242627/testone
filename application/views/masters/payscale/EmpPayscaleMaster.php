<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
<section>
   <!-- START Page content-->
   <div class="main-content">
      <!-- START row-->
      <div class="row">
         <div class="col-md-12 ">
            
               <!-- START panel-->
                <div class="panel panel-default">
               <div class="panel-heading form-heading">EMPLOYEE PAY SCALE ENTRY SCREEN</div>
                <div class="panel-body">
				<div class="row">
					    <form class="form-horizontal" action="<?php echo base_url()."EmpPayScale/EmppayscalBulkupload"; ?>" method="post" enctype="multipart/form-data">
						<div class="form-group col-md-2"> </div>
						<div class="form-group col-md-10"> 
					         <label class="col-lg-6 control-label">Bulk Upload</label>
                              <div class="col-lg-6">
                               <span><input type="file" name="emp_bluk" class="fleft">
                                <input type="submit" name="employeedd" value="submit" class="btn-form submit-btnn btn-edit"></span>
							   </div>
						</div>
						</form>
					   </div>
                  <form action="<?php echo base_url(); ?>EmpPayScale/add"  novalidate=""  method="POST" name="payscaleform" class="form-horizontal">
                  <div class="col-md-12 form-sub input-padding" style="background: #f5f8fa">
                  <div class="row">
                     <div class="form-group col-md-4">
                        <label class="col-lg-4 p-0 control-label">E BID</label>
                        <div class="col-lg-8">
                           <select id="ebid" name="ebid" class="form-control">
                              <option>Select Emp Bid</option>
                              <?php if(!empty($get_employee)){
                              foreach ($get_employee as $value) {
                                 ?>
                              <option value="<?php echo $value->emp_bid;  ?>"><?php echo $value->emp_bid;  ?></option>
                              <?php } } ?>
                           </select>
                           
                        </div>
                     </div>
                      <div class="form-group col-md-4">
                        <label class="col-lg-4 p-0 control-label">E Name</label>
                        <div class="col-lg-8">
                           <input type="text" placeholder="Auto-Reflect (Corresponding e bid)" class="form-control" id="ename" name="ename">
                        </div>
                     </div>
                      <div class="form-group col-md-4">
                        <label class="col-lg-4 p-0 control-label">Mode of Payment</label>
                        <div class="col-lg-8">
                           <select id="pmt_type" name="pmt_type" class="form-control">
                              <option>Select Mode of Payment</option><?php if(!empty($payTypes)){
                              foreach ($payTypes as $payType) {
                                 ?>
                              <option value="<?php echo $payType->payment_type_id;  ?>"><?php echo $payType->payment_type;  ?></option>
                              <?php } } ?>
                           </select>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="form-group col-md-4">
                        <label class="col-lg-4 p-0 control-label">Bank A/c Number</label>
                        <div class="col-lg-8">
                           <input type="text" id="accno" name="accno" placeholder="No (15c)" class="form-control">
                        </div>
                     </div>

                     <div class="form-group col-md-4">
                        <label class="col-lg-4 p-0 control-label">UAN</label>
                        <div class="col-lg-8">
                           <input type="text" name="uan" id="uan" placeholder="No (15c)" class="form-control">
                        </div>
                     </div>
                     <div class="form-group col-md-4">
                        <label class="col-lg-4 p-0 control-label">PF Number</label>
                        <div class="col-lg-8">
                           <input type="text" name="pf" id="pf" placeholder="Number (5c)" class="form-control">
                        </div>
                     </div>
                     </div>
                     <div class="row">
                     <div class="form-group col-md-4">
                        <label class="col-lg-4 p-0 control-label">ESI Number</label>
                        <div class="col-lg-8">
                           <input type="text" name="esi" id="esi" placeholder="No (20c)" class="form-control">
                        </div>
                     </div>
                     </div>
                  </div>
                  <!-- ======================= end-section1================= -->
                   <div class="col-md-12 form-sub input-padding" style="background: #f5f8fa">
                   <h4>Pay scale assignment</h4><br>
                   <div class="row">
                     <div class="form-group col-md-4">
                        <label class="col-lg-4 p-0 control-label">Basic*</label>
                        <div class="col-lg-8">
                           <input type="text" name="basic" id="basic" placeholder="No (6c)" class="form-control input-imp">
                        </div>
                     </div>
                     <div class="form-group col-md-4">
                        <label class="col-lg-4 p-0 control-label">Increment*</label>
                        <div class="col-lg-8">
                           <input type="text" name="incr" id="incr" placeholder="No (6c)" class="form-control input-imp">
                        </div>
                     </div>
                     <div class="form-group col-md-4">
                        <label class="col-lg-4 p-0 control-label">D.A*</label>
                        <div class="col-lg-8">
                           <input type="text" name="da" id="da" placeholder="No (6c)" class="form-control input-imp">
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="form-group col-md-4">
                        <label class="col-lg-4 p-0 control-label">H.R.A*</label>
                        <div class="col-lg-8">
                           <input type="text" name="hra" id="hra" placeholder="No (6c)" class="form-control input-imp">
                        </div>
                     </div>
                     <div class="form-group col-md-4">
                        <label class="col-lg-4 p-0 control-label">C.C.A*</label>
                        <div class="col-lg-8">
                           <input type="text" name="cca" id="cca" placeholder="No (6c)" class="form-control input-imp">
                        </div>
                     </div>
                     <div class="form-group col-md-4">
                        <label class="col-lg-4 p-0 control-label">Other allowances*</label>
                        <div class="col-lg-8">
                           <input type="text" name="oa" id="oa" placeholder="No (6c)" class="form-control input-imp">
                        </div>
                     </div>
                     
                  </div>
                  <div class="row">
                     <div class="form-group col-md-4">
                        <label class="col-lg-4 p-0 control-label">Head/HOD/Coordinator allowances*</label>
                        <div class="col-lg-8">
                           <input type="text" name="co_allow" id="co_allow" placeholder="No (6c)" class="form-control input-imp">
                        </div>
                     </div>
                     
                     <div class="form-group col-md-4">
                        <label class="col-lg-4 p-0 control-label">Special allowances*</label>
                        <div class="col-lg-8">
                           <input type="text" name="spa" id="spa" placeholder="No (6c)" class="form-control input-imp">
                        </div>
                     </div>
                  </div>
                     
                     <div class="form-group col-md-4" style="display: none;">
                        <label class="col-lg-4 p-0 control-label">Bonus</label>
                        <div class="col-lg-8">
                           <input type="text" name="bonus" id="bonus" placeholder="No (6c)" class="form-control">
                        </div>
                     </div>
                     
                     
                  </div>
                <!--  ====================end row======================= -->
                <div class="col-md-12 form-sub" style="background: #f5f8fa">
                  <h4>Special assignments</h4><br>
                  <div class="row">
                      <div class="form-group col-md-6">
                        <label class="col-lg-4 p-0 control-label">Standard Installments</label>
                        <div class="col-lg-8">
                           <input type="text" name="standard_instalmemt" id="standard_instalmemt" class="form-control">
                        </div>
                     </div>
                      <div class="form-group col-md-6">
                        <label class="col-lg-4 p-0 control-label">Installments Paid</label>
                        <div class="col-lg-8">
                           <input type="text" name="inst_paid" id="inst_paid"  class="form-control">
                        </div>
                     </div>
                     </div>
                  <div class="row">
                     <div class="form-group col-md-3">
                       <div class="checkbox c-checkbox pull-left">
                     <label>
                     <input type="checkbox" name="esi_consider" id="esi_consider">
                     <span class="fa fa-check"></span>ESI to consider</label>
                     </div>
                     </div>
                     <div class="form-group col-md-3">
                        <div class="checkbox c-checkbox pull-left">
                     <label>
                     <input type="checkbox" name="is_pay_hold" id="is_pay_hold" >
                     <span class="fa fa-check"></span>Is Payment to hold</label>
                     </div>
                     </div>
                     <div class="form-group col-md-3">
                     <div class="checkbox c-checkbox pull-left">
                     <label>
                     <input type="checkbox" name="sdscope" id="sdscope" >
                     <span class="fa fa-check"></span>10% s.d Scope</label>
                     </div>
                     </div>
                     <div class="form-group col-md-3">
                     <div class="checkbox c-checkbox pull-left">
                     <label>
                     <input type="checkbox" name="pf_consider" id="pf_consider" >
                     <span class="fa fa-check"></span>PF to consider</label>
                     </div>
                     </div>
                     </div>

                      <div class=" text-center" style="text-align: center; margin:0px auto">
                           <input type="submit" name="employeedd"  class="btn-form btn-edit" value="save"> 
                           <input type="button"  name="undo" class="btn-form btn-undo" id="undo" value="UNDO"> 
                  </div>
                  </div>    
                  

                  </form>
    
               </div>
            </div>
               <!-- END panel-->
            </form>
         </div>
      </div>
      <!-- END row-->
   </div>
   <!-- END Page content-->
   <!-- START Page footer-->
   <footer class="text-center">&copy; 2018 - HR Payroll</footer>
   <!-- END Page Footer-->
</section>
<!-- END Main section-->
   <script type="text/javascript">
      $("#ebid").on("change",function(){
         
      $.ajax({  
      type: "POST",  
      url:"<?php echo base_url();?>EmpPayScale/getEmpData",  
      data: {empbid: $("#ebid").val()},  
      
      success:function(data){
         //console.log(data);
          var dispdata = $.parseJSON(data);
			$("#ename").val(dispdata.emp_name);
			$("#accno").val(dispdata.acc_no);
			$("#uan").val(dispdata.uan);
			$("#pf").val(dispdata.pf_no);
			$("#esi").val(dispdata.esi_no);
			$("#basic").val(dispdata.basic);
			$("#oa").val(dispdata.other_allowance);
			$("#incr").val(dispdata.incr);
			$("#co_allow").val(dispdata.co_allowance);
			$("#da").val(dispdata.da);
			$("#spa").val(dispdata.special_allowance);
			$("#hra").val(dispdata.hra);
			$("#bonus").val(dispdata.hra);
			$("#cca").val(dispdata.cca);
			$("#inst_paid").val(dispdata.inst_paid);
			$("#standard_instalmemt").val(dispdata.standard_instalmemt);
			if(dispdata.esi_consider ==1)
			{
				$('#esi_consider').prop('checked', true);
			}
			if(dispdata.is_pay_hold ==1)
			{
				$('#is_pay_hold').prop('checked', true);
			}
			if(dispdata.sdscope ==1)
			{
				$('#sdscope').prop('checked', true);
			}
			if(dispdata.pf_consider ==1)
			{
				$('#pf_consider').prop('checked', true);
			}

         //$("#ename").val(dispdata.emp_name);

         }  
      });    
      })
      $("#master_ul").addClass("nav collapse in");
      $("#psm").addClass("active");
   </script>