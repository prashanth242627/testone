 <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
 <?php

?>
   <!-- START Main section-->
      <section>
         <!-- START Page content-->
         <div class="main-content">

            <!-- START row-->
            <div class="row">
                  <form action="#" onsubmit="return confirm('Please Enter CLs,ODs and LOPs Before Calculate of BIO LOPs');"  novalidate="" class="form-horizontal" method="post">
                     <!-- START panel-->
                      <div class="panel panel-default">
                     <div class="panel-heading form-heading"><?php echo $emp_bid; ?>Employee LOP Calculation</div>
                     <div class="panel-body bgc-1">
                        <!-- START table-responsive-->
                        <div class="row col-md-5 col-md-offset-2">
						   <div class="form-group">
	                              <label class="col-lg-4 control-label p-0">Year</label>
	                              <div class="col-lg-4 p-0 pl-6">
	                                 <input  class="form-control" style="line-height: 20px;" id="year_id"  type="number" min="2019" max="2099" step="1" value="2019" name="year"  >
	                              </div>
	                         </div>
                         </div>
                         <div class="row col-md-5 ">
						   <div class="form-group">
	                              <label class="col-lg-4 control-label p-0">Month</label>
	                              <div class="col-lg-4 p-0 pl-6">
	                                 <select name="month" class="form-control m-b" id="month_id" required> 
		                                 <option value="">Select</option>
		                                 <option value="January" <?php if (isset($_GET['month']) && $_GET['month']=="January") {  ?> selected <?php } ?>>January</option>
		                                 <option value="February" <?php if (isset($_GET['month']) && $_GET['month']=="February") {  ?> selected <?php } ?>>February</option>
		                                 <option value="March" <?php if (isset($_GET['month']) && $_GET['month']=="March") {  ?> selected <?php } ?>>March</option>
		                                 <option value="April" <?php if (isset($_GET['month']) && $_GET['month']=="April") {  ?> selected <?php } ?>>April</option>
		                                 <option value="May" <?php if (isset($_GET['month']) && $_GET['month']=="May") {  ?> selected <?php } ?>>May</option>
		                                 <option value="June" <?php if (isset($_GET['month']) && $_GET['month']=="June") {  ?> selected <?php } ?>>June</option>
		                                 <option value="July" <?php if (isset($_GET['month']) && $_GET['month']=="July") {  ?> selected <?php } ?>>July</option>
		                                 <option value="August" <?php if (isset($_GET['month']) && $_GET['month']=="August") {  ?> selected <?php } ?>>August</option>
		                                 <option value="September" <?php if (isset($_GET['month']) && $_GET['month']=="September") {  ?> selected <?php } ?>>September</option>
		                                 <option value="October" <?php if (isset($_GET['month']) && $_GET['month']=="October") {  ?> selected <?php } ?>>October</option>
		                                 <option value="November" <?php if (isset($_GET['month']) && $_GET['month']=="November") {  ?> selected <?php } ?>>November</option>
		                                 <option value="December" <?php if (isset($_GET['month']) && $_GET['month']=="December") {  ?> selected <?php } ?>>December</option>
		                                 } ?>
		                              </select>
	                              </div>
	                         </div>
                         </div>

					   <div class="row">

						  <div class="col-md-offset-5 btn-sec center">
						  <div class="btn-row">
								<input type="submit" class="btn-form btn-edit" value="SUBMIT"> 
								<input type="reset" class="btn-form btn-delete" value="CANCEL"> 
						  </div>
						  </div>
					   </div>
                        <!-- END table-responsive-->
                     </div>
                  </div>
                     <!-- END panel-->
                  </form>
            </div>
            <div class="row">
				<div class="panel panel-default">
					<div class="panel-body " style="background: #edffd2">
						<h4 class="text-center">Employee LOP Summary</h4>
						<div class="panel-body ">
                        <!-- START table-responsive-->
                        <div class="row col-md-5 col-md-offset-2">
						   <div class="form-group">
	                              <label class="col-lg-4 control-label p-0">Year</label>
	                              <div class="col-lg-4 p-0 pl-6">
	                                 <input  class="form-control" style="line-height: 20px;" id="year_id1" type="number" min="2019" max="2099" step="1" value="2019" name="year"  >
	                              </div>
	                         </div>
                         </div>
                         <div class="row col-md-5 ">
						   <div class="form-group">
	                              <label class="col-lg-4 control-label p-0">Month</label>
	                              <div class="col-lg-4 p-0 pl-6">
	                                 <select name="month" class="form-control m-b" id="month_id1" required> 
		                                 <option value="">Select</option>
		                                 <option value="January" <?php if (isset($_GET['month']) && $_GET['month']=="January") {  ?> selected <?php } ?>>January</option>
		                                 <option value="February" <?php if (isset($_GET['month']) && $_GET['month']=="February") {  ?> selected <?php } ?>>February</option>
		                                 <option value="March" <?php if (isset($_GET['month']) && $_GET['month']=="March") {  ?> selected <?php } ?>>March</option>
		                                 <option value="April" <?php if (isset($_GET['month']) && $_GET['month']=="April") {  ?> selected <?php } ?>>April</option>
		                                 <option value="May" <?php if (isset($_GET['month']) && $_GET['month']=="May") {  ?> selected <?php } ?>>May</option>
		                                 <option value="June" <?php if (isset($_GET['month']) && $_GET['month']=="June") {  ?> selected <?php } ?>>June</option>
		                                 <option value="July" <?php if (isset($_GET['month']) && $_GET['month']=="July") {  ?> selected <?php } ?>>July</option>
		                                 <option value="August" <?php if (isset($_GET['month']) && $_GET['month']=="August") {  ?> selected <?php } ?>>August</option>
		                                 <option value="September" <?php if (isset($_GET['month']) && $_GET['month']=="September") {  ?> selected <?php } ?>>September</option>
		                                 <option value="October" <?php if (isset($_GET['month']) && $_GET['month']=="October") {  ?> selected <?php } ?>>October</option>
		                                 <option value="November" <?php if (isset($_GET['month']) && $_GET['month']=="November") {  ?> selected <?php } ?>>November</option>
		                                 <option value="December" <?php if (isset($_GET['month']) && $_GET['month']=="December") {  ?> selected <?php } ?>>December</option>
		                                 } ?>
		                              </select>
	                              </div>
	                         </div>
                         </div>
                        <!-- END table-responsive-->
                     </div>
						<div class="row">
							<div class="form-group col-md-12">
								<div class="table-responsive">
									<table id="gradeX"  class="table  table-bordered table-hover">
					                    <thead>
					                       <tr class="custom_centered">
											  <th>SL.NO</th>
											  <th>EMP ID</th>
											  <th>EMP BID</th>
											  <th>EMP Name</th>
											  <th>Department</th>
											  <th>Designation</th>
											  <th>Total Lops</th>
											  <th>CLs Adjusted</th>
											  <th>LOP Count</th>
											  <th>LOP Month</th>
											  <th>LOP Year</th>
					                       </tr>
					                    </thead>
					                    <tbody>
					                    	<?php if (isset($lops_data) && count($lops_data)>0) { 
					                    		$i=1;
					                    		foreach ($lops_data as $lop_data) { ?>
					                    			<tr  >
													 <td><?php echo $i++; ?></td>
													 <td><?php echo $lop_data['temp_emp_id']; ?></td>
													 <td><?php echo $lop_data['emp_bid']; ?></td>
													 <td><?php echo $lop_data['emp_name']; ?></td>
													 <td><?php echo $lop_data['dept_name']; ?></td>
													 <td><?php echo $lop_data['designation_name']; ?></td>
													 <td><?php echo $lop_data['total_lop_count']; ?></td>
													 <td><?php echo $lop_data['bio_cls_adjusted']; ?></td>
													 <td><?php echo $lop_data['lop_count']; ?></td>
													 <td><?php echo date("F", strtotime("2001-" . $lop_data['lop_month'] . "-01")); ?></td>
													 <td><?php echo $lop_data['lop_year']; ?></td>
							                       </tr>
					                    		
					                    	<?php } } else{ ?>
					                       <tr class="custom_centered" >
											 <td></td>
											 <td></td>
											 <td></td>
											 <td></td>
											 <td></td>
											 <td></td>
											 <td></td>
											 <td></td>
											 <td></td>
					                       </tr>
					                       <tr class="custom_centered" >
											 <td></td>
											 <td></td>
											 <td></td>
											 <td></td>
											 <td></td>
											 <td></td>
											 <td></td>
											 <td></td>
											 <td></td>
					                       </tr>
					                   <?php } ?>
					                    </tbody>
					                </table>
								</div>
							</div>
						</div>
         				</div>
         			</div>
         		</div>
         	</div>
         <!-- END Page content-->
		 
<script>
$("#assin_ul").addClass("nav collapse in");
$("#lop_li").addClass("active");
var base_url="<?php echo base_url(); ?>";
		$("#month_id1").on("change",function(){
         var year= $("#year_id1").val();
         var month= $("#month_id1").val();
         window.location.replace(base_url+"EmployeeLOPCalculation/activity?year="+year+"&month="+month);         
      });
</script>
<style type="text/css">
	#attendence_form .form-group {

    	margin-left: -10px;
    	margin-right: -10px;
	}
	tr.custom_centered th,td{
		text-align: center;
	}

</style>