 <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
 <?php
 $emp_name = ($getEmployees->emp_name)?$getEmployees->emp_name:'';
 $emp_bid = ($getEmployees->emp_name)?$getEmployees->emp_bid:'';
 $dob = ($getEmployees->date_of_birth)?$getEmployees->date_of_birth:"";
 $temp_emp_id = ($getEmployees->temp_emp_id)?$getEmployees->temp_emp_id:'';
 $education = ($getEmployees->education)?$getEmployees->education:'';
 $experience = ($getEmployees->experience)?$getEmployees->experience:"";
$doj = ($getEmployees->date_of_join)?$getEmployees->date_of_join:'';
$date = ($getEmployees->date)?$getEmployees->date:"";
$mobile = ($getEmployees->mobile)?$getEmployees->mobile:"";
$activity_id = ($getEmployees->activity_id)?$getEmployees->activity_id:"";
$subject_details_id = ($getEmployees->subject_details_id)?$getEmployees->subject_details_id:"";
$residence_details_id = ($getEmployees->residence_details_id)?$getEmployees->residence_details_id:"";
$transport_details_id = ($getEmployees->transport_details_id)?$getEmployees->transport_details_id:"";
$email = ($getEmployees->email)?$getEmployees->email:'';
$sex = ($getEmployees->sex)?$getEmployees->sex:'';
$alternate_no = ($getEmployees->alternate_no)?$getEmployees->alternate_no:'';
$reportingslots_id = ($getEmployees->from_time)?$getEmployees->from_time:'';
$emergency_no = ($getEmployees->emergency_no)?$getEmployees->emergency_no:'';
$designation = ($getEmployees->designation)?$getEmployees->designation:'';
$address = ($getEmployees->address)?$getEmployees->address:'';
$spouse_name = ($getEmployees->spouse_name)?$getEmployees->spouse_name:'';
$spouse_phoneno = ($getEmployees->spouse_phoneno)?$getEmployees->spouse_phoneno:'';
$spouse_occupancy = ($getEmployees->spouse_occupancy)?$getEmployees->spouse_occupancy:'';
 $get_employees="";
$i=1;
foreach($get_employee as $row)
{
	$get_employees .= "<tr><td>".$i++."</td><td>".$row->temp_emp_id."</td><td>".$row->emp_name."</td><td>".$row->emp_bid."</td><td>".$row->dept_name."</td><td>".date("d-m-Y", strtotime($row->date))."</td><td><p><a href='".base_url()."Employee/edit/".base64_encode($row->emp_id)."' class='btn-form btn-edit'> EDIT </a>&nbsp;<a href='".base_url()."Employee/delete/".base64_encode($row->emp_id)."' class='btn-form btn-delete'> DELETE </a>&nbsp;<a href='".base_url()."Employee/undo/".base64_encode($row->emp_id)."' class='btn-form btn-undo'> UNDO </a></p></td></tr>"; 
}
foreach($get_employee_log as $row)
{
	$get_employees .= "<tr><td>".$i++."</td><td>".$row->temp_emp_id."</td><td>".$row->emp_name."</td><td>".$row->emp_bid."</td><td>".$row->dept_name."</td><td>".date("d-m-Y", strtotime($row->date))."</td><td>&nbsp;</td></tr>"; 
}
?>
    <section>
         <!-- START Page content-->
         <div class="main-content">
            <!-- START row-->
            <div class="row">
               <div class="col-md-12">
                     <!-- START panel-->
                      <div class="panel panel-default">
                     <div class="panel-heading form-heading">EMPLOYEE REPORTING TIME SLOT MASTER</div>
                     <div class="panel-body">
					   <div class="row">
					    <form class="form-horizontal" action="<?php echo base_url()."Employee/EmpBulkupload"; ?>" method="post" enctype="multipart/form-data">
						<?php if(empty($getEmployees)) { ?>
						<div class="form-group col-md-8"> </div>
						<div class="form-group col-md-4"> 
					         <label class="col-lg-4 control-label">Bulk Upload</label>
                              <div class="col-lg-8">
                               <span><input type="file" name="emp_bluk">
							   <input type="hidden" name="check" value="1" />
                                <input type="submit" name="employeeadd" id="go" value="go" class="btn-form btn-edit"></span>
							   </div>
						</div>
						<?php } ?>
						</form>
					   </div>
					   <br /><br />
                       <form class="form-horizontal" action="<?php echo base_url()."Employee/add"; ?>" method="post" enctype="multipart/form-data">
                           <div class="form-group col-md-6">
                              <label class="col-lg-4 control-label">Emp ID</label>
                              <div class="col-lg-8">
                               <input type="text" name="emp_id" placeholder="Emp ID" value ="<?php echo $temp_emp_id; ?>" class="form-control" <?php echo ($temp_emp_id)?"readonly":''; ?>>
							   
                              </div>
                           </div>
                            <div class="form-group col-md-6">
                              <label class="col-lg-4 control-label">D.O.B</label>
                              <div class="col-lg-8">
                                 <input type="date" name="dob" value="<?php echo $dob; ?>" class="form-control" <?php echo ($dob)?"readonly":''; ?>>
                              </div>
                           </div>
                            <div class="form-group col-md-6">
                              <label class="col-lg-4 control-label">Emp Name</label>
                              <div class="col-lg-8">
                                 <input type="text" name="emp_name" placeholder="Number (20c)" value ="<?php echo $emp_name; ?>"class="form-control">
                              </div>
                           </div>
                            <div class="form-group col-md-6">
                              <label class="col-lg-4 control-label">Education</label>
                              <div class="col-lg-8">
                                 <input type="text" name="education" placeholder="Number (20c)" value ="<?php echo $education; ?>" class="form-control">
                              </div>
                           </div>
                            <div class="form-group col-md-6">
                              <label class="col-lg-4 control-label">Emp BID</label>
                              <div class="col-lg-8">
                                 <input type="text" name="emp_bid"  placeholder="Number (20c)" value ="<?php echo $emp_bid; ?>" class="form-control" <?php echo ($emp_bid)?"readonly":''; ?>>
                              </div>
                           </div>
                            <div class="form-group col-md-6">
                              <label class="col-lg-4 control-label">Experience-Yrs</label>
                              <div class="col-lg-8">
                                 <input type="text" name="experience" value="<?php echo $experience; ?>" placeholder="Number (20c)" class="form-control">
                              </div>
                           </div>
                            <div class="form-group col-md-6">
                              <label class="col-lg-4 control-label">D.O.J</label>
                              <div class="col-lg-8">
                                 <input type="date" name="doj" value="<?php echo $doj; ?>" class="form-control" <?php echo ($doj)?"readonly":''; ?>>
                              </div>
                           </div>
                            <div class="form-group col-md-6">
                              <label class="col-lg-4 control-label">Mobile No</label>
                              <div class="col-lg-8">
                                 <input type="text" name="mobile" value ="<?php echo $mobile; ?>" placeholder="Number (20c)" class="form-control">
                              </div>
                           </div>
                            <div class="form-group col-md-6">
                              <label class="col-lg-4 control-label">Line of Activity</label>
                              <div class="col-lg-8">
                                 <select name="line_of_activity" id="line_of_activity_id" class="form-control">
                                 	<option value="">select</option>
								 <?php 
								 $line_of_activity="";
								 $cc ="";
								 foreach($get_line_of_activity as $row)
								 {
									 $cc = ($row->activity_id == $activity_id)?"selected":"";
									 $line_of_activity .="<option value='".$row->activity_id."' ".$cc.">".$row->dept_name."</option>";
								 }
								 echo $line_of_activity;
								 ?>
								 </select>
                              </div>
                           </div>
                           
                            <div class="form-group col-md-6">
                              <label class="col-lg-4 control-label">Teach.Subject</label>
                              <div class="col-lg-8">
								<select name="subject_details" class="form-control">
								 <?php 
								 $subject_details="";
								 $cc = "";
								 foreach($get_subject_details as $row)
								 {
									  $cc = ($row->subject_details_id == $subject_details_id)?"selected":"";
									 $subject_details .="<option value='".$row->subject_details_id."' ".$cc.">".$row->sub_name."</option>";
								 }
								 echo $subject_details;
								 ?>
								 </select>
                              </div>
                           </div>
						    <div class="form-group col-md-6">
                              <label class="col-lg-4 control-label">House Details</label>
                              <div class="col-lg-8">
                                 	<select name="house_details" class="form-control" id="house_details">
								 <?php 
								 $house_details="";
								 $cc ="";
								 foreach($get_house_details as $row)
								 {
									 $cc = ($row->transport_details_id == $house_details)?"selected":"";
									 $house_details .="<option value='".$row->transport_details_id."'>".$row->transport_name."</option>";
								 }
								 echo $house_details;
								 ?>
								 </select>
                              </div>
                           </div>
						   <div class="form-group col-md-6">
                              <label class="col-lg-4 control-label">Transport Details</label>
                              <div class="col-lg-8">
                                  	<select name="transport_details" class="form-control" id="transport_details">
								 <?php 
								 $transport_details="";
								 foreach($get_transport_details as $row)
								 {
									 $cc = ($row->transport_details_id == $transport_details)?"selected":"";
									 $transport_details .="<option value='".$row->transport_details_id."'>".$row->transport_name."</option>";
								 }
								 echo $transport_details;
								 ?>
								 </select>
                              </div>
                           </div>
                  
							<div class="form-group col-md-6">
								  <label class="col-lg-4 control-label">email</label>
								  <div class="col-lg-8">
									 <input type="text" name="email" value="<?php echo $email; ?>" placeholder="email" class="form-control">
								  </div>
								</div>
						<div class="form-group col-md-6">
						  <label class="col-lg-4 control-label">sex</label>
						  <div class="col-lg-8">
							 	<select name="sex" class="form-control" id="sex">
									<option value="M">Male</option>
									<option value="F">Female</option>
								</select>
						  </div>
						</div>
						<div class="form-group col-md-6">
						  <label class="col-lg-4 control-label">Alternate No</label>
						  <div class="col-lg-8">
							 <input type="text" name="alternate_no" value="<?php echo $alternate_no; ?>" placeholder="alternate_no" class="form-control">
						  </div>
						</div>
						<div class="form-group col-md-6">
						  <label class="col-lg-4 control-label">Reporting slots</label>
						  <div class="col-lg-8">
							 <input type="text" readonly="" id="reportingslots_id" name="designation"  value="<?php echo $designation; ?>" placeholder="Reporting slots" class="form-control">
						  </div>
						</div>
						<!-- <div class="form-group col-md-6">
						  <label class="col-lg-4 control-label">Reporting slots</label>
						  <div class="col-lg-8">
							   	<select name="reportingslots_id" class="form-control" id="reportingslots_id">
								 <?php 
								 $reportingslots_id="";
								 $cc = "";
								 foreach($get_reportingslots as $row)
								 {
									 $cc = ($row->reportingslots_id == $reportingslots_id)?"selected":"";
									 $reportingslots_id .="<option value='".$row->reportingslots_id."' ".$cc.">".$row->from_time." - ".$row->to_time."</option>";
								 }
								 echo $reportingslots_id;
								 ?>
								 </select>
						  </div>
						</div> -->
						<div class="form-group col-md-6">
						  <label class="col-lg-4 control-label">Emergency No</label>
						  <div class="col-lg-8">
							 <input type="text" name="emergency_no" value="<?php echo $emergency_no; ?>" placeholder="emergency_no" class="form-control">
						  </div>
						</div>
						<div class="form-group col-md-6">
                              <label class="col-lg-4 control-label">Designations</label>
                              <div class="col-lg-8">
                                 <select name="designation" class="form-control">
								 <?php 
								 $designation="";
								 $cc ="";
								 foreach($get_designations as $row)
								 {
									 $cc = ($row->designation_id == $designation_id)?"selected":"";
									 $designation .="<option value='".$row->designation_id."' ".$cc.">".$row->designation_name."</option>";
								 }
								 echo $designation;
								 ?>
								 </select>
                              </div>
                           </div>
						<div class="form-group col-md-6">
						  <label class="col-lg-4 control-label">Address</label>
						  <div class="col-lg-8">
							 <input type="text" name="address" value="<?php echo $address; ?>" placeholder="address" class="form-control">
						  </div>
						</div>
						<div class="form-group col-md-6">
						  <label class="col-lg-4 control-label">Spouse Name</label>
						  <div class="col-lg-8">
							 <input type="text" name="spouse_name" value="<?php echo $spouse_name; ?>" placeholder="spouse_name" class="form-control">
						  </div>
						</div>
						<div class="form-group col-md-6">
						  <label class="col-lg-4 control-label">Spouse Phone No</label>
						  <div class="col-lg-8">
							 <input type="text" name="spouse_phoneno" value="<?php echo $spouse_phoneno; ?>" placeholder="spouse_phoneno" class="form-control">
						  </div>
						</div>
						<div class="form-group col-md-6">
						  <label class="col-lg-4 control-label">Spouse Occupancy</label>
						  <div class="col-lg-8">
							 <input type="text" name="spouse_occupancy" value="<?php echo $spouse_occupancy; ?>" placeholder="spouse_occupancy" class="form-control">
						  </div>
						</div>
						<div class="form-group col-md-6">
						  <label class="col-lg-4 control-label">Status</label>
						  <div class="col-lg-8">
							<input type='checkbox' name="status" checked>&nbsp;Status
						  </div>
						</div>
					 <div class="row">
						 <div class="text-center btn-sec">
							  <div class="btn-row">
									<input type="submit" name="employeeadd" id="employeeadd" class="btn-form btn-edit" value="SUBMIT"> 
									<input type="reset" class="btn-form btn-delete" value="CANCEL"> 
							  </div>
						  </div>
					 </div>
    
                        </form>
          
                     </div>
                  </div>
                     <!-- END panel-->
        
               </div>
            </div>
            <!-- END row-->
			<?php if(!empty($get_employees)) {  ?>
			<div class="table-responsive">
                           <table id="gradeX" class="table  table-bordered table-hover">
                        <thead>
                           <tr>
							  <th>S.No</th>
							  <th>Employee ID</th>
                              <th>Employee Name</th>
							  <th>Employee BID</th>
							  <th>Department</th>
							  <th>Date</th>
							  <th>Action</th>
                           </tr>
                        </thead>
                        <tbody class="mytable">
                           <?php echo $get_employees; ?>
                        </tbody>
                  </table>
            </div>
			<?php } ?>
         </div>
         <!-- END Page content-->
<script>
$(document).ready(function(){
	$("#residence_details").change(function(){
		$.ajax({
			url:'<?php echo base_url();?>Employee/get_housedetails',
			type:'POST',
			data: {store_id:store_id},
			success:function(data){
			}
		});
	});
	
	$("#line_of_activity_id").on("change",function(){
		var line_of_activity_id=$("#line_of_activity_id").val();
		$.ajax({
			url:'<?php echo base_url();?>Employee/getReportingSlots',
			type:'POST',
			data: {line_of_activity_id:line_of_activity_id},
			success:function(data){
				$('#reportingslots_id').val(data);
			}
		});
	});
})
$("#master_ul").addClass("nav collapse in");
   $("#empm").addClass("active");
</script>		 