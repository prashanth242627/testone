 <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
 <?php

 $emp_bid = ($getresignationedit['emp_bid'])?$getresignationedit['emp_bid']:'';
 $emp_name = ($getresignationedit['emp_name'])?$getresignationedit['emp_name']:'';
 $dept_name = ($getresignationedit['dept_name'])?$getresignationedit['dept_name']:'';
 $designation_name = ($getresignationedit['designation_name'])?$getresignationedit['designation_name']:'';
 $date_of_resignation = ($getresignationedit['date_of_resignation'])?$getresignationedit['date_of_resignation']:'';
 $resignation_reason = ($getresignationedit['resignation_reason'])?$getresignationedit['resignation_reason']:'';
 $date_of_approve = ($getresignationedit['date_of_approve'])?$getresignationedit['date_of_approve']:'';
 $hr_comments = ($getresignationedit['hr_comments'])?$getresignationedit['hr_comments']:'';
 $resignation_status = ($getresignationedit['status'])?$getresignationedit['status']:'';
 $resignations="";
$i=1;
foreach($get_resignations as $row)
{
	 $Status = ($row['status'])?'Approved':'Pending';
	$resignations .= "<tr><td>".$i++."</td><td>".$row['emp_bid']."</td><td>".$row['emp_name']."</td><td>".$row['temp_emp_id']."</td><td>".$row['dept_name']."</td><td>".$row['designation_name']."</td><td>".$row['date_of_resignation']."</td><td>".$row['resignation_reason']."</td><td>".$Status."</td><td>".$row['date_of_approve']."</td><td>".$row['hr_comments']."</td><td><p><a href='".base_url()."ResignationApproval/edit/".base64_encode($row['id'])."' class='btn-form btn-edit'> EDIT </a></p></td></tr>"; 
}

?>
   <!-- START Main section-->
      <section>
         <!-- START Page content-->
         <div class="main-content">
            <!-- START row-->
            <div class="row">
               <div class="col-md-8 col-md-offset-2">
                  <form action="#"  novalidate="" class="form-horizontal" method="post">
                     <!-- START panel-->
                      <div class="panel panel-default">
                     <div class="panel-heading form-heading">EMPLOYEE RESIGNATIONS</div>
                     <div class="panel-body">
                        <!-- START table-responsive-->
						<div class="row input-panel">
							<div class="form-group col-md-6">
                              <label class="col-lg-6 control-label">Employee BID</label>
                              <div class="col-lg-6">
                                <input type='text' class="form-control" name="emp_bid" id="emp_bid" value='<?php echo $emp_bid; ?>'  readonly />
                              </div>
							</div>							  
							  <div class="form-group col-md-6">
							   <label class="col-lg-6 control-label">Employee Name</label>
                              <div class="col-lg-6">
                                 <input type='text' class="form-control" name="emp_name" id="emp_name" value='<?php echo $emp_name; ?>'  readonly />
								 <input type='hidden' class="form-control" name="emp_id" id="emp_id" readonly />
                              </div>
                           </div>
                           
					   </div>
					   <div class="row">
						   <div class="form-group col-md-6">
							   <label class="col-lg-6 control-label">Line of Activity</label>
                              <div class="col-lg-6">
                                 <input type='text' class="form-control" name="activity_id" value='<?php echo $dept_name; ?>' id="activity_id" readonly />
                              </div>
                           </div>
						    <div class="form-group col-md-6">
							   <label class="col-lg-6 control-label">Designation</label>
                              <div class="col-lg-6">
                                 <input type='text' class="form-control" name="activity_id" value='<?php echo $dept_name; ?>' id="activity_id" readonly />
                              </div>
                           </div>
					   <div class="row">
					      <div class="form-group col-md-6">
							   <label class="col-lg-6 control-label">Resignation Date</label>
                              <div class="col-lg-6">
                                 <input type='text' class="form-control" name="date_of_resignation" value="<?php echo $date_of_resignation; ?>" id="date_of_resignation" readonly />
                              </div>
                           </div>
						   <div class="form-group col-md-6">
							   <label class="col-lg-6 control-label">Resignation Reason</label>
                              <div class="col-lg-6">
                                 <input type='text' class="form-control" name="resignation_reason" value="<?php echo $resignation_reason; ?>" id="resignation_reason" readonly />
                              </div>
                           </div>
					   </div>
					   <div class="row">
					      <div class="form-group col-md-6">
							   <label class="col-lg-6 control-label">Status</label>
                              <div class="col-lg-6">
                                 <select name="resignation_status" id="resignation_status" class="form-control input-imp " required="">
							 	<option value="1" <?php if($resignation_status){ echo "selected"; } ?>>Approved</option>
							 	<option value="0" <?php  if($resignation_status==0){ echo "selected"; } ?>>Pending</option>
							 	 
							 </select>
                              </div>
                           </div>
                           <div class="form-group col-md-6">
							   <label class="col-lg-6 control-label">Approval Date</label>
                              <div class="col-lg-6">
                                 <input type="date" name="date_of_approve" value ="<?php echo $date_of_approve; ?>" required placeholder="Approval Date" class="form-control" required>
                              </div>
                           </div>
                       </div>
                       <div class="row">
						   <div class="form-group col-md-12">
							   <label class="col-lg-3 control-label">HR Comments</label>
                              <div class="col-lg-9">
                              	<textarea name="hr_comments" id="hr_comments" value="<?php echo $hr_comments; ?>" class="form-control">
                              		
                              	</textarea>
                              </div>
                           </div>
					   </div>
					   <div class="row">
						  <div class="btn-sec text-center">
						  <div class="btn-row">
								<input type="submit" class="btn-form btn-edit" value="SUBMIT"> 
								<input type="reset" class="btn-form btn-delete" value="CANCEL"> 
						  </div>
						  </div>
					   </div>
                        <!-- END table-responsive-->
                     </div>
                  </div>
                     <!-- END panel-->
                  </form>
               </div>
            </div>
            <!-- END row-->
			<?php if(!empty($resignations)) {  ?>
			<div class="table-responsive">
                           <table id="gradeX" class="table  table-bordered table-hover">
                        <thead>
                           <tr>
							  <th>S.No</th>
                              <th>Employee BID</th>
							  <th>Employee Name</th>
							  <th>Employee ID</th>
							  <th>Department </th>
							  <th>Designation</th>
							  <th>Date of Resignation</th>
							  <th>Resignation Reason</th>
							  <th>Status Of Approval</th>
							  <th>Date of Approve</th>
							  <th>HR Comments</th>
							  <th>Edit</th>
                           </tr>
                        </thead>
                        <tbody class="mytable">
                           <?php echo $resignations; ?>
                        </tbody>
                  </table>
            </div>
			<?php } else{ echo "NO Resignations";}?>
         </div>
         <!-- END Page content-->
		 
<script>
$(document).ready(function(){
	var val = $("#emp_bid").val();
    $("#emp_bid").change(function(){ 
		$.ajax({
			url: '<?php echo base_url();?>Leave_master/getEmployeeDetails',
			data: {emp_bid:$(this).val()},
			method:"POST",
			success:function(data){
				var obj=jQuery.parseJSON(data);
				$('#emp_name').val(obj.emp_name);
				$('#activity_id').val(obj.dept_name); 
				$('#emp_id').val(obj.emp_id); 
			}
		});
    });
	$("#reports_ul").addClass("nav collapse in");
   $("#resig_li").addClass("active");
});
</script>