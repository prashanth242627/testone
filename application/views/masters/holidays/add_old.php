 <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
 <?php
  $holiday_name = ($getPpaymentTypeEdit->holiday_name)?$getPpaymentTypeEdit->holiday_name:'';
  $from_date = ($getPpaymentTypeEdit->from_date)?$getPpaymentTypeEdit->from_date:'';
  $to_date = ($getPpaymentTypeEdit->to_date)?$getPpaymentTypeEdit->to_date:'';
 $holidays="";
$i=1;
foreach($get_holidays as $row)
{
   $earlier = new DateTime($row->to_date);
   $later = new DateTime($row->from_date);
   $no_of_days = $later->diff($earlier)->format("%a")+1;
	$holidays .= "<tr><td>".$i++."</td><td>".$row->holiday_name."</td><td>".date("d-m-Y", strtotime($row->from_date))."</td><td>".date("d-m-Y", strtotime($row->to_date))."</td><td style='text-align:center'>".$no_of_days."</td><td>".date("d-m-Y", strtotime($row->date))."</td><td><p><a href='".base_url()."holidays/edit/".base64_encode($row->holiday_id)."' class='btn-form btn-edit'> EDIT </a>&nbsp;<a href='".base_url()."holidays/delete/".base64_encode($row->holiday_id)."' class='btn-form btn-delete'> DELETE </a>&nbsp;<a href='".base_url()."holidays/undo/".base64_encode($row->holiday_id)."' class='btn-form btn-undo'> UNDO </a></p></td></tr>"; 
}
foreach($get_holidays_log as $row)
{
   $earlier = new DateTime($row->to_date);
   $later = new DateTime($row->from_date);
   $no_of_days = $later->diff($earlier)->format("%a")+1;
	$holidays .= "<tr><td>".$i++."</td><td>".$row->holiday_name."</td><td>".date("d-m-Y", strtotime($row->from_date))."</td><td>".date("d-m-Y", strtotime($row->to_date))."</td><td style='text-align:center'>".$no_of_days."</td><td>".date("d-m-Y", strtotime($row->date))."</td><td>&nbsp;</td></tr>"; 
}
?>
   <!-- START Main section-->
      <section>
         <!-- START Page content-->
         <div class="main-content">
            <!-- START row-->
            <div class="row">
               <div class="col-md-8 col-md-offset-2">
                  <form action="#"  novalidate="" class="form-horizontal" method="post">
                     <!-- START panel-->
                      <div class="panel panel-default">
                     <div class="panel-heading form-heading">Holidays ENTRY SCREEN</div>
                     <div class="panel-body">
                        <!-- START table-responsive-->
					   <div class="row">
							<div class="form-group col-md-8">
                              <label class="col-lg-6 control-label">Holiday Name</label>
                              <div class="col-lg-6">
                                 <input type="text" name="holiday_name" value ="<?php echo $holiday_name; ?>" placeholder="Holiday Name" class="form-control">
                              </div>
                           </div>
						</div>
						<div class="row">						
						   	<div class="form-group col-md-8">
                              <label class="col-lg-6 control-label">From Date</label>
                              <div class="col-lg-6">
                                 <input type="date" name="from_date" value ="<?php echo $from_date; ?>" placeholder="From Date" class="form-control">
                              </div>
                           </div>
						
						   </div>
						   	<div class="row">						
						   	<div class="form-group col-md-8">
                              <label class="col-lg-6 control-label">To Date</label>
                              <div class="col-lg-6">
                                 <input type="date" name="to_date" value ="<?php echo $to_date; ?>" class="form-control">
                              </div>
                           </div>
						   </div>
						   <div class="row">
						   <div class="form-group col-md-4">
						   &nbsp;
						   </div>
						   <div class="form-group col-md-6">
                              <div class="col-lg-6">
                                 <input type='checkbox' name="status" checked>&nbsp;Status
                              </div>
                           </div>
					   </div>
					   <div class="row">
						  <div class="btn-sec text-center">
						  <div class="btn-row">
								<input type="submit" class="btn-form btn-edit" value="SUBMIT"> 
								<input type="reset" class="btn-form btn-delete" value="CANCEL"> 
						  </div>
						  </div>
					   </div>
                        <!-- END table-responsive-->
                     </div>
                  </div>
                     <!-- END panel-->
                  </form>
               </div>
            </div>
            <!-- END row-->
			<?php if(!empty($holidays)) {  ?>
			<div class="table-responsive">
                           <table id="gradeX" class="table  table-bordered table-hover">
                        <thead>
                           <tr>
							  <th>S.No</th>
                        <th>Holiday Name</th>
							  <th>From Date</th>
							  <th>To Date</th>
                       <th>No Of Days</th>
							  <th>Date</th>
							  <th>Action</th>
                           </tr>
                        </thead>
                        <tbody class="mytable">
                           <?php echo $holidays; ?>
                        </tbody>
                  </table>
            </div>
			<?php } ?>
         </div>
         <!-- END Page content-->
		 
<script>
   $("#reports_ul").addClass("nav collapse in");
   $("#hc_li").addClass("active");
</script>