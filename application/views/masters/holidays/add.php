 <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
 <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>app/css/multiselect.css">
 <script type="text/javascript" src="<?php echo base_url();?>app/js/multiselect.js"></script>
 <?php
  $holiday_name = ($getPpaymentTypeEdit['holiday_name'])?$getPpaymentTypeEdit['holiday_name']:'';
  $from_date = ($getPpaymentTypeEdit['from_date'])?$getPpaymentTypeEdit['from_date']:'';
  $to_date = ($getPpaymentTypeEdit['to_date'])?$getPpaymentTypeEdit['to_date']:'';
  $group_name = ($getPpaymentTypeEdit['group_name'])?$getPpaymentTypeEdit['group_name']:'';
 $holidays="";
 $holiday_group="";
$i=1;
foreach($get_holidays as $row)
{
   $earlier = new DateTime($row['to_date']);
   $later = new DateTime($row['from_date']);
   $no_of_days = $later->diff($earlier)->format("%a")+1;
	$holidays .= "<tr><td>".$i++."</td><td>".$row['holiday_name']."</td><td>".date("d-m-Y", strtotime($row['from_date']))."</td><td>".date("d-m-Y", strtotime($row['to_date']))."</td><td style='text-align:center'>".$no_of_days."</td><td>".date("d-m-Y", strtotime($row['date']))."</td><td>".$row['group_name']."</td><td><p><a href='".base_url()."holidays/edit/".base64_encode($row['holiday_id'])."' class='btn-form btn-edit'> EDIT </a>&nbsp;<a href='".base_url()."holidays/delete/".base64_encode($row['holiday_id'])."' class='btn-form btn-delete'> DELETE </a>&nbsp;<a href='".base_url()."holidays/undo/".base64_encode($row['holiday_id'])."' class='btn-form btn-undo'> UNDO </a></p></td></tr>"; 
}
foreach($get_holidays_log as $row)
{
   $earlier = new DateTime($row->to_date);
   $later = new DateTime($row->from_date);
   $no_of_days = $later->diff($earlier)->format("%a")+1;
	$holidays .= "<tr><td>".$i++."</td><td>".$row->holiday_name."</td><td>".date("d-m-Y", strtotime($row->from_date))."</td><td>".date("d-m-Y", strtotime($row->to_date))."</td><td style='text-align:center'>".$no_of_days."</td><td>".date("d-m-Y", strtotime($row->date))."</td><td>&nbsp;</td><td>".$row->class."</td></tr>"; 
}
// foreach ($get_holiday_groups as $get_holiday_group) {
//   $holiday_group.='<option value="'.$get_holiday_group->id.'" ($group_name==$get_holiday_group->group_name)? "selected":" " >'.$get_holiday_group->group_name.'</option>';
// }
?>
   <!-- START Main section-->
      <section>
         <!-- START Page content-->
         <div class="main-content">
            <!-- START row-->
            <div class="row">
               <div class="col-md-8 col-md-offset-2">
                  <form action="#"  novalidate="" class="form-horizontal" method="post">
                     <!-- START panel-->
                      <div class="panel panel-default">
                     <div class="panel-heading form-heading">Holidays ENTRY SCREEN</div>
                     <div class="panel-body">
                        <!-- START table-responsive-->
					   <div class="row input-panel">
							<div class="form-group col-md-6">
                              <label class="col-lg-4 control-label">Holiday Name</label>
                              <div class="col-lg-8">
                                 <input type="text" name="holiday_name" value ="<?php echo $holiday_name; ?>" placeholder="Holiday Name" class="form-control">
                              </div>
                           </div>
                           <div class="form-group col-md-6" style="display: none;">
                           <label class="col-lg-4 control-label">Class</label>
                           <div class="col-lg-8">
                           <select name="class" class="form-control">
                              <!-- <option value="Select">Select</option> -->
                             <option value="All" <?php if($class && ($class=="All")){echo "selected";}?>>All</option>
                             <option value="X" <?php if($class && ($class=="X")){ echo "selected";} ?>>X</option>
                             <option value="IX" <?php if($class && ($class=="IX")){echo "selected"; } ?>>IX</option>
                             <option value="VIII" <?php if($class && ($class=="VIII")){echo "selected"; }?>>VIII</option>
                             <option value="VII" <?php if($class && ($class=="VII")){echo "selected";} ?>>VII</option>
                           </select>
                         </div>
                        </div>
                        <div class="form-group col-md-6">
                           <label class="col-lg-4 control-label">Holiday Group</label>
                           <div class="col-lg-8">
                           <select name="holiday_group[]" multiple=”multiple” required class="form-control">
                              <!-- <option value="Select">Select</option> -->
                            <?php foreach($get_holiday_groups as $row) {  ?>
                             <option value='<?php echo $row->id; ?>' <?php echo (($row->group_name == $group_name)?"selected":"");?>> <?php echo $row->group_name; ?></option>
                            <?php } ?>
                           </select>
                         </div>
                        </div>
						</div>
						<div class="row">						
						   	<div class="form-group col-md-6">
                              <label class="col-lg-4 control-label">From Date</label>
                              <div class="col-lg-8">
                                 <input type="date" name="from_date" value ="<?php echo $from_date; ?>" required placeholder="From Date" class="form-control">
                              </div>
                           </div>				
						   	<div class="form-group col-md-6">
                              <label class="col-lg-4 control-label">To Date</label>
                              <div class="col-lg-8">
                                 <input type="date" required name="to_date" value ="<?php echo $to_date; ?>" class="form-control">
                              </div>
                           </div>
						   </div>

					   <div class="row">
						  <div class="btn-sec text-center">
						  <div class="btn-row">
								<input type="submit" class="btn-form btn-edit" value="SUBMIT"> 
								<input type="reset" class="btn-form btn-delete" value="CANCEL"> 
						  </div>
						  </div>
					   </div>
                        <!-- END table-responsive-->
                     </div>
                  </div>
                     <!-- END panel-->
                  </form>
               </div>
            </div>
            <!-- END row-->
			<?php if(!empty($holidays)) {  ?>
			<div class="table-responsive">
                           <table id="gradeX" class="table  table-bordered table-hover">
                        <thead>
                           <tr>
							  <th>S.No</th>
                        <th>Holiday Name</th>
							  <th>From Date</th>
							  <th>To Date</th>
                       <th>No Of Days</th>
							  <th>Date</th>
                       <th>Holiday Group</th>
							  <th>Action</th>
                           </tr>
                        </thead>
                        <tbody class="mytable">
                           <?php echo $holidays; ?>
                        </tbody>
                  </table>
            </div>
			<?php } ?>
         </div>
         <!-- END Page content-->
		 
<script>
   $("#master_ul").addClass("nav collapse in");
   $("#holidays_li").addClass("active");
  </script>
  
<script>
 
$('select[multiple]').multiselect({
 
columns: 1,
 
placeholder: 'Select options'
 
});
 
</script>