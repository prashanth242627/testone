 <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
 <?php
 
 $emp_bid = ($getLeaveMasterEdit->emp_bid)?$getLeaveMasterEdit->emp_bid:'';
 $emp_name = ($getLeaveMasterEdit->emp_name)?$getLeaveMasterEdit->emp_name:'';
 $dept_name = ($getLeaveMasterEdit->dept_name)?$getLeaveMasterEdit->dept_name:'';
 
 $no_leaves = ($getLeaveMasterEdit->no_leaves)?$getLeaveMasterEdit->no_leaves:'';
 $leave_type_id = ($getLeaveMasterEdit->leave_type_id)?$getLeaveMasterEdit->leave_type_id:'';
 $empleave_masters="";
$i=1;
foreach($get_leave_master as $row)
{
	$empleave_masters .= "<tr><td>".$i++."</td><td>".$row->emp_bid."</td><td>".$row->emp_name."</td><td>".$row->dept_name."</td><td>".$row->leave_type."</td><td>".$row->no_leaves."</td><td><p><a href='".base_url()."Leave_master/edit/".base64_encode($row->leave_master_id)."' class='btn-form btn-edit'> EDIT </a>&nbsp;<a href='".base_url()."Leave_master/undo/".base64_encode($row->leave_master_id)."' class='btn-form btn-undo'> UNDO </a></p></td></tr>"; 
}
//;<a href='".base_url()."Leave_master/delete/".base64_encode($row->leave_master_id)."' class='btn-form btn-delete'> DELETE </a>&nbsp
foreach($get_leave_master_log as $row)
{
	$empleave_masters .= "<tr><td>".$i++."</td><td>".$row->emp_bid."</td><td>".$row->emp_name."</td><td>".$row->dept_name."</td><td>".$row->leave_type."</td><td>".$row->no_leaves."</td><td>&nbsp;</td></tr>"; 
}
?>
   <!-- START Main section-->
      <section>
         <!-- START Page content-->
         <div class="main-content">
            <!-- START row-->
            <div class="row">
               <div class="col-md-8 col-md-offset-2">
                  <form action="#"  novalidate="" class="form-horizontal" method="post">
                     <!-- START panel-->
                      <div class="panel panel-default">
                     <div class="panel-heading form-heading"><?php echo $emp_bid; ?>EMPLOYEE LEAVE MASTER</div>
                     <div class="panel-body">
                        <!-- START table-responsive-->
						<div class="row input-panel">
							<div class="form-group col-md-6">
                              <label class="col-lg-6 control-label">Employee BID</label>
                              <div class="col-lg-6">
                                <select name="emp_bid" class='form-control' id="emp_bid">
								<?php foreach($get_employees as $row) {  ?>
								   
								 <option value='<?php echo $row->emp_bid; ?>' <?php echo (($row->emp_bid == $emp_bid)?"selected":"");?>> <?php echo $row->emp_bid; ?></option>
								<?php } ?>
								</select>
                              </div>
							</div>							  
							  <div class="form-group col-md-6">
							   <label class="col-lg-6 control-label">Employee Name</label>
                              <div class="col-lg-6">
                                 <input type='text' class="form-control" name="emp_name" id="emp_name" value='<?php echo $emp_name; ?>'  readonly />
								 <input type='hidden' class="form-control" name="emp_id" id="emp_id" readonly />
                              </div>
                           </div>
                           
					   </div>
					   <div class="row">
						   <div class="form-group col-md-6">
							   <label class="col-lg-6 control-label">Line of Activity</label>
                              <div class="col-lg-6">
                                 <input type='text' class="form-control" name="activity_id" value='<?php echo $dept_name; ?>' id="activity_id" readonly />
                              </div>
                           </div>
						    <div class="form-group col-md-6">
							   <label class="col-lg-6 control-label">Calendar Year</label>
                              <div class="col-lg-6">
                                 <select name="calendar_year" class='form-control'>
									
									<option value="2019">2019-2020</option>
									</select>
									</div>
                           </div>
					   <div class="row">
					      <div class="form-group col-md-6">
							   <label class="col-lg-6 control-label">Leave Type</label>
                              <div class="col-lg-6">
                                 <select name="leave_type_id" class='form-control' id="leave_type_id">
								<?php foreach($get_leave_types as $row) {  ?>
								 <option value='<?php echo $row->leave_type_id; ?>' <?php echo (($row->leave_type_id == $leave_type_id)?"selected":"");?>> <?php echo $row->leave_type; ?></option>
								<?php } ?>
								</select>
                              </div>
                           </div>
						   <div class="form-group col-md-6">
							   <label class="col-lg-6 control-label">No of Leaves</label>
                              <div class="col-lg-6">
                                 <input type='text' class="form-control" name="no_leaves" value="<?php echo $no_leaves; ?>" id="no_leaves" />
                              </div>
                           </div>
					   </div>
					   <div class="row">
						  <div class="btn-sec text-center">
						  <div class="btn-row">
								<input type="submit" class="btn-form btn-edit" value="SUBMIT"> 
								<input type="reset" class="btn-form btn-delete" value="CANCEL"> 
						  </div>
						  </div>
					   </div>
                        <!-- END table-responsive-->
                     </div>
                  </div>
                     <!-- END panel-->
                  </form>
               </div>
            </div>
            <!-- END row-->
			<?php if(!empty($empleave_masters)) {  ?>
			<div class="table-responsive">
                           <table id="gradeX" class="table  table-bordered table-hover">
                        <thead>
                           <tr>
							  <th>S.No</th>
                              <th>Employee BID</th>
							  <th>Employee Name</th>
							  <th>Department</th>
							  <th>Leave Type</th>
							  <th>No of Leave</th>
							  <th>Action</th>
                           </tr>
                        </thead>
                        <tbody class="mytable">
                           <?php echo $empleave_masters; ?>
                        </tbody>
                  </table>
            </div>
			<?php } ?>
         </div>
         <!-- END Page content-->
		 
<script>
$(document).ready(function(){
	var val = $("#emp_bid").val();
    $("#emp_bid").change(function(){ 
		$.ajax({
			url: '<?php echo base_url();?>Leave_master/getEmployeeDetails',
			data: {emp_bid:$(this).val()},
			method:"POST",
			success:function(data){
				var obj=jQuery.parseJSON(data);
				$('#emp_name').val(obj.emp_name);
				$('#activity_id').val(obj.dept_name); 
				$('#emp_id').val(obj.emp_id); 
			}
		});
    });
	$("#master_ul").addClass("nav collapse in");
   $("#lm").addClass("active");
});
</script>