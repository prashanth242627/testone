 <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
   <!-- START Main section-->
<?php
$designation="";
$i=1;
foreach($designations as $row)
{
	$designation .= "<tr><td>".$i++."</td><td><input type='text' name='designation_name[]'  value='".$row->designation_name."' placeholder='".$row->designation_name."' readonly></td><td><input type='text' name='desig_id[]' placeholder='".$row->desig_id."' value='".$row->desig_id."' readonly></td><td><p><a href='".base_url()."Designation/edit/".base64_encode($row->designation_id)."' class='btn-form btn-edit'> EDIT </a>&nbsp;<a href='".base_url()."Designation/delete/".base64_encode($row->designation_id)."' class='btn-form btn-delete'> DELETE </a>&nbsp;<a href='".base_url()."Designation/undo/".base64_encode($row->designation_id)."' class='btn-form btn-undo'> UNDO </a></p></td></tr>"; 
}
?>

      <section>
         <!-- START Page content-->
         <div class="main-content">
            <!-- START row-->
            <div class="row">
               <div class="col-md-offset-1">
                  <form action="#"  novalidate="" class="form-horizontal" method="post">
                     <!-- START panel-->
                      <div class="panel panel-default">
                     <div class="panel-heading form-heading">EMPLOYEE DESIGNATIONS ENTRY SCREEN</div>
                     <div class="panel-body">
                        <!-- START table-responsive-->
						<div class="pull-right">
							<a href="<?php echo base_url()."Designations/Add"; ?>" class="btn-form" value="List">Add</a>
						</div>
                        <div class="table-responsive">
                           <table id="gradeX" class="table  table-bordered table-hover">
                        <thead>
                           <tr>
							        <th>S.No</th>
                              <th>Designation</th>
                              <th>ABBR</th>
							        <th>Action</th>
                           </tr>
                        </thead>
                        <tbody class="mytable">
                           <?php echo $designation; ?>
                        </tbody>
                  </table>
                  <!--<div class="col-md-12 btn-sec">
                  <div class="btn-row">
						<input type="submit" class="btn-form btn-delete" value="SUBMIT"> 
						<button class="btn-form btn-edit" id="add"> ADD </button>
						<button class="btn-form" id="save">save</button>
						<button class="btn-form btn-undo"> UNDO </button>
						<button class="btn-form btn-edit"> EDIT </button>
						<button class="btn-form btn-delete delete"> DELETE </button>
						<button class="btn-form btn-exit"> EXIT </button>
                  </div>
                  </div>-->
                        </div>
                        <!-- END table-responsive-->
					<div>
				
					
					</div>
                     </div>
                  </div>
                     <!-- END panel-->
                  </form>
               </div>
            </div>
            <!-- END row-->
         </div>
         <!-- END Page content-->
		 
<script>
	$("#master_ul").addClass("nav collapse in");
   $("#desim").addClass("active");
</script>