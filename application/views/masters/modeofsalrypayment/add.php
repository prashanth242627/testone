 <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
 <?php
 $payment_type = ($getPpaymentTypeEdit->payment_type)?$getPpaymentTypeEdit->payment_type:'';
 $payment_types="";
$i=1;
foreach($get_payment_types as $row)
{
	$payment_types .= "<tr><td>".$i++."</td><td>".$row->payment_type."</td><td>".date("d-m-Y", strtotime($row->date))."</td><td><p><a href='".base_url()."Modeofsalrypayment/edit/".base64_encode($row->payment_type_id)."' class='btn-form btn-edit'> EDIT </a>&nbsp;<a href='".base_url()."Modeofsalrypayment/delete/".base64_encode($row->payment_type_id)."' class='btn-form btn-delete'> DELETE </a>&nbsp;<a href='".base_url()."Modeofsalrypayment/undo/".base64_encode($row->payment_type_id)."' class='btn-form btn-undo'> UNDO </a></p></td></tr>"; 
}
foreach($get_payment_types_log as $row)
{
	$payment_types .= "<tr><td>".$i++."</td><td>".$row->payment_type."</td><td>".date("d-m-Y", strtotime($row->date))."</td><td>&nbsp;</td></tr>"; 
}
?>
   <!-- START Main section-->
      <section>
         <!-- START Page content-->
         <div class="main-content">
            <!-- START row-->
            <div class="row">
               <div class="col-md-6 col-md-offset-3">
                  <form action="#"  novalidate="" class="form-horizontal" method="post">
                     <!-- START panel-->
                      <div class="panel panel-default">
                     <div class="panel-heading form-heading">MODE OF PAYMENT ENTRY SCREEN</div>
                     <div class="panel-body">
                        <!-- START table-responsive-->
					   <div class="row input-panel">
							<div class="form-group col-md-8">
                              <label class="col-lg-6 control-label">Payment Type</label>
                              <div class="col-lg-6">
                                 <input type="text" name="payment_type" value ="<?php echo $payment_type; ?>" placeholder="Payment Type" class="form-control">
                              </div>
                           </div>
					   </div>
					   <div class="row">
						  <div class="btn-sec text-center">
						  <div class="btn-row">
								<input type="submit" class="btn-form btn-edit" value="SUBMIT"> 
								<input type="reset" class="btn-form btn-delete" value="CANCEL"> 
						  </div>
						  </div>
					   </div>
                        <!-- END table-responsive-->
                     </div>
                  </div>
                     <!-- END panel-->
                  </form>
               </div>
            </div>
            <!-- END row-->
			<?php if(!empty($payment_types)) {  ?>
			<div class="table-responsive">
                           <table id="gradeX" class="table  table-bordered table-hover">
                        <thead>
                           <tr>
							  <th>S.No</th>
                              <th>Payment Type</th>
							  <th>Date</th>
							   <th>Action</th>
                           </tr>
                        </thead>
                        <tbody class="mytable">
                           <?php echo $payment_types; ?>
                        </tbody>
                  </table>
            </div>
			<?php } ?>
         </div>
         <!-- END Page content-->
		 
<script>
	$("#master_ul").addClass("nav collapse in");
   $("#mospm").addClass("active");
</script>