 <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
 <?php
 $residence_name = ($getresidence_detailsEdit->residence_name)?$getresidence_detailsEdit->residence_name:'';
 $residence_id = ($getresidence_detailsEdit->residence_id)?$getresidence_detailsEdit->residence_id:'';
 $residence_details="";
$i=1;
foreach($get_residence_details as $row)
{
   $residence_details .= "<tr><td>".$i++."</td><td>".$row->residence_name."</td><td>".$row->residence_id."</td><td>".date("d-m-Y", strtotime($row->date))."</td><td><p><a href='".base_url()."Residence_details/edit/".base64_encode($row->residence_details_id)."' class='btn-form btn-edit'> EDIT </a>&nbsp;<a href='".base_url()."Residence_details/delete/".base64_encode($row->residence_details_id)."' class='btn-form btn-delete'> DELETE </a>&nbsp;<a href='".base_url()."Residence_details/undo/".base64_encode($row->residence_details_id)."' class='btn-form btn-undo'> UNDO </a></p></td></tr>"; 
}
foreach($get_residence_details_log as $row)
{
   $residence_details .= "<tr><td>".$i++."</td><td>".$row->residence_name."</td><td>".date("d-m-Y", strtotime($row->date))."</td><td>&nbsp;</td></tr>"; 
}
?>
   <!-- START Main section-->
      <section>
         <!-- START Page content-->
         <div class="main-content">
            <!-- START row-->
            <div class="row">
               <div class="col-md-8 col-md-offset-2">
                  <form action="#"  novalidate="" class="form-horizontal" method="post">
                     <!-- START panel-->
                      <div class="panel panel-default">
                     <div class="panel-heading form-heading">RESIDENCE DETAILS ENTRY SCREEN</div>
                     <div class="panel-body">
                        <!-- START table-responsive-->
                  <div class="row">
                     <div class="form-group col-md-6">
                              <label class="col-lg-6 control-label">Residence Name</label>
                              <div class="col-lg-6">
                                 <input type="text" name="residence_name" value ="<?php echo $residence_name; ?>" placeholder="Residence Name" class="form-control">
                              </div>
                           </div>
                        <div class="form-group col-md-6">
                              <label class="col-lg-6 control-label">ABBR</label>
                              <div class="col-lg-6">
                                 <input type="text" name="residence_id" value ="<?php echo $residence_id; ?>" placeholder="Residence id" class="form-control">
                              </div>
                           </div>
                     </div>
                     <div class="row">
                     <div class="form-group col-md-4">
                     &nbsp;
                     </div>
                     <div class="form-group col-md-6">
                              <div class="col-lg-6">
                                 <input type='checkbox' name="status" checked>&nbsp;Status
                              </div>
                           </div>
                  </div>
                  <div class="row">
                    <div class="btn-sec text-center">
						  <div class="btn-row">
								<input type="submit" class="btn-form btn-edit" value="SUBMIT"> 
								<input type="reset" class="btn-form btn-delete" value="CANCEL"> 
						  </div>
						  </div>
                  </div>
                        <!-- END table-responsive-->
                     </div>
                  </div>
                     <!-- END panel-->
                  </form>
               </div>
            </div>
            <!-- END row-->
         <?php if(!empty($residence_details)) {  ?>
         <div class="table-responsive">
                           <table id="gradeX" class="table  table-bordered table-hover">
                        <thead>
                           <tr>
                       <th>S.No</th>
                 <th>Residence Name</th>
                       <th>ABBR</th>
                       <th>Date</th>
                        <th>Action</th>
                           </tr>
                        </thead>
                        <tbody class="mytable">
                           <?php echo $residence_details; ?>
                        </tbody>
                  </table>
            </div>
         <?php } ?>
         </div>
         <!-- END Page content-->
       
<script>
  $("#master_ul").addClass("nav collapse in");
  $("#rsm").addClass("active");
</script>