<!DOCTYPE html>
<html lang="en" class="no-ie">
<head>
   <!-- Meta-->
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
   <meta name="description" content="">
   <meta name="keywords" content="">
   <meta name="author" content="">
   <title><?php echo $page_title ?></title>

   <link rel="stylesheet" href="<?php echo base_url();?>app/css/bootstrap.css">
   <!-- Vendor CSS-->
   <link rel="stylesheet" href="<?php echo base_url();?>vendor/fontawesome/css/font-awesome.min.css">
   <link rel="stylesheet" href="<?php echo base_url();?>vendor/animo/animate%2banimo.css">
   <link rel="stylesheet" href="<?php echo base_url();?>vendor/csspinner/csspinner.min.css">
   <!-- START Page Custom CSS-->
   <!-- END Page Custom CSS-->
   <!-- App CSS-->
   <link rel="stylesheet" href="<?php echo base_url();?>app/css/app.css">
   <!-- Modernizr JS Script-->
   <script src="<?php echo base_url();?>vendor/modernizr/modernizr.js" type="application/javascript"></script>
   <!-- FastClick for mobiles-->
   <script src="<?php echo base_url();?>vendor/fastclick/fastclick.js" type="application/javascript"></script>
   <script src="<?php echo base_url();?>vendor/jquery/jquery.min.js"></script>
   <script src="<?php echo base_url();?>vendor/bootstrap/js/bootstrap.min.js"></script>
   <!-- Plugins-->
   <script src="<?php echo base_url();?>vendor/chosen/chosen.jquery.min.js"></script>
   <script src="<?php echo base_url();?>vendor/slider/js/bootstrap-slider.js"></script>
   <script src="<?php echo base_url();?>vendor/filestyle/bootstrap-filestyle.min.js"></script>
   <!-- Animo-->
   <script src="<?php echo base_url();?>vendor/animo/animo.min.js"></script>
   <!-- Sparklines-->
   <script src="<?php echo base_url();?>vendor/sparklines/jquery.sparkline.min.js"></script>
   <!-- Slimscroll-->
   <script src="<?php echo base_url();?>vendor/slimscroll/jquery.slimscroll.min.js"></script>
   <!-- Store + JSON-->
   <script src="<?php echo base_url();?>vendor/store/store%2bjson2.min.js"></script>
   <!-- Classyloader-->
   <script src="<?php echo base_url();?>vendor/classyloader/js/jquery.classyloader.min.js"></script>
   <!-- START Page Custom Script-->
   <!-- Form Validation-->
   <script src="<?php echo base_url();?>vendor/parsley/parsley.min.js"></script>
   <!-- END Page Custom Script-->
   <!-- App Main-->
   <script src="<?php echo base_url();?>app/js/app.js"></script>
 <script src="<?php echo base_url();?>vendor/jquery/tableHeadFixer.js"></script>
 <script>	
     $(document).ready(function() {
     $(".table") .tableHeadFixer();
    });
 </script>
</head>

<body>
   <!-- START Main wrapper-->
   <div class="wrapper">