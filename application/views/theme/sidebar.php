<nav class="navbar navbar-fixed-top" style="background-color: #354177">
  <div class="container-fluid">
  <div class="col-md-4">
    <div class="col-md-2 navbar-header" style="padding-top: 15px;font-size: 30px;">
      <a href="#" data-toggle-state="aside-collapsed" data-persists="true" class="hidden-xs">
             <em class="fa fa-navicon"></em>
           </a>
    </div>
    <ul class="col-md-6 nav navbar-nav">
      <li class="active"><img src="<?php echo base_url();?>images/logo.png" alt="App Logo" class="img-responsive" style="height: 77px;"></li>
    </ul>
    </div>

    <div class="col-md-4">
    <ul class="" style="list-style: none">
      <li class="active"><img src="<?php echo base_url();?>images/logo2.png" alt="App Logo" class="img-responsive" style="height: 68px;margin: 0px auto;padding-top: 16px;"></li>
    </ul>
    </div>

    <div class="col-md-4">
    <ul class="nav navbar-nav navbar-right">
      <li><a href="#" data-toggle="reset">
             <em class="fa fa-refresh"></em> Reload
           </a></li>
      <li><a href="<?php echo base_url(); ?>Login/logouts">Logout</a></li>
    </ul>
    </div>
  </div>
</nav>
  <!-- END Top Navbar-->
    <!-- START aside-->
      <aside class="aside">
         <!-- START Sidebar (left)-->
          <nav class="sidebar">
            <ul class="nav" id="main_ul">
               <!-- START Menu-->
               <li class="" id="home_li">
                  <a href="<?php echo base_url();?>Dashboard" title="Home">
                  <em class="fa fa-dot-circle-o"></em>
                     <span class="item-text">Home</span>
                  </a>
         
               </li>
               <li class="" id="master_li">
                  <a href="#" title="Dashboard" data-toggle="collapse-next" class="has-submenu">
                     <em class="fa fa-dot-circle-o"></em>
                     <!--<div class="label label-primary pull-right">12</div>-->
                     <span class="item-text">Masters Data</span>
                  </a>
                  <!-- START SubMenu item-->
                  <ul class="nav collapse" id="master_ul">
                     <li class="" id="loam">
                        <a href="<?php echo base_url()."Lineofactivity" ?>" title="Emp Department Master" data-toggle="" class="no-submenu">
                           <span class="item-text">Department</span>
                        </a>
                     </li>
                     <li class="" id="desim">
                        <a href="<?php echo base_url()."Designation" ?>" title="Emp Designation Master" data-toggle="" class="no-submenu">
                           <span class="item-text">Designation</span>
                        </a>
                     </li>
                     <li class="" id="desim">
                        <a href="<?php echo base_url()."Roles" ?>" title="Emp Designation Master" data-toggle="" class="no-submenu">
                           <span class="item-text">Roles</span>
                        </a>
                     </li>
                     <li class="" id="tsm">
                        <a href="<?php echo base_url()."Teachingsubject"; ?>" title="Emp Teaching subject Master" data-toggle="" class="no-submenu">
                           <span class="item-text">Teaching subject </span>
                        </a>
                     </li>
                     <!--<li>-->
                     <!--   <a href="<?php //echo base_url()."Residence_details"; ?>" title="Emp Residenced" data-toggle="" class="no-submenu">-->
                     <!--      <span class="item-text">Emp Residenced Master</span>-->
                     <!--   </a>-->
                     <!--</li>-->
					    <li class="" id="tdm">
                        <a href="<?php echo base_url()."Transport_details"; ?>" title="Emp Transport_details" data-toggle="" class="no-submenu">
                           <span class="item-text">Transport Details </span>
                        </a>
                     </li>
                     <li class="" id="rsm" style="display: none;">
                        <a href="<?php echo base_url()."Reportingslots"; ?>" title="Emp Reporting slots Master" data-toggle="" class="no-submenu">
                           <span class="item-text">Reporting slots</span>
                        </a>
                     </li>
                     <li class="" id="empm">
                        <a href="<?php echo base_url()."Employee"; ?>" title="Emp Master" data-toggle="" class="no-submenu">
                           <span class="item-text">Emp Details</span>
                        </a>
                     </li>
                     <li class="" id="mospm">
                        <a href="<?php echo base_url()."Modeofsalrypayment"; ?>" title="Mode of salry Payment Master" data-toggle="" class="no-submenu">
                           <span class="item-text">Mode of salary Payment </span>
                        </a>
                     </li>
                     <li class="" id="psm">
                        <a href="<?php echo base_url()."EmpPayScale"; ?>" title="Emp Payscale Master" data-toggle="" class="no-submenu">
                           <span class="item-text">Pay scale</span>
                        </a>
                     </li>
                     <li class="" id="ddm">
                        <a href="<?php echo base_url()."StanderdDeductions"; ?>" title="Emp Deductions Master" data-toggle="" class="no-submenu">
                           <span class="item-text">Deductions </span>
                        </a>
                     </li>
                     <li class="" id="md">
                        <a href="<?php echo base_url()."ManagementDeductions"; ?>" title="Management Deductions" data-toggle="" class="no-submenu">
                           <span class="item-text">Other Deductions </span>
                        </a>
                     </li>

                     <li class="" id="savm">
                        <a href="<?php echo base_url()."Empsavingsmaster"; ?>" title="Emp savings Master" data-toggle="" class="no-submenu">
                           <span class="item-text">TDS Savings</span>
                        </a>
                     </li>
                     <li class="" id="exhm">
                        <a href="<?php echo base_url()."Empextrahoursmaster"; ?>" title="Emp Extra Hours Master" data-toggle="" class="no-submenu">
                           <span class="item-text">Extra Hours</span>
                        </a>
                     </li>
                     <li class="" id="ltm">
                        <a href="<?php echo base_url()."Leave_types"; ?>" title="Emp Leave type Master" data-toggle="" class="no-submenu">
                           <span class="item-text">Leave Type </span>
                        </a>
                     </li>
                     <li class="" id="lm">
                        <a href="<?php echo base_url()."Leave_master"; ?>" title="Emp Leave Master" data-toggle="" class="no-submenu">
                           <span class="item-text">Leave Master</span>
                        </a>
                     </li>

                     <li class="" id="holidays_li">
                        <a href="<?php echo base_url()."Holidays"; ?>" title="Holidays Master" data-toggle="" class="no-submenu">
                           <span class="item-text">Holidays Master</span>
                        </a>
                     </li>
                     <!-- <li class="" id="loam">
                        <a href="<?php echo base_url()."Leaves"; ?>" title="Emp Leave Master" data-toggle="" class="no-submenu">
                           <span class="item-text">Emp Leaves Master</span>
                        </a>
                     </li> -->

                  </ul>
                  <!-- END MASTER SCRREN-->
               </li>
              <!--************************************
                        ASSIGNMETS /ENTRIES SCREENS
              ******************************** -->
               <li class="" id="assin_li">
                  <a href="Attendance_marking" title="Assignments Entries" data-toggle="collapse-next" class="has-submenu">
                     <em class="fa fa-dot-circle-o"></em>
                     <span class="item-text"> Assignments Entries </span>
                  </a>
                  <!-- START SubMenu item-->
                  <ul class="nav collapse " id="assin_ul">
                     <li id="atten_li" class="">
                        <a href="<?php echo base_url()."Attendance_marking"; ?>" title="Attendance Marking Entry screen" data-toggle="" class="no-submenu">
                           <span class="item-text">Attendance Marking Entry</span>
                        </a>
                     </li>
                     <li id="bmatten_li" class="">
                        <a href="<?php echo base_url()."Biometric_attendance_marking"; ?>" title="Attendance Marking Entry screen" data-toggle="" class="no-submenu">
                           <span class="item-text">BioMetric Attendance Marking</span>
                        </a>
                     </li>
                     <li id="lop_li" class="">
                        <a href="<?php echo base_url()."EmployeeLOPCalculation"; ?>" title="Bio Metric Leaves and LOP Entry screen" data-toggle="" class="no-submenu">
                           <span class="item-text">Bio Metric LOP Calculation</span>
                        </a>
                     </li>
                     <li id="bam_li" class="">
                        <a href="<?php echo base_url()."BioMetricAttendancesExport"; ?>" title="BioMetric Attendances Export" data-toggle="" class="no-submenu">
                           <span class="item-text">BioMetric Attendance Migration</span>
                        </a>
                     </li>
                     
                     <li id="pd_li" class="">
                        <a href="<?php echo base_url()."ProbationPeriod"; ?>" title="probation period" data-toggle="" class="no-submenu">
                           <span class="item-text">Probation Period Employees</span>
                        </a>
                     </li>
                     <li id="lf_li" class="">
                        <a href="<?php echo base_url()."LeaveForm"; ?>" title="Emp Leave Form" data-toggle="" class="no-submenu">
                           <span class="item-text">Leave Form</span>
                        </a>
                     </li>
                  </ul>
                  <!-- END SubMenu item-->
               </li>
			        <!--************************************
                        Administration /ENTRIES SCREENS
              ******************************** -->
               <li class="" id="adminentry_li" style="display: none;">
                  <a href="#" title="Assignments Entries" data-toggle="collapse-next" class="has-submenu">
                     <em class="fa fa-dot-circle-o"></em>
                     <span class="item-text"> Administration Entries </span>
                  </a>
                  <!-- START SubMenu item-->
                  <ul class="nav collapse ">
                     <li>
                        <a href="user.html" title="Attendance Marking Entry screen" data-toggle="" class="no-submenu">
                           <span class="item-text">Add User</span>
                        </a>
                     </li>
                     <li>
                        <a href="role.html" title="Bio Metric Leaves and LOP Entry screen" data-toggle="" class="no-submenu">
                           <span class="item-text">Add role</span>
                        </a>
                     </li>
					           <li>
                        <a href="role_permission.html" title="Bio Metric Leaves and LOP Entry screen" data-toggle="" class="no-submenu">
                           <span class="item-text">Role Permission</span>
                        </a>
                     </li>
                  </ul>
                  <!-- END SubMenu item-->
               </li>
               <!--************************************
                      REPORTS
              ******************************** -->
               <li class="" id="reports_li">
                  <a href="#" title="Pages" data-toggle="collapse-next" class="has-submenu">
                      <em class="fa fa-dot-circle-o"></em>
                     <span class="item-text">Reports</span>
                  </a>
                  <!-- START SubMenu item-->
                  <ul class="nav collapse " id="reports_ul">
                           <b class="sub-heading">HR REPORTS</b>
                     
                     <li class="" id="atndsr_li">
                        <a href="<?php echo base_url()."AttendanceReports"; ?>" title="Attendance report" data-toggle="" class="no-submenu">
                           <span class="item-text">Attendance</span>
                        </a>
                     </li>
                     <li class="" id="mar_li">
                        <a href="<?php echo base_url()."MonthlyReports"; ?>" title="Monthly Attendance Summary" data-toggle="" class="no-submenu">
                           <span class="item-text">Monthly Attendance Summary</span>
                        </a>
                     </li>
                     <!-- <li class="" id="atndsr_li">
                        <a href="BioMetricattendancereport.html" title="Bio Metric attendance report" data-toggle="" class="no-submenu">
                           <span class="item-text">Bio Metric attendance report</span>
                        </a>
                     </li>
                     <li>
                        <a href="EmployeevsBioMetric.html" title="Employee attendance vs Bio Metric reporting" data-toggle="" class="no-submenu">
                           <span class="item-text">Employee attendance vs Bio Metric reporting</span>
                        </a>
                     </li> -->
                     <li class="" id="emls_li">
                        <a href="<?php echo base_url()."EmployeeMonthlyLeaveStatus"; ?>" title="Employee Monthly Leaves Status" data-toggle="" class="no-submenu">
                           <span class="item-text">Employee Monthly Leaves Status</span>
                        </a>
                     </li>
                     <li class="" id="els_li">
                        <a href="<?php echo base_url()."EmployeeLeaveStatus"; ?>" title="Employee Leave status" data-toggle="" class="no-submenu">
                           <span class="item-text">Employee Leave status</span>
                        </a>
                     </li>
                     <li class="" id="resig_li">
                        <a href="<?php echo base_url()."ResignationApproval"; ?>" title="Resignation Approval" data-toggle="" class="no-submenu">
                           <span class="item-text">Resignation Approval</span>
                        </a>
                     </li>
                     <li class="" id="hcl_li">
                        <a href="<?php echo base_url()."HolidaysList"; ?>" title="Holidays_list" data-toggle="" class="no-submenu">
                           <span class="item-text">Holidays List</span>
                        </a>
                     </li>
                     <!-- <li class="" id="eod_li">
                        <a href="ListofEmployeesonOD.html" title="List of Employees on OD" data-toggle="" class="no-submenu">
                           <span class="item-text">List of Employees on OD</span>
                        </a>
                     </li> -->
                     <li class="" id="ed_li">
                        <a href="<?php echo base_url()."EmployeeDetails"; ?>" title="Employee Details" data-toggle="" class="no-submenu">
                           <span class="item-text">Employee Details</span>
                        </a>
                     </li>
                     <!-- <li style="list-style-type: none;">
                        <a href="Listofabsentees.html" title="List of absentees" data-toggle="" class="no-submenu">
                           <span class="item-text"> --><b class="sub-heading">FINANCE REPORTS</b><!-- </span>
                        </a>
                     </li> -->
                     <!-- <li class="" id="ss_li">
                        <a href="Empsalarystructure.html" title="Emp. salary structure" data-toggle="" class="no-submenu">
                           <span class="item-text">Emp. salary structure</span>
                        </a>
                     </li> -->
                     <li class="" id="sald_li">
                        <a href="<?php echo base_url()."Salaries"; ?>" title="Emp. CTS details" data-toggle="" class="no-submenu">
                           <span class="item-text">Emp. CTS details</span>
                        </a>
                     </li>
                     <li class="" id="msald_li">
                        <a href="<?php echo base_url()."MonthlySalaries"; ?>" title="Emp. Monthly salary details" data-toggle="" class="no-submenu">
                           <span class="item-text">Emp. Monthly Salary details</span>
                        </a>
                     </li>
                     <li class="" id="msald_li">
                        <a href="<?php echo base_url()."BankStatement"; ?>" title="Emp. Monthly salary details" data-toggle="" class="no-submenu">
                           <span class="item-text">Bank Statement</span>
                        </a>
                     </li>
                     <li class="" id="msalc_li">
                        <a href="<?php echo base_url()."SalariesCalculation"; ?>" title="Emp. Monthly salary details" data-toggle="" class="no-submenu">
                           <span class="item-text">Emp. Salary Calculation</span>
                        </a>
                     </li>
                     <li class="" id="pm_li">
                        <a href="<?php echo base_url()."PaymentModes"; ?>" title="Payment Modes" data-toggle="" class="no-submenu">
                           <span class="item-text">Payment Modes</span>
                        </a>
                     </li>
                     <li class="" id="deptsal_li">
                        <a href="<?php echo base_url()."DepartmentSalaries"; ?>" title="Department Salaries" data-toggle="" class="no-submenu">
                           <span class="item-text">Department Salaries</span>
                        </a>
                     </li>
                     <li class="" id="econ_li" style="display: none;">
                        <a href="Employercontribution.html" title="Employer contribution" data-toggle="" class="no-submenu">
                           <span class="item-text">Employer contribution</span>
                        </a>
                     </li>
                     <li class="" id="eps_li">
                        <a href="<?php echo base_url()."EmpPaySlips"; ?>" title="Emp.Payslip" data-toggle="" class="no-submenu">
                           <span class="item-text">Emp.Payslip</span>
                        </a>
                     </li>
                     <!-- <li class="" id="atndsr_li">
                        <a href="DeptwiseSalarayPaymentsummary.html" title="Dept wise Salaray Payment summary" data-toggle="" class="no-submenu">
                           <span class="item-text">Dept wise Salaray Payment summary</span>
                        </a>
                     </li> -->
                     
                     <li class="" id="eluts_li" style="display: none;">
                        <a href="EmployeesListunderTaxscope.html" title="Employees List under Tax scope" data-toggle="" class="no-submenu">
                           <span class="item-text">Employees List under Tax scope</span>
                        </a>
                     </li>
                     <li class="" id="eti_li" style="display: none;">
                        <a href="EmployeeTaxableincome.html" title="Employee Taxable income" data-toggle="" class="no-submenu">
                           <span class="item-text">Employee Taxable income</span>
                        </a>
                     </li>
                     <!-- <li>
                        <a href="#," title="Employees List under Tax scope" data-toggle="" class="no-submenu">
                           <span class="item-text"> --><b class="sub-heading">STATUTATORY </b><!-- </span>
                        </a>
                     </li> -->
                     <li class="" id="esave_li">
                        <a href="<?php echo base_url()."EmployeeSavings"; ?>" title="Employee savings" data-toggle="" class="no-submenu">
                           <span class="item-text">Employee TDS and Savings</span>
                        </a>
                     </li>
                     <li class="" id="ept_li">
                        <a href="<?php echo base_url()."ProfessionalTax"; ?>" title="Employee Professional Tax" data-toggle="" class="no-submenu">
                           <span class="item-text">Employee Professional Tax</span>
                        </a>
                     </li>
                     <li class="" id="epfr_li" >
                        <a href="<?php echo base_url()."EmpPFContribution"; ?>" title="Stautatory PF Returns" data-toggle="" class="no-submenu">
                           <span class="item-text">Statutory PF Returns</span>
                        </a>
                     </li>
                     <li class="" id="esir_li">
                        <a href="<?php echo base_url()."EmpESIreturns"; ?>" title="Stautatory ESI Returns" data-toggle="" class="no-submenu">
                           <span class="item-text">Statutory ESI Returns</span>
                        </a>
                     </li>
                     <!-- <li>
                        <a href="#." title="Employees List under Tax scope" data-toggle="" class="no-submenu">
                           <span class="item-text"> <b>MANAGERIAL REPORTS</b>--><!-- </span>
                        </a>
                     </li> -->
                     <li class="" id="empr_li" style="display: none;">
                        <a href="EmployeesRating.html" title="Employees Rating" data-toggle="" class="no-submenu">
                           <span class="item-text">Employees Rating</span>
                        </a>
                     </li>
                     <li class="" id="ecfc_li" style="display: none;">
                        <a href="ListofEmployeescalls.html" title="List of Employees calls for councilling" data-toggle="" class="no-submenu">
                           <span class="item-text">List of Employees calls for counseling</span>
                        </a>
                     </li>
                  </ul>
                  <!-- END SubMenu item-->
               </li>
            </ul>
         </nav>
         <!-- END Sidebar (left)-->
      </aside>
      <!-- End aside-->