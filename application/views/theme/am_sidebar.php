<nav class="navbar navbar-fixed-top" style="background-color: #354177">
  <div class="container-fluid">
  <div class="col-md-4">
    <div class="col-md-2 navbar-header" style="padding-top: 15px;font-size: 30px;">
      <a href="#" data-toggle-state="aside-collapsed" data-persists="true" class="hidden-xs">
             <em class="fa fa-navicon"></em>
           </a>
    </div>
    <ul class="col-md-6 nav navbar-nav">
      <li class="active"><img src="<?php echo base_url();?>images/logo.png" alt="App Logo" class="img-responsive" style="height: 77px;"></li>
    </ul>
    </div>

    <div class="col-md-4">
    <ul class="" style="list-style: none">
      <li class="active"><img src="<?php echo base_url();?>images/logo2.png" alt="App Logo" class="img-responsive" style="height: 68px;margin: 0px auto;padding-top: 16px;"></li>
    </ul>
    </div>

    <div class="col-md-4">
    <ul class="nav navbar-nav navbar-right">
      <li><a href="#" data-toggle="reset">
             <em class="fa fa-refresh"></em> Reload
           </a></li>
      <li><a href="<?php echo base_url(); ?>Login/logouts">Logout</a></li>
    </ul>
    </div>
  </div>
</nav>
  <!-- END Top Navbar-->
    <!-- START aside-->
      <aside class="aside">
         <!-- START Sidebar (left)-->
          <nav class="sidebar">
            <ul class="nav" id="main_ul">
               <!-- START Menu-->
               <li class="" id="home_li">
                  <a href="<?php echo base_url();?>Dashboard" title="Home">
                  <em class="fa fa-dot-circle-o"></em>
                     <span class="item-text">Home</span>
                  </a>
         
               </li>
               
              <!--************************************
                        ASSIGNMETS /ENTRIES SCREENS
              ******************************** -->
               <li class="" id="assin_li">
                  <a href="Attendance_marking" title="Assignments Entries" data-toggle="collapse-next" class="has-submenu">
                     <em class="fa fa-dot-circle-o"></em>
                     <span class="item-text"> Assignments Entries </span>
                  </a>
                  <!-- START SubMenu item-->
                  <ul class="nav collapse " id="assin_ul">
                     <li id="atten_li" class="">
                        <a href="<?php echo base_url()."Attendance_marking"; ?>" title="Attendance Marking Entry screen" data-toggle="" class="no-submenu">
                           <span class="item-text">Attendance Marking Entry screen</span>
                        </a>
                     </li>
                     <li id="bmatten_li" class="">
                        <a href="<?php echo base_url()."Biometric_attendance_marking"; ?>" title="Attendance Marking Entry screen" data-toggle="" class="no-submenu">
                           <span class="item-text">BioMetric Attendance Marking</span>
                        </a>
                     </li>
                     <li id="lop_li" class="">
                        <a href="<?php echo base_url()."EmployeeLOPCalculation"; ?>" title="Bio Metric Leaves and LOP Entry screen" data-toggle="" class="no-submenu">
                           <span class="item-text">Bio Metric LOP Calculation screen</span>
                        </a>
                     </li>
                     <li id="bam_li" class="">
                        <a href="<?php echo base_url()."BioMetricAttendancesExport"; ?>" title="BioMetric Attendances Export" data-toggle="" class="no-submenu">
                           <span class="item-text">BioMetric Attendance Migration</span>
                        </a>
                     </li>
                     <li id="lf_li" class="">
                        <a href="<?php echo base_url()."LeaveForm"; ?>" title="Emp Leave Form" data-toggle="" class="no-submenu">
                           <span class="item-text">Leave Form</span>
                        </a>
                     </li>
                  </ul>
                  <!-- END SubMenu item-->
               </li>
			        
                  <!-- END SubMenu item-->
               </li>
            </ul>
         </nav>
         <!-- END Sidebar (left)-->
      </aside>
      <!-- End aside-->