<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$config=array(	'line_of_activity'=>'line_of_activity',
				'designation'=>'designation',
				'designation_log'=>'designation_log',
				'line_of_activity_log'=>'line_of_activity_log',
				'subject_details'=>'subject_details',
				'subject_details_log'=>'subject_details_log',
				'residence_details'=>'residence_details',
				'residence_details_log'=>'residence_details_log',
				'transport_details'=>'transport_details',
				'transport_details_log'=>'transport_details_log',
				'reportingslots'=>'reportingslots',
				'reportingslots_log'=>'reportingslots_log',
				'employee'=>'employee',
				'employee_log'=>'employee_log',
				'payment_types'=>'payment_types',
				'payment_types_log'=>'payment_types_log',
				'empsavingsmaster'=>'empsavingsmaster',
				'empsavingsmaster_log'=>'empsavingsmaster_log',
				'empextrahoursmaster'=>'empextrahoursmaster',
				'empextrahoursmaster_log'=>'empextrahoursmaster_log',
				'leave_types'=>'leave_types',
				'leave_types_log'=>'leave_types_log',
				'leave_master'=>'leave_master',
				'leave_master_log'=>'leave_master_log',
				'standerd_deductions'=>'standerd_deductions',
				'employee_pay_details'=>'employee_pay_details',
				'standerd_deductions_log' => 'standerd_deductions_log',
				'employee_pay_details_log' => 'employee_pay_details_log',
				'holidays'=>'holidays',
				'holidays_log'=>'holidays_log',
				'attendance_marking'=>'attendance_marking',
				'attendance_marking_log'=>'attendance_marking'
			 );
