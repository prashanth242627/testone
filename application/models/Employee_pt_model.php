<?php
class Employee_pt_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct(); 
	}
    
	public function AllEmployeesPTData($year,$month)
    {
        $CI =& get_instance();
        $CI->load->model('Myfunctions');
        $prev_month_days = cal_days_in_month(CAL_GREGORIAN, date('m',strtotime($month)), $year);  
        $sql_query="select ems.pt,ems.gross,la.dept_name,d.designation_name,ems.emp_bid,e.temp_emp_id as emp_id,e.date_of_join,e.emp_name,la.dept_name from emp_monthly_salaries ems,employee e,designation d,line_of_activity la where la.activity_id=e.activity_id and e.emp_bid=ems.emp_bid and d.designation_id=e.designation and ems.salary_month=".date('m',strtotime($month))." and ems.salary_year=".$year." order by length(e.emp_bid),e.emp_bid";
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
    }
    public function EmployeePTDataWhere($where,$year,$month)
    {
        $CI =& get_instance();
        $CI->load->model('Myfunctions');
        $prev_month_days = cal_days_in_month(CAL_GREGORIAN, date('m',strtotime($month)), $year);  
        //new query
        $sql_query="select ems.pt,ems.gross,la.dept_name,d.designation_name,ems.emp_bid,e.temp_emp_id as emp_id,e.date_of_join,e.emp_name,la.dept_name from emp_monthly_salaries ems,employee e,designation d,line_of_activity la where la.activity_id=e.activity_id and e.emp_bid=ems.emp_bid and d.designation_id=e.designation and ems.salary_month=".date('m',strtotime($month))." and ems.salary_year=".$year." and $where order by length(e.emp_bid),e.emp_bid";
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
    }
	public function getSalariesData($where)
	{
		$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="select epd.*,e.education,e.designation,e.emp_id,e.date_of_join,e.emp_name,pt.payment_type, la.dept_name from employee_pay_details epd,employee e,payment_types pt, line_of_activity la where la.activity_id=e.activity_id and e.emp_bid=epd.emp_bid and epd.payment_type_id=pt.payment_type_id and ".$where;
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
	}
	public function activeEmployees()
	{
		$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="select e.*,loa.dept_name from employee e,line_of_activity loa where e.activity_id=loa.activity_id";
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
	}
	public function inActiveEmployees()
	{
		$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="select e.*,loa.dept_name from employee_log e,line_of_activity loa where e.activity_id=loa.activity_id";
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
	}
	public function newEmployees()
	{
		$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $d2 = date('Y-m-d', strtotime('-30 days'));
        $sql_query="select e.*,loa.dept_name from employee e,line_of_activity loa where e.activity_id=loa.activity_id and e.date>".$d2;
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
	}
	public function paymentModes()
	{
		$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="select payment_type from payment_types order by payment_type";
        $data=$CI->Myfunctions->getQueryDataList($sql_query);
        return $data;
	}
}
?>