<?php
class Employee_esi_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct(); 
	}

    
public function AllEmployeesESIData($year,$month)
    {
        $CI =& get_instance();
        $CI->load->model('Myfunctions');
        $prev_month_days = cal_days_in_month(CAL_GREGORIAN, date('m',strtotime($month)), $year);  
        $sql_query="select salary.*,

 m_gross*esi.deduct_value/100 esi_employee_contribution,

 m_gross*4.75/100  esi_employr_contribution

from (

      select e.temp_emp_id temp_emp_id, e.emp_bid, e.emp_name, pay.esi_no, la.dept_name,mon_salary.pay_days, d.designation_name,

      (pay.basic+pay.other_allowance+incr+co_allowance+da+special_allowance+hra+cca)/12 m_gross,                           

       esi_consider

       from employee_pay_details pay LEFT JOIN employee e ON pay.emp_bid = e.emp_bid

       LEFT JOIN line_of_activity la on e.activity_id = la.activity_id

       LEFT JOIN emp_monthly_salaries mon_salary on e.emp_bid= mon_salary.emp_bid and salary_month=".date('m',strtotime($month))." and salary_year=".$year."

       LEFT JOIN designation d on e.designation = d.designation_id

       where pay.employment_type = 0) salary,

       std_esi_deductions esi

where m_gross between esi.min_limit and esi.max_limit

and esi_consider = 1

and m_gross >0 and m_gross <= 21000

order by length(emp_bid),emp_bid asc";
        /*$sql_query="select salary.*,
case when esi_consider = 1 and m_gross<= 21000 then m_gross*esi.deduct_value/100 else 0 end esi_employee_contribution,
case when esi_consider = 1 and m_gross<= 21000 then m_gross*4.75/100 else 0 end esi_employr_contribution
from (
      select e.temp_emp_id, e.emp_bid, e.emp_name, la.dept_name, d.designation_name,
      (pay.basic+pay.other_allowance+incr+co_allowance+da+special_allowance+hra+cca)/12 m_gross,                            
       esi_consider
       from employee e LEFT JOIN employee_pay_details pay ON pay.emp_bid = e.emp_bid
       LEFT JOIN line_of_activity la on e.activity_id = la.activity_id
       LEFT JOIN designation d on e.designation = d.designation_id) salary,
       std_esi_deductions esi
where m_gross between esi.min_limit and esi.max_limit
and esi_consider = 1
and m_gross >0 and m_gross <= 21000
order by length(emp_bid),emp_bid asc";*/
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
    }
     public function EmployeeESIDataWhere($where,$year,$month)
    {
        $CI =& get_instance();
        $CI->load->model('Myfunctions');
        $prev_month_days = cal_days_in_month(CAL_GREGORIAN, date('m',strtotime($month)), $year);  
        $sql_query="select salary.*,

 m_gross*esi.deduct_value/100 esi_employee_contribution,

 m_gross*4.75/100  esi_employr_contribution

from (

      select e.temp_emp_id temp_emp_id, e.emp_bid, e.emp_name, pay.esi_no, la.dept_name,mon_salary.pay_days, d.designation_name,

      (pay.basic+pay.other_allowance+incr+co_allowance+da+special_allowance+hra+cca)/12 m_gross,                           

       esi_consider

       from employee_pay_details pay LEFT JOIN employee e ON pay.emp_bid = e.emp_bid

       LEFT JOIN line_of_activity la on e.activity_id = la.activity_id
       LEFT JOIN emp_monthly_salaries mon_salary on e.emp_bid= mon_salary.emp_bid and salary_month=".date('m',strtotime($month))." and salary_year=".$year."

       LEFT JOIN designation d on e.designation = d.designation_id

       where pay.employment_type = 0 and $where) salary,

       std_esi_deductions esi

where m_gross between esi.min_limit and esi.max_limit

and esi_consider = 1

and m_gross >0 and m_gross <= 21000

order by length(emp_bid),emp_bid asc";
        /*$sql_query="select salary.*,
case when esi_consider = 1 and m_gross<= 21000 then m_gross*esi.deduct_value/100 else 0 end esi_employee_contribution,
case when esi_consider = 1 and m_gross<= 21000 then m_gross*4.75/100 else 0 end esi_employr_contribution
from (
      select e.temp_emp_id, e.emp_bid, e.emp_name, la.dept_name, d.designation_name,
      (pay.basic+pay.other_allowance+incr+co_allowance+da+special_allowance+hra+cca)/12 m_gross,                            
       esi_consider
       from employee e LEFT JOIN employee_pay_details pay ON pay.emp_bid = e.emp_bid
       LEFT JOIN line_of_activity la on e.activity_id = la.activity_id
       LEFT JOIN designation d on e.designation = d.designation_id where $where) salary,
       std_esi_deductions esi
where m_gross between esi.min_limit and esi.max_limit
and esi_consider = 1
and m_gross >0 and m_gross <= 21000
order by length(emp_bid),emp_bid asc";*/
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
    }
    // old
	public function oldAllEmployeesESIData($year,$month)
    {
        $CI =& get_instance();
        $CI->load->model('Myfunctions');
        $prev_month_days = cal_days_in_month(CAL_GREGORIAN, date('m',strtotime($month)), $year);  
        $sql_query="select salary.*,
                        case when esi_consider = 1 then gross*esi.deduct_value/100
                             else 'No ESI Considered'
                        end esi_employee_contribution,
                        case when esi_consider = 1 then gross*4.75/100
                             else 'No ESI Considered'
                        end esi_employr_contribution
                        from (
                        select e.temp_emp_id, e.emp_bid, e.emp_name, la.dept_name, d.designation_name,
                        (pay.basic+pay.other_allowance+incr+co_allowance+da+special_allowance+hra+cca)/12 m_gross,    
                        case when ifnull(lc.lop_count,0) <= 0 then (pay.basic/12)
                             else (pay.basic/12)-((pay.basic/(12* ".$prev_month_days."))*lc.lop_count)
                        end +
                        case when ifnull(lc.lop_count,0) <= 0 then (pay.other_allowance/12)
                             else (pay.other_allowance/12)-((pay.other_allowance/(12* ".$prev_month_days."))*lc.lop_count)
                        end +
                        case when ifnull(lc.lop_count,0) <= 0 then (pay.incr/12)
                             else (pay.incr/12)-((pay.incr/(12* ".$prev_month_days."))*lc.lop_count)
                        end +
                        case when ifnull(lc.lop_count,0) <= 0 then (pay.co_allowance/12)
                             else (pay.co_allowance/12)-((pay.co_allowance/(12* ".$prev_month_days."))*lc.lop_count)
                        end +
                        case when ifnull(lc.lop_count,0) <= 0 then (pay.da/12)
                             else (pay.da/12)-((pay.da/(12* ".$prev_month_days."))*lc.lop_count)
                        end +
                        case when ifnull(lc.lop_count,0) <= 0 then (pay.special_allowance/12)
                             else (pay.special_allowance/12)-((pay.special_allowance/(12* ".$prev_month_days."))*lc.lop_count)
                        end +
                        case when ifnull(lc.lop_count,0) <= 0 then (pay.hra/12)
                             else (pay.hra/12)-((pay.hra/(12* ".$prev_month_days."))*lc.lop_count)
                        end +
                        case when ifnull(lc.lop_count,0) <= 0 then (pay.cca/12)
                             else (pay.cca/12)-((pay.cca/(12* ".$prev_month_days."))*lc.lop_count)
                        end gross,
                        esi_consider
                        from employee e LEFT JOIN employee_pay_details pay ON pay.emp_bid = e.emp_bid
                        LEFT JOIN emp_lop_calculation lc ON e.emp_bid = lc.emp_bid 
                                                  and lc.lop_month= ".(date('m',strtotime($month)))." 
                                                  AND lc.lop_year = ".$year."
                        LEFT JOIN line_of_activity la on e.activity_id = la.activity_id
                        LEFT JOIN designation d on e.designation = d.designation_id) salary,
                        std_esi_deductions esi
                        where m_gross between esi.min_limit and esi.max_limit
                        order by length(emp_bid),emp_bid asc";
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
    }
    // old
    public function oldEmployeeESIDataWhere($where,$year,$month)
    {
        $CI =& get_instance();
        $CI->load->model('Myfunctions');
        $prev_month_days = cal_days_in_month(CAL_GREGORIAN, date('m',strtotime($month)), $year);  
        $sql_query="select salary.*,
                        case when esi_consider = 1 then gross*esi.deduct_value/100
                             else 'No ESI Considered'
                        end esi_employee_contribution,
                        case when esi_consider = 1 then gross*4.75/100
                             else 'No ESI Considered'
                        end esi_employr_contribution
                        from (
                        select e.temp_emp_id, e.emp_bid, e.emp_name, la.dept_name, d.designation_name,
                        (pay.basic+pay.other_allowance+incr+co_allowance+da+special_allowance+hra+cca)/12 m_gross,
                        case when ifnull(lc.lop_count,0) <= 0 then (pay.basic/12)
                             else (pay.basic/12)-((pay.basic/(12* ".$prev_month_days."))*lc.lop_count)
                        end +
                        case when ifnull(lc.lop_count,0) <= 0 then (pay.other_allowance/12)
                             else (pay.other_allowance/12)-((pay.other_allowance/(12* ".$prev_month_days."))*lc.lop_count)
                        end +
                        case when ifnull(lc.lop_count,0) <= 0 then (pay.incr/12)
                             else (pay.incr/12)-((pay.incr/(12* ".$prev_month_days."))*lc.lop_count)
                        end +
                        case when ifnull(lc.lop_count,0) <= 0 then (pay.co_allowance/12)
                             else (pay.co_allowance/12)-((pay.co_allowance/(12* ".$prev_month_days."))*lc.lop_count)
                        end +
                        case when ifnull(lc.lop_count,0) <= 0 then (pay.da/12)
                             else (pay.da/12)-((pay.da/(12* ".$prev_month_days."))*lc.lop_count)
                        end +
                        case when ifnull(lc.lop_count,0) <= 0 then (pay.special_allowance/12)
                             else (pay.special_allowance/12)-((pay.special_allowance/(12* ".$prev_month_days."))*lc.lop_count)
                        end +
                        case when ifnull(lc.lop_count,0) <= 0 then (pay.hra/12)
                             else (pay.hra/12)-((pay.hra/(12* ".$prev_month_days."))*lc.lop_count)
                        end +
                        case when ifnull(lc.lop_count,0) <= 0 then (pay.cca/12)
                             else (pay.cca/12)-((pay.cca/(12* ".$prev_month_days."))*lc.lop_count)
                        end gross,
                        esi_consider
                        from employee e LEFT JOIN employee_pay_details pay ON pay.emp_bid = e.emp_bid
                        LEFT JOIN emp_lop_calculation lc ON e.emp_bid = lc.emp_bid 
                                                  and lc.lop_month= ".(date('m',strtotime($month)))." 
                                                  AND lc.lop_year = ".$year."
                        LEFT JOIN line_of_activity la on e.activity_id = la.activity_id
                        LEFT JOIN designation d on e.designation = d.designation_id where $where ) salary,
                        std_esi_deductions esi
                        where m_gross between esi.min_limit and esi.max_limit 
                        order by emp_bid asc";
                       // echo $sql_query;exit();
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
    }
	public function getSalariesData($where)
	{
		$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="select epd.*,e.education,e.designation,e.emp_id,e.date_of_join,e.emp_name,pt.payment_type, la.dept_name from employee_pay_details epd,employee e,payment_types pt, line_of_activity la where la.activity_id=e.activity_id and e.emp_bid=epd.emp_bid and epd.payment_type_id=pt.payment_type_id and ".$where;
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
	}
	public function activeEmployees()
	{
		$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="select e.*,loa.dept_name from employee e,line_of_activity loa where e.activity_id=loa.activity_id";
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
	}
	public function inActiveEmployees()
	{
		$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="select e.*,loa.dept_name from employee_log e,line_of_activity loa where e.activity_id=loa.activity_id";
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
	}
	public function newEmployees()
	{
		$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $d2 = date('Y-m-d', strtotime('-30 days'));
        $sql_query="select e.*,loa.dept_name from employee e,line_of_activity loa where e.activity_id=loa.activity_id and e.date>".$d2;
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
	}
	public function paymentModes()
	{
		$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="select payment_type from payment_types order by payment_type";
        $data=$CI->Myfunctions->getQueryDataList($sql_query);
        return $data;
	}
}
?>