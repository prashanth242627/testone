<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Holidays_model extends CI_Model
{
	
	public function __construct() {
        parent::__construct();
       
    }
    public function get_holidays_list($table, $where) 
    {
        $res=$this->db->get_where($table, $where);
        if($res->num_rows()> 0)
        {
            return $res->result();
        }
        else
        {
            return array();
        }
    }

    public function year_based_list($year) 
    {
        $res=$this->db->query("SELECT * from holidays where YEAR(from_date)='".$year."' order by Created_Datetime desc");
        if($res->num_rows()> 0)
        {
            return $res->result();
        }
        else
        {
            return array();
        }

    }
}
?>