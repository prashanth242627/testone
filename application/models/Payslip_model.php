<?php
class Payslip_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct(); 
    }
    

    public function EmpPaySlipByWhere($where,$month,$year)
    {
        $CI =& get_instance();
        $CI->load->model('Myfunctions');
        $month_days=cal_days_in_month(CAL_GREGORIAN,date('m',strtotime($month)),$year);
        if ($month_days>30) {
          $month_days=30;
        }
        $sql_query="select e.temp_emp_id temp_emp_id, e.emp_bid, e.emp_name, e.date_of_join, d.designation_name,
       l.dept_name, pay.acc_no, pmttyp.payment_type payment_mode,pay.uan, pay.pf_no, 
       pay.esi_no,pay.PAN, pay.Aadhar,
       slry.lop_count, slry.pay_days as PAY_DAYS,slry.TWD,
       slry.basic_ctc, slry.basic_earnings, 
       slry.incr_ctc, slry.incr_earnings, 
       slry.da_ctc, slry.da_earnings, 
       slry.hra_ctc, slry.hra_earnings,
       slry.cca_ctc, slry.cca_earnings, 
       slry.other_allowance_ctc, slry.other_allowance_earnings as other_allowances_earnings, 
       slry.co_allowance_ctc, slry.co_allowance_earnings,
       slry.special_allowance_ctc, slry.special_allowance_earnings, 
       slry.management_deductions,
       slry.pf as PF,slry.pt PT,
       slry.esi as ESI, slry.tds as TDS,slry.sd as SD, 
       slry.gross, slry.net_income as net_income, slry.fixed_monthly_ctc as FIXED_MONTHLY_CTC      
from employee e LEFT JOIN designation d on (e.designation = d.designation_id)
     LEFT JOIN line_of_activity l on (e.activity_id = l.activity_id)
     LEFT JOIN employee_pay_details pay on (e.emp_bid = pay.emp_bid)
     LEFT JOIN payment_types pmttyp on pay.payment_type_id = pmttyp.payment_type_id
     LEFT JOIN (select * 
                from emp_monthly_salaries
                where salary_month = ".date('m',strtotime($month))."
                and salary_year = $year) slry  on (e.emp_bid = slry.emp_bid) where $where";
        $employees=$CI->Myfunctions->getQueryData($sql_query);
        return $employees;
    }
    public function PaySlipTDSDetails($where,$month,$year)
    {
      $CI =& get_instance();
        $CI->load->model('Myfunctions');
      $month=date('m',strtotime($month));
        if ($month>=4 && $month<=12) {
            $a_year=$year;
            $b_year=$year+1;
        }
        else if ($month>=1 && $month<=3) {
           $a_year=$year;
            $b_year=$year-1;
        }
      $sql_query="select p.*, ifnull(sm.sec_80c,0) sec_80c, ifnull(sm.sec_80d,0) sec_80d,
case when ifnull(sm.sec_80c,0) > 150000 then 150000 else ifnull(sm.sec_80c,0) END 80C_savings_amt,
case when ifnull(sm.sec_80d,0) > 25000 then 25000 else ifnull(sm.sec_80d,0) end 80D_savings_amt,
150000 80C_exmpt, 25000 80D_exmpt, pt.deduct_value*12 PT_AMT,
case when (taxable_amt-(pt.deduct_value*12)-175000) > 0 then (taxable_amt-(pt.deduct_value*12)-175000) else 0 end taxable_income,
case when (taxable_amt-(pt.deduct_value*12)-175000-ifnull(te.exmp_limit,0)) > 0 then 
           (taxable_amt-(pt.deduct_value*12)-175000-ifnull(te.exmp_limit,0))*td.deduct_value/100 else 0 end tax_amt,
case when (taxable_amt-(pt.deduct_value*12)-175000-ifnull(te.exmp_limit,0)) > 0 then 
          ((taxable_amt-pt.deduct_value*12-175000-ifnull(te.exmp_limit,0))*td.deduct_value/100 )*3/100 else 0 end surcharges,
case when (taxable_amt-(pt.deduct_value*12)-175000-ifnull(te.exmp_limit,0)) > 0 then 
          ((taxable_amt-pt.deduct_value*12-175000-ifnull(te.exmp_limit,0))*td.deduct_value/100) else 0 end projected_tax_per_month,
ifnull(tds_recv.tds_recovered,0) tds_deducted_till_date, 
case when (taxable_amt-(pt.deduct_value*12)-175000-ifnull(te.exmp_limit,0)) >0 then 
          (taxable_amt-(pt.deduct_value*12)-175000-ifnull(te.exmp_limit,0))*td.deduct_value/100 - ifnull(tds_recv.tds_recovered,0) else 0 end           tds_to_be_deducted
from 
(select pay.emp_bid, e.sex, basic, da, hra, cca conveyance_allowance, other_allowance, 
(incr+special_allowance+co_allowance) other_cost_cpmponents,
(basic+da)*0.1 hra_exmpt, convy.max_imit convy_exmpt,
case when hra > (basic+da)*0.1 then hra-(basic+da)*0.1 else 0 end as hra_tax_amt,
case when cca > convy.max_imit then convy.max_imit else 0 end convy_tax_amt,
basic+da+hra+cca+incr+other_allowance+co_allowance+special_allowance gross,
basic+da+case when hra > (basic+da)*0.1 then hra-(basic+da)*0.1 else 0 end+
case when cca > convy.max_imit then convy.max_imit else 0 end+other_allowance+
(incr+special_allowance+co_allowance) as taxable_amt
from employee_pay_details pay, 
     employee e, 
     std_conveyance_exemption convy
where e.emp_bid = pay.emp_bid 
) p
LEFT JOIN empsavingsmaster sm on p.emp_bid = sm.emp_bid
LEFT JOIN std_pt_deductions pt on gross BETWEEN pt.min_limit and pt.max_limit
LEFT JOIN std_tds_exemption te on p.sex = te.sex
LEFT JOIN std_tds_deductions td on (taxable_amt-pt.deduct_value*12-175000-ifnull(te.exmp_limit,0)) between td.min_limit and

td.max_limit
LEFT JOIN (select td.emp_bid, sum(td.tds_amt) tds_recovered
           from (select emp_bid, SUM(TDS) tds_amt from emp_monthly_salaries
                 where salary_month between 4 and 12 and salary_year = $b_year
                 group by emp_bid
                 UNION ALL
                 select emp_bid, SUM(TDS) tds_amt from emp_monthly_salaries
                 where salary_month between 1 and 3 and salary_year = $a_year
                  group by emp_bid) td
           group by td.emp_bid) tds_recv on p.emp_bid = tds_recv.emp_bid where $where";
           //echo $sql_query;exit();
           $employees=$CI->Myfunctions->getQueryData($sql_query);
        return $employees;
    }
    public function PaySlipTDSDetailso($where,$month,$year)
    {
      $CI =& get_instance();
        $CI->load->model('Myfunctions');
      $month=date('m',strtotime($month));
        if ($month>=4 && $month<=12) {
            $a_year=$year;
            $b_year=$year+1;
        }
        else if ($month>=1 && $month<=3) {
           $a_year=$year;
            $b_year=$year-1;
        }
      $sql_query="select p.*, sm.sec_80c, sm.sec_80d,case when sm.sec_80c > 150000 then 150000 else sm.sec_80c END 80C_savings_amt,
case when sm.sec_80d > 25000 then 25000 else sm.sec_80d end 80D_savings_amt,
150000 80C_exmpt, 25000 80D_exmpt, pt.deduct_value*12 PT_AMT,
(taxable_amt-pt.deduct_value*12-175000) taxable_income,
(taxable_amt-pt.deduct_value*12-175000-te.exmp_limit)*td.deduct_value/100 tax_amt,
((taxable_amt-pt.deduct_value*12-175000-te.exmp_limit)*td.deduct_value/100 )*3/100 surcharges,
((taxable_amt-pt.deduct_value*12-175000-te.exmp_limit)*td.deduct_value/100) projected_tax_per_month,
tds_recv.tds_recovered tds_deducted_till_date, 
(taxable_amt-pt.deduct_value*12-175000-te.exmp_limit)*td.deduct_value/100 - tds_recv.tds_recovered tds_to_be_deducted
from 
(select pay.emp_bid, e.sex, basic, da, hra, cca conveyance_allowance, other_allowance, 
(incr+special_allowance+co_allowance) other_cost_cpmponents,
(basic+da)*0.1 hra_exmpt, convy.max_imit convy_exmpt,
case when hra > (basic+da)*0.1 then hra-(basic+da)*0.1 else 0 end as hra_tax_amt,
case when cca > convy.max_imit then convy.max_imit else 0 end convy_tax_amt,
basic+da+hra+cca+incr+other_allowance+co_allowance+special_allowance gross,
basic+da+case when hra > (basic+da)*0.1 then hra-(basic+da)*0.1 else 0 end+
case when cca > convy.max_imit then convy.max_imit else 0 end+other_allowance+
(incr+special_allowance+co_allowance) as taxable_amt
from employee_pay_details pay, 
     employee e, 
     std_conveyance_exemption convy
where e.emp_bid = pay.emp_bid
and $where) p
LEFT JOIN empsavingsmaster sm on p.emp_bid = sm.emp_bid
LEFT JOIN std_pt_deductions pt on gross BETWEEN pt.min_limit and pt.max_limit
LEFT JOIN std_tds_exemption te on p.sex = te.sex
LEFT JOIN std_tds_deductions td on (taxable_amt-pt.deduct_value*12-175000-te.exmp_limit) between td.min_limit and
td.max_limit
LEFT JOIN (select td.emp_bid, sum(td.tds_amt) tds_recovered
           from (select emp_bid, SUM(TDS) tds_amt from emp_monthly_salaries
                 where salary_month between 4 and 12 and salary_year = $a_year
                 group by emp_bid
                 UNION ALL
                 select emp_bid, SUM(TDS) tds_amt from emp_monthly_salaries
                 where salary_month between 1 and 3 and salary_year = $b_year
                  group by emp_bid) td
            group by td.emp_bid) tds_recv on p.emp_bid = tds_recv.emp_bid";
           $employees=$CI->Myfunctions->getQueryData($sql_query);
        return $employees;
    }
    public function taxPaid($where,$month,$year)
    {
      $CI =& get_instance();
        $CI->load->model('Myfunctions');
      $month=date('m',strtotime($month));
        if ($month>=4 && $month<=12) {
            $a_year=$year;
            $b_year=$year+1;
        }
        else if ($month>=1 && $month<=3) {
           $a_year=$year;
            $b_year=$year-1;
        }
      $sql_query="select salary_month, TDS tds_amt from emp_monthly_salaries where salary_month between 4 and 12 and salary_year = $b_year and $where UNION ALL select salary_month, TDS tds_amt from emp_monthly_salaries where salary_month between 1 and 3 and salary_year = $a_year and $where";
      $employees=$CI->Myfunctions->getQueryData($sql_query);
        return $employees;
    }
    public function oldEmpPaySlipByWhere($where,$month,$year)
    {
        $CI =& get_instance();
        $CI->load->model('Myfunctions');
        $month_days=cal_days_in_month(CAL_GREGORIAN,date('m',strtotime($month)),$year);
        if ($month_days>30) {
          $month_days=30;
        }
        $sql_query="select monsal.*, gross-(PF+ESI+PT+SD+tds) net_income
from(
select temp_emp_id, 
       emp_name, 
       sex Gender, 
       designation_name, 
       dept_name,
       date_of_join, 
       acc_no, 
       pf_no, 
       esi_no, 
       uan,
       Aadhar, 
       PAN,
       ".$month_days." as TWD, 
       lop_count , 
       ".$month_days."-lop_count PAY_DAYS,
       emp_bid,
       basic_ctc, basic_earnings, 
       da_ctc, da_earnings,
       incr_ctc,incr_earnings,
       hra_ctc, hra_earnings,
       cca_ctc, cca_earnings,
       other_allowance_ctc, other_allowances_earnings, 
       co_allowance_ctc, co_allowance_earnings, 
       special_allowance_ctc, special_allowance_earnings,
       m_gross FIXED_MONTHLY_CTC,
       exmp_limit, savings,
       gross, 
case when pf_consider = 1 then 
    case when (basic_earnings+da_earnings)* (select deduct_value from std_pf_deductions)/100 <=1800 
          then round((basic_earnings+da_earnings)*(select deduct_value from std_pf_deductions)/100) else 1800 END
         else 0
end PF,
case when esi_consider = 1 then round((gross)*esi.deduct_value/100)
     else 0
end ESI,
pt.deduct_value PT,
case when m_gross < (exmp_limit+savings) then 0 
     else round(((m_gross-(exmp_limit+savings))*tds.deduct_value/100)) 
END TDS,
case when (sdscope = 1 and DATE_ADD(NOW(), INTERVAL -10 MONTH) > date_of_join) then round(gross*10/100)
     else 0
end SD
from(
select salary.*,
(salary.basic_earnings+salary.other_allowances_earnings+salary.incr_earnings+salary.co_allowance_earnings+salary.da_earnings+
 salary.special_allowance_earnings+salary.hra_earnings+salary.cca_earnings) gross
from (
select e.temp_emp_id, e.emp_name, e.sex, d.designation_name, loa.dept_name, e.emp_bid, e.date_of_join,
    pay.acc_no,pay.esi_no, pay.pf_no, pay.uan, pay.Aadhar, pay.PAN,
    pay.basic basic_ctc, pay.da da_ctc, pay.hra hra_ctc, pay.incr incr_ctc, pay.other_allowance other_allowance_ctc, pay.co_allowance co_allowance_ctc,
    pay.special_allowance special_allowance_ctc, pay.cca cca_ctc,
    (pay.basic+pay.other_allowance+incr+co_allowance+da+special_allowance+hra+cca) m_gross,    
round(case when ifnull(lc.lop_count,0) <= 0 then (pay.basic)
     else (pay.basic)-((pay.basic/(".$month_days."))*lc.lop_count)
end) basic_earnings,     
round(case when ifnull(lc.lop_count,0) <= 0 then (pay.other_allowance)
     else (pay.other_allowance)-((pay.other_allowance/(".$month_days."))*lc.lop_count)
end) other_allowances_earnings,     
round(case when ifnull(lc.lop_count,0) <= 0 then (pay.incr)
     else (pay.incr)-((pay.incr/(".$month_days."))*lc.lop_count)
end) incr_earnings,
round(case when ifnull(lc.lop_count,0) <= 0 then (pay.co_allowance)
     else (pay.co_allowance)-((pay.co_allowance/(".$month_days."))*lc.lop_count)
end) co_allowance_earnings,
round(case when ifnull(lc.lop_count,0) <= 0 then (pay.da)
     else (pay.da)-((pay.da/(".$month_days."))*lc.lop_count)
end) da_earnings,
round(case when ifnull(lc.lop_count,0) <= 0 then (pay.special_allowance)
     else (pay.special_allowance)-((pay.special_allowance/(".$month_days."))*lc.lop_count)
end) special_allowance_earnings,
round(case when ifnull(lc.lop_count,0) <= 0 then (pay.hra)
     else (pay.hra)-((pay.hra/(".$month_days."))*lc.lop_count)
end) hra_earnings,
round(case when ifnull(lc.lop_count,0) <= 0 then (pay.cca)
     else (pay.cca)-((pay.cca/(".$month_days."))*lc.lop_count)
end) cca_earnings,
esi_consider,is_pay_hold,pf_consider,sdscope,lc.lop_count, te.exmp_limit, 
    CASE when lc.lop_month between 4 and 12 then 180000 
         else (case when sm.sec_80c<150000 then sm.sec_80c else 150000 END
              +case when sm.sec_80d<25000 then sm.sec_80d else 25000 END
              +case when sm.sec_80cc<25000 then sm.sec_80cc else 25000 END) 
    end savings
from employee e LEFT JOIN employee_pay_details pay ON pay.emp_bid = e.emp_bid
LEFT JOIN designation d on e.designation = d.designation_id
LEFT JOIN line_of_activity loa on loa.activity_id=e.activity_id
LEFT JOIN std_tds_exemption te on case when e.sex='' then 'M' else e.sex END = te.sex
LEFT JOIN empsavingsmaster sm on e.emp_bid = sm.emp_bid
LEFT JOIN emp_lop_calculation lc ON e.emp_bid = lc.emp_bid 
                          and lc.lop_month=".date('m',strtotime($month))." 
                          and lc.lop_year = $year where $where) salary
) mon_sal
LEFT JOIN std_tds_deductions tds on 
   (case when (m_gross-(exmp_limit+savings)) > 0 then (m_gross-(exmp_limit+savings)) 
        else 0 end)  between tds.min_limit and tds.max_limit
LEFT JOIN std_esi_deductions esi on round(m_gross) between esi.min_limit and esi.max_limit
LEFT JOIN std_pt_deductions pt on gross between pt.min_limit and pt.max_limit)monsal";
        $employees=$CI->Myfunctions->getQueryData($sql_query);
        return $employees;
    }
   public function getIndianCurrency($number)
  {
      $decimal = round($number - ($no = floor($number)), 2) * 100;
      $hundred = null;
      $digits_length = strlen($no);
      $i = 0;
      $str = array();
      $words = array(0 => '', 1 => 'one', 2 => 'two',
          3 => 'three', 4 => 'four', 5 => 'five', 6 => 'six',
          7 => 'seven', 8 => 'eight', 9 => 'nine',
          10 => 'ten', 11 => 'eleven', 12 => 'twelve',
          13 => 'thirteen', 14 => 'fourteen', 15 => 'fifteen',
          16 => 'sixteen', 17 => 'seventeen', 18 => 'eighteen',
          19 => 'nineteen', 20 => 'twenty', 30 => 'thirty',
          40 => 'forty', 50 => 'fifty', 60 => 'sixty',
          70 => 'seventy', 80 => 'eighty', 90 => 'ninety');
      $digits = array('', 'hundred','thousand','lakh', 'crore');
      while( $i < $digits_length ) {
          $divider = ($i == 2) ? 10 : 100;
          $number = floor($no % $divider);
          $no = floor($no / $divider);
          $i += $divider == 10 ? 1 : 2;
          if ($number) {
              $plural = (($counter = count($str)) && $number > 9) ? '' : null;
              $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
              $str [] = ($number < 21) ? $words[$number].' '. $digits[$counter]. $plural.' '.$hundred:$words[floor($number / 10) * 10].' '.$words[$number % 10]. ' '.$digits[$counter].$plural.' '.$hundred;
          } else $str[] = null;
      }
      $Rupees = implode('', array_reverse($str));
      $paise = ($decimal) ? "." . ($words[$decimal / 10] . " " . $words[$decimal % 10]) . ' Paise' : '';
      return ($Rupees ? $Rupees . 'Rupees ' : '') . $paise ;
  }
}
?>