<?php
class Employee_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct(); 
	}
	public function allEmployees()
	{
		$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="select e.*,loa.dept_name,loa.from_time,loa.to_time,d.designation_name, td.transport_name from employee e,line_of_activity loa,designation d,transport_details td where d.designation_id=e.designation and e.`transport_details_id`=td.transport_details_id  and e.activity_id=loa.activity_id";
        $sql_query1="select e.*,loa.dept_name,loa.from_time,loa.to_time,d.designation_name, td.transport_name from employee_log e,line_of_activity loa,designation d,transport_details td where d.designation_id=e.designation and e.`transport_details_id`=td.transport_details_id  and  e.flag=2 and e.activity_id=loa.activity_id";
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
       // print_r($employees);exit();
        $employees_log=$CI->Myfunctions->getQueryDataList($sql_query1);
        foreach ($employees_log as $employee_log) {
        	array_push($employees, $employee_log);
        }
        return $employees;
	}
	public function activities()
	{
		$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $employees=$CI->Myfunctions->getDataList("line_of_activity","");
        return $employees;
	}
	public function employeesBid()
	{
		$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="select emp_bid from employee order by LENGTH(emp_bid),emp_bid asc";
        $employees_bid=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees_bid;
	}
	public function employeesName()
	{
		$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="select emp_name from employee order by emp_name asc";
        $employees_name=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees_name;
	}
	public function employeeData($where)
	{
		$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="select e.*,loa.dept_name,loa.from_time,loa.to_time,d.designation_name, td.transport_name from employee e LEFT JOIN line_of_activity loa ON e.activity_id=loa.activity_id LEFT JOIN designation d on d.designation_id=e.designation LEFT JOIN transport_details td on e.`transport_details_id`=td.transport_details_id where $where order by e.emp_bid";
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
	}
	public function lineOfActivityEmployees($type)
	{
		$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="select e.*,loa.dept_name,loa.from_time,loa.to_time,d.designation_name, td.transport_name from employee e LEFT JOIN line_of_activity loa ON e.activity_id=loa.activity_id LEFT JOIN designation d on d.designation_id=e.designation LEFT JOIN transport_details td on e.`transport_details_id`=td.transport_details_id where loa.dept_name='".$type."' order by e.emp_bid";
       $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
	}
        public function lineOfActivityEmployeesAndStatus($type,$status)
        {
        $CI =& get_instance();
        $CI->load->model('Myfunctions');
        if ($status=="New") {
                $d2 = date('Y-m-d', strtotime('-30 days'));
               $sql_query="select e.*,loa.dept_name,loa.from_time,loa.to_time,d.designation_name, td.transport_name from employee e,line_of_activity loa,designation d,transport_details td where loa.dept_name='".$type."' and e.activity_id=loa.activity_id and e.`transport_details_id` = td.transport_details_id and e.date_of_join >'".$d2."'  and d.designation_id=e.designation order by e.emp_bid";
        }
        else if ($status=="Inactive") {
                $sql_query="select e.*,loa.dept_name,loa.from_time,loa.to_time,d.designation_name, td.transport_name from employee_log e,line_of_activity loa,designation d,transport_details td where loa.dept_name='".$type."' and e.activity_id=loa.activity_id and e.`transport_details_id`=td.transport_details_id  and d.designation_id=e.designation order by e.emp_bid";
        }
        else{
                $sql_query="select e.*,loa.dept_name,loa.from_time,loa.to_time,d.designation_name, td.transport_name from employee e,line_of_activity loa,designation d,transport_details td where loa.dept_name='".$type."' and e.activity_id=loa.activity_id and e.`transport_details_id`=td.transport_details_id  and d.designation_id=e.designation order by e.emp_bid";
        }
       $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
        }
	public function activeEmployees()
	{
	$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="select e.*,loa.dept_name,loa.from_time,loa.to_time,d.designation_name,td.transport_name from employee e,line_of_activity loa,designation d,transport_details td where e.activity_id=loa.activity_id and d.designation_id=e.designation and e.`transport_details_id`=td.transport_details_id order by e.emp_bid";
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
	}
	public function inActiveEmployees()
	{
		$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="select e.*,loa.dept_name,loa.from_time,loa.to_time,d.designation_name,td.transport_name from employee_log e,line_of_activity loa, designation d, transport_details td where e.designation=d.designation_id and e.activity_id=loa.activity_id and e.`transport_details_id`=td.transport_details_id  and e.flag=2 order by e.emp_bid";
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
	}
	public function newEmployees()
	{
	$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $d2 = date('Y-m-d', strtotime('-30 days'));
        $sql_query="select e.*,loa.dept_name,loa.from_time,loa.to_time, d.designation_name,td.transport_name from employee e,line_of_activity loa,designation d,transport_details td where e.activity_id=loa.activity_id and d.designation_id=e.designation and e.`transport_details_id`=td.transport_details_id  and e.date_of_join >'".$d2."' order by e.emp_bid";
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
	}
	public function getEmployeesData($where)
	{
		$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="select e.*,loa.dept_name,loa.from_time,loa.to_time,d.designation_name,td.transport_name from employee e,line_of_activity loa,designation d,transport_details td where e.activity_id=loa.activity_id and d.designation_id=e.designation and e.`transport_details_id`=td.transport_details_id  and ".$where." order by e.emp_bid";
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
	}
}
?>