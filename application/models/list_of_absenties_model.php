<?php
class list_of_absenties_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct(); 
	}
	public function allEmployees()
	{
		$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="select e.*,loa.dept_name from employee e,line_of_activity loa where e.activity_id=loa.activity_id";
        $sql_query1="select e.*,loa.dept_name from employee_log e,line_of_activity loa where e.activity_id=loa.activity_id";
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        $employees_log=$CI->Myfunctions->getQueryDataList($sql_query1);
        foreach ($employees_log as $employee_log) {
        	array_push($employees, $employee_log);
        }
        return $employees;
	}
	public function activities()
	{
		$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $employees=$CI->Myfunctions->getDataList("line_of_activity","");
        return $employees;
	}
	public function employeesBid()
	{
		$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="select emp_bid from employee order by emp_bid asc";
        $employees_bid=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees_bid;
	}
	public function employeesName()
	{
		$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="select emp_name from employee order by emp_name asc";
        $employees_name=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees_name;
	}
	public function employeeData($where)
	{
		$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="select e.*,loa.dept_name from employee e,line_of_activity loa where e.activity_id=loa.activity_id and ".$where;
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
	}
	public function lineOfActivityEmployees($type)
	{
		$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="SELECT e.emp_id,e.emp_bid,e.emp_name,d.designation_name,loa.dept_name,lm.no_leaves,lm.leave_master_id, lt.leave_type, COUNT(am.attendance_id) as used_count FROM line_of_activity loa,designation d, employee e, `leave_master` lm JOIN leave_types lt on lm.leave_type_id =lt.leave_type_id LEFT JOIN attendance_marking am on am.leave_master_id=lm.leave_master_id WHERE e.emp_id= am.emp_id and e.designation=d.designation_id and e.activity_id=loa.activity_id and loa.dept_name='".$type."' and  lm.calendar_year=2018  GROUP BY am.emp_id,am.leave_master_id ";
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
	}
	public function activeEmployees()
	{
		$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="select e.*,loa.dept_name from employee e,line_of_activity loa where e.activity_id=loa.activity_id";
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
	}
	public function inActiveEmployees()
	{
		$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="select e.*,loa.dept_name from employee_log e,line_of_activity loa where e.activity_id=loa.activity_id";
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
	}
	public function newEmployees()
	{
		$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $d2 = date('Y-m-d', strtotime('-30 days'));
        $sql_query="select e.*,loa.dept_name from employee e,line_of_activity loa where e.activity_id=loa.activity_id and e.date>".$d2;
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
	}
	public function getEmployeesData($where)
	{
		$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="select e.*,loa.dept_name from employee e, line_of_activity loa where e.activity_id=loa.activity_id and ".$where;
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
	}
	public function leaveType()
	{
		$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="select * from leave_types order by leave_type";
        $leave_types=$CI->Myfunctions->getQueryDataList($sql_query);
        return $leave_types;
	}
	public function noOfLeaves()
	{
		$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="SELECT e.emp_id,e.emp_bid,e.emp_name,d.designation_name,loa.dept_name,lm.no_leaves,lm.leave_master_id, lt.leave_type, COUNT(am.attendance_id) as used_count FROM line_of_activity loa,designation d, employee e, `leave_master` lm JOIN leave_types lt on lm.leave_type_id =lt.leave_type_id LEFT JOIN attendance_marking am on am.leave_master_id=lm.leave_master_id WHERE e.emp_id= am.emp_id and e.designation=d.designation_id and e.activity_id=loa.activity_id and  lm.calendar_year=2018  GROUP BY am.emp_id,am.leave_master_id ";
        $leave_types=$CI->Myfunctions->getQueryDataList($sql_query);
        return $leave_types;
	}
        public function onDutyEmployees()
        {
                $CI =& get_instance();
                $CI->load->model('Myfunctions');
                $sql_query="SELECT e.emp_id,e.emp_bid,e.emp_name,d.designation_name,am.leave_date,loa.dept_name,lm.leave_master_id FROM line_of_activity loa,designation d, employee e, `leave_master` lm LEFT JOIN attendance_marking am on am.leave_master_id=lm.leave_master_id WHERE lm.leave_type_id =6 and e.emp_id= am.emp_id and e.designation=d.designation_id and e.activity_id=loa.activity_id and lm.calendar_year=2018 GROUP BY am.emp_id,am.leave_master_id";
                $onDutyEmployees=$CI->Myfunctions->getQueryDataList($sql_query);
                return $onDutyEmployees;
        }
        public function noOfLeavesByWhere($where)
        {
                $CI =& get_instance();
                $CI->load->model('Myfunctions');
                $sql_query="SELECT e.emp_id,e.emp_bid,e.emp_name,d.designation_name,loa.dept_name,lm.no_leaves,lm.leave_master_id, lt.leave_type, COUNT(am.attendance_id) as used_count FROM line_of_activity loa,designation d, employee e, `leave_master` lm JOIN leave_types lt on lm.leave_type_id =lt.leave_type_id LEFT JOIN attendance_marking am on am.leave_master_id=lm.leave_master_id WHERE e.emp_id= am.emp_id and e.designation=d.designation_id and e.activity_id=loa.activity_id and $where and  lm.calendar_year=2018  GROUP BY am.emp_id,am.leave_master_id ";
                $noOfLeavesByWhere=$CI->Myfunctions->getQueryDataList($sql_query);
                return $noOfLeavesByWhere;
                //echo $sql_query;
        }
}
?>