<?php
class Savings_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct(); 
	}
    public function AllEmployeesSavingsData()
    {
        $CI =& get_instance();
        $CI->load->model('Myfunctions');
        $lastcalmonthandyear=$CI->Myfunctions->getQueryData("SELECT salary_month,salary_year from emp_monthly_salaries WHERE salary_year=(SELECT MAX(salary_year) from emp_monthly_salaries) GROUP by salary_month ORDER by salary_month desc LIMIT 1");
        $month=$lastcalmonthandyear[0]['salary_month'];
        $year=$lastcalmonthandyear[0]['salary_year'];
        if ($month>=4 && $month<=12) {
            $a_year=$year;
            $b_year=$year+1;
        }
        else if ($month>=1 && $month<=3) {
           $a_year=$year;
            $b_year=$year-1;
        }
         $sql_query="select tds_cal.emp_id as temp_emp_id, 
       tds_cal.emp_bid, 
       tds_cal.emp_name, 
       tds_cal.date_of_join, 
       tds_cal.designation_name, 
       tds_cal.dept_name, 
       tds_cal.CTC, 
       (case when tds_cal.tax_amt > 0 then tds_cal.tax_amt else 0 end) taxable_amount,
       tds_cal.sec_80c, tds_cal.sec_80d, tds_cal.sec_80cc, 
       (case when aftr_saving > 0 then aftr_saving else 0 end) tax_amt_aftr_savings,
       (case when aftr_saving > 0 then aftr_saving else 0 end)*(tdsd.deduct_value/100) tds_per_anm,
       ifnull(tds_recv.tds_recovered,0) as tds_recovered,
       ((case when aftr_saving > 0 then aftr_saving else 0 end)*(tdsd.deduct_value/100) - ifnull(tds_recv.tds_recovered,0)) tds_balance
from (select e.temp_emp_id emp_id, e.emp_bid, e.emp_name, e.date_of_join, d.designation_name,l.dept_name, 
            (basic+cca+da+hra+incr+ other_allowance+co_allowance+special_allowance) as CTC,
            round((basic+cca+da+hra+incr+ other_allowance+co_allowance+special_allowance)- te.exmp_limit,2) tax_amt,
            ifnull(sm.sec_80c,0) sec_80c, 
            ifnull(sm.sec_80d,0) sec_80d, 
            ifnull(sm.sec_80cc,0) sec_80cc,
            round((basic+(case when cca> 0 then 19200-cca else 0 end)+da+(hra-(basic+da)*10/100)+incr+ other_allowance+co_allowance+special_allowance)- (te.exmp_limit+175000),2)  aftr_saving
      from employee e LEFT JOIN designation d on (e.designation = d.designation_id)
           LEFT JOIN line_of_activity l on (e.activity_id = l.activity_id)     
           LEFT JOIN employee_pay_details slry  on (e.emp_bid = slry.emp_bid)
          LEFT JOIN empsavingsmaster sm on e.emp_bid = sm.emp_bid         
         LEFT JOIN std_tds_exemption te on case when e.sex='' then 'M' else e.sex END = te.sex
) tds_cal 
LEFT JOIN std_tds_deductions tdsd ON (case when tds_cal.aftr_saving > 0 then tds_cal.aftr_saving else 0 end) BETWEEN tdsd.min_limit and tdsd.max_limit
LEFT JOIN (select td.emp_bid, sum(td.tds_amt) tds_recovered
           from (select emp_bid, SUM(TDS) tds_amt from emp_monthly_salaries
                 where salary_month between 4 and 12 and salary_year = $b_year
                 group by emp_bid
                 UNION ALL
                 select emp_bid, SUM(TDS) tds_amt from emp_monthly_salaries
                 where salary_month between 1 and 3 and salary_year = $a_year
                  group by emp_bid) td
           group by td.emp_bid
          ) tds_recv  on (tds_cal.emp_bid = tds_recv.emp_bid) 
order by length(tds_cal.emp_bid),tds_cal.emp_bid";
        /*$sql_query="select tds_cal.emp_id as temp_emp_id, tds_cal.emp_bid, tds_cal.emp_name, tds_cal.date_of_join, tds_cal.designation_name, tds_cal.dept_name, tds_cal.CTC, 
(case when tds_cal.tax_amt > 0 then tds_cal.tax_amt else 0 end) taxable_amount,
tds_cal.sec_80c, tds_cal.sec_80d, tds_cal.sec_80cc, 
(case when aftr_saving > 0 then aftr_saving else 0 end) tax_amt_aftr_savings,
(case when aftr_saving > 0 then aftr_saving else 0 end)*(tdsd.deduct_value/100) tds_per_anm,
tds_recv.tds_recovered,
((case when aftr_saving > 0 then aftr_saving else 0 end)*(tdsd.deduct_value/100) - tds_recv.tds_recovered) tds_balance
from (
select e.temp_emp_id emp_id, e.emp_bid, e.emp_name, e.date_of_join, d.designation_name,l.dept_name, 
       (basic_ctc+cca_ctc+da_ctc+hra_ctc+incr_ctc+ other_allowance_ctc+co_allowance_ctc+special_allowance_ctc)*12 as CTC,
(basic_ctc+cca_ctc+da_ctc+hra_ctc+incr_ctc+ other_allowance_ctc+co_allowance_ctc+special_allowance_ctc)*12 - slry.exmp_limit tax_amt,
sm.sec_80c, sm.sec_80d, sm.sec_80cc,
slry.savings, (basic_ctc+cca_ctc+da_ctc+hra_ctc+incr_ctc+ other_allowance_ctc+co_allowance_ctc+special_allowance_ctc)*12 - (slry.exmp_limit+slry.savings)  aftr_saving
from employee e LEFT JOIN designation d on (e.designation = d.designation_id)
     LEFT JOIN line_of_activity l on (e.activity_id = l.activity_id)     
     LEFT JOIN (select * 
                from emp_monthly_salaries
                where salary_month = $month
                and salary_year = $a_year) slry  on (e.emp_bid = slry.emp_bid)
     LEFT JOIN empsavingsmaster sm on e.emp_bid = sm.emp_bid                
) tds_cal 
LEFT JOIN std_tds_deductions tdsd ON (case when tds_cal.aftr_saving > 0 then tds_cal.aftr_saving else 0 end) BETWEEN tdsd.min_limit and tdsd.max_limit
LEFT JOIN (select td.emp_bid, sum(td.tds_amt) tds_recovered
           from (select emp_bid, SUM(TDS) tds_amt from emp_monthly_salaries
                 where salary_month between 4 and 12 and salary_year = $b_year
                 group by emp_bid
                 UNION ALL
                 select emp_bid, SUM(TDS) tds_amt from emp_monthly_salaries
                 where salary_month between 1 and 3 and salary_year = $a_year
                  group by emp_bid) td
           group by td.emp_bid
          ) tds_recv  on (tds_cal.emp_bid = tds_recv.emp_bid) order by length(tds_cal.emp_bid),tds_cal.emp_bid";*/
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
    }
    //new
    public function EmployeeSavingsDataWhere($where)
    {
        $CI =& get_instance();
        $CI->load->model('Myfunctions');
        $lastcalmonthandyear=$CI->Myfunctions->getQueryData("SELECT salary_month,salary_year from emp_monthly_salaries WHERE salary_year=(SELECT MAX(salary_year) from emp_monthly_salaries) GROUP by salary_month ORDER by salary_month desc LIMIT 1");
        $month=$lastcalmonthandyear[0]['salary_month'];
        $year=$lastcalmonthandyear[0]['salary_year'];
        if ($month>=4 && $month<=12) {
            $a_year=$year;
            $b_year=$year+1;
        }
        else if ($month>=1 && $month<=3) {
           $a_year=$year;
            $b_year=$year-1;
        }
        $sql_query="select tds_cal.emp_id as temp_emp_id, 
       tds_cal.emp_bid, 
       tds_cal.emp_name, 
       tds_cal.date_of_join, 
       tds_cal.designation_name, 
       tds_cal.dept_name, 
       tds_cal.CTC, 
       (case when tds_cal.tax_amt > 0 then tds_cal.tax_amt else 0 end) taxable_amount,
       tds_cal.sec_80c, tds_cal.sec_80d, tds_cal.sec_80cc, 
       (case when aftr_saving > 0 then aftr_saving else 0 end) tax_amt_aftr_savings,
       (case when aftr_saving > 0 then aftr_saving else 0 end)*(tdsd.deduct_value/100) tds_per_anm,
       ifnull(tds_recv.tds_recovered,0) as tds_recovered,
       ((case when aftr_saving > 0 then aftr_saving else 0 end)*(tdsd.deduct_value/100) - ifnull(tds_recv.tds_recovered,0)) tds_balance
from (select e.temp_emp_id emp_id, e.emp_bid, e.emp_name, e.date_of_join, d.designation_name,l.dept_name, 
            (basic+cca+da+hra+incr+ other_allowance+co_allowance+special_allowance) as CTC,
            round((basic+cca+da+hra+incr+ other_allowance+co_allowance+special_allowance)- te.exmp_limit,2) tax_amt,
            ifnull(sm.sec_80c,0) sec_80c, 
            ifnull(sm.sec_80d,0) sec_80d, 
            ifnull(sm.sec_80cc,0) sec_80cc,
            round((basic+(case when cca> 0 then 19200-cca else 0 end)+da+(hra-(basic+da)*10/100)+incr+ other_allowance+co_allowance+special_allowance)- (te.exmp_limit+175000),2)  aftr_saving
      from employee e LEFT JOIN designation d on (e.designation = d.designation_id)
           LEFT JOIN line_of_activity l on (e.activity_id = l.activity_id)     
           LEFT JOIN employee_pay_details slry  on (e.emp_bid = slry.emp_bid)
          LEFT JOIN empsavingsmaster sm on e.emp_bid = sm.emp_bid         
         LEFT JOIN std_tds_exemption te on case when e.sex='' then 'M' else e.sex END = te.sex
) tds_cal 
LEFT JOIN std_tds_deductions tdsd ON (case when tds_cal.aftr_saving > 0 then tds_cal.aftr_saving else 0 end) BETWEEN tdsd.min_limit and tdsd.max_limit
LEFT JOIN (select td.emp_bid, sum(td.tds_amt) tds_recovered
           from (select emp_bid, SUM(TDS) tds_amt from emp_monthly_salaries
                 where salary_month between 4 and 12 and salary_year = $a_year
                 group by emp_bid
                 UNION ALL
                 select emp_bid, SUM(TDS) tds_amt from emp_monthly_salaries
                 where salary_month between 1 and 3 and salary_year = $b_year
                  group by emp_bid) td
           group by td.emp_bid
          ) tds_recv  on (tds_cal.emp_bid = tds_recv.emp_bid) where $where
order by length(tds_cal.emp_bid),tds_cal.emp_bid";
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
    }
    // old
    public function oldEmployeeSavingsDataWhere($where)
    {
        $CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="select temp_emp_id, emp_name, emp_bid, date_of_join, CTC,dept_name,designation_name, Taxable_amt, etyc 80c, etycc 80cc, etyd 80d, 
                    (Taxable_amt-(etyc+etycc+etyd)) After_savings,
                    (Taxable_amt-(etyc+etycc+etyd))*tds.deduct_value/100 tds_year
                    from (
                    select e.temp_emp_id, e.emp_name, e.emp_bid, e.date_of_join,loa.dept_name,d.designation_name,
                    (pay.basic+pay.other_allowance+pay.incr+pay.co_allowance+pay.da+pay.special_allowance+pay.hra+pay.cca)+((pay.basic+pay.da)*12/100) as CTC,
                    (pay.basic+pay.incr+pay.da+pay.cca) Taxable_amt,es.sec_80c, es.sec_80cc,es.sec_80d,
                    case when ifnull(sec_80c, 0) <=150000 then ifnull(sec_80c, 0) else 150000 end etyc,
                    case when ifnull(sec_80cc, 0)<=25000 then ifnull(sec_80cc, 0) else 25000 end etycc,
                    case when ifnull(sec_80d, 0) <= 25000 then ifnull(sec_80d, 0) else 25000 end etyd    
                    from line_of_activity loa, designation d, employee e LEFT JOIN employee_pay_details pay ON pay.emp_bid = e.emp_bid
                        left JOIN empsavingsmaster es on e.emp_bid = es.emp_bid where d.designation_id=e.designation and loa.activity_id=e.activity_id and $where
                    ) grs left join
                     std_tds_deductions tds 
                    on (Taxable_amt-(etyc+etycc+etyd)) between tds.min_limit and tds.max_limit order by emp_bid asc";
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
    }
	public function getSalariesData($where)
	{
		$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="select epd.*,e.education,e.designation,e.emp_id,e.date_of_join,e.emp_name,pt.payment_type, la.dept_name from employee_pay_details epd,employee e,payment_types pt, line_of_activity la where la.activity_id=e.activity_id and e.emp_bid=epd.emp_bid and epd.payment_type_id=pt.payment_type_id and ".$where;
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
	}
	public function activeEmployees()
	{
		$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="select e.*,loa.dept_name from employee e,line_of_activity loa where e.activity_id=loa.activity_id";
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
	}
	public function inActiveEmployees()
	{
		$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="select e.*,loa.dept_name from employee_log e,line_of_activity loa where e.activity_id=loa.activity_id";
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
	}
	public function newEmployees()
	{
		$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $d2 = date('Y-m-d', strtotime('-30 days'));
        $sql_query="select e.*,loa.dept_name from employee e,line_of_activity loa where e.activity_id=loa.activity_id and e.date>".$d2;
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
	}
	public function paymentModes()
	{
		$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="select payment_type from payment_types order by payment_type";
        $data=$CI->Myfunctions->getQueryDataList($sql_query);
        return $data;
	}
}
?>