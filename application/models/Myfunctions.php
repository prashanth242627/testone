<?php
class Myfunctions extends CI_model
{
	public function __construct()
	{
		parent::__construct();
        $this->load->helper('url');
	}
	public function getDataList($table,$where='')
	{
		if($where!='') $where=" where ".$where; else $where=" where 1";	

		$data_sql="select * from ".$table." ".$where;
		$data_res=$this->db->query($data_sql);
		$data_count=$data_res->num_rows();

		if ($data_res->num_rows()>0) {
			
			$attendance_data=$data_res->result_array();
			
			return $attendance_data;
		}
		else{
			$msg = $this->db->error();
			
			return $msg;
		}
	}
	public function addRecord($table,$data)
	{
		$columns='';
		$values = '';
		foreach($data as $k=>$v){	
	
			$columns.=$k.",";
			
			$values.="'".	$v."',";	
		}
		$columns=rtrim($columns,',');
		
		$values=rtrim($values,',');
		$query = $this->db->query("INSERT into ".$table." (".$columns.") values(".$values.");");

		if ($query) {
			$id = $this->db->insert_id();
			return $id;
		}
		else{
			$msg = $this->db->error();
			return $msg;
		}
	}
	public function updateRecord($table,$data,$where)
	{
	$set='';

	foreach($data as $k=>$v){
					
		$set.=$k."='".$v."',";		
	}
		
	$set=rtrim($set,',');
	
	$sql="update ".$table." set ".$set." where ".$where;	
	$query_result=$this->db->query($sql);
	if ($query_result) {
		return 1;
	}
	else{
		$msg = $this->db->error();
		return $msg;
	}

	}
	public function getData($table,$where){ 
	    $data=array();
	    
	    if($where!='') $where=" where ".$where;
	    
	    $data_sql="select * from ".$table." ".$where;
	    
	    $data_res=$this->db->query($data_sql);
	    
	    $data_count=$data_res->num_rows();
	    
	    if($data_count==1){ 
	      
	      foreach($data_res->result_array() as $row){
	        
	        return $row;
	      }  
	    }
	    else{
	     
	     return $data;
	   }
  }
  public function getQueryDataList($data_sql){
	    
	    $data=array();
	    
	    $data_res=$this->db->query($data_sql);
	    if($data_res->num_rows()>0){ 
	     
	     $data=$data_res->result_array();
	    
	    }
	     return $data;
  	}
  	public function getDuplicate($table,$where='')
  	{
  		if($where!='')
		$sql="select * from ".$table." where ".$where;
		else
		$sql="select * from ".$table;
		$sql_result=$this->db->query($sql);
		$rows_count=$sql_result->num_rows();
		return $rows_count;
  	}
  	public function deleteData($table,$where)
  	{
  		$query="delete from ".$table." where ".$where;
  		$query_result=$this->db->query($query);
  		if ($query_result) {
  			return 1;
  		}
  	}
  	public function getQueryData($sql)
  	{
  		$data=array();
	
		$res=$this->db->query($sql);
		$count=$res->num_rows();
		
		if($count>0) $data=$res->result_array();	
			
		return $data;
  	}
}
?>