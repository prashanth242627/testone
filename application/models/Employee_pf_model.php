<?php
class Employee_pf_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct(); 
	}
    
	public function AllEmployeesPFData($year,$month)
    {
        $CI =& get_instance();
        $CI->load->model('Myfunctions');
        $prev_month_days = cal_days_in_month(CAL_GREGORIAN, date('m',strtotime($month)), $year);  
        $sql_query="select salary.emp_id, salary.emp_bid, salary.emp_name, salary.pf_acc_no, salary.uan, salary.dept_name, salary.designation_name,

       (basic+da+incr+hra+cca+other_allowances+co_allowance+special_allowance) gross_wages,

       case when (basic+da) >=15000 then 15000 else (basic+da) end pf_wages,

       round(case when (basic+da)*12/100 <= 1800 then (basic+da)*12/100 else 1800 end) PF,

       round(case when (basic+da) >=15000 then 15000*emplr_pf.eps/100 else (basic+da)*emplr_pf.eps/100 end) EPS_Contribution,

       round(case when (basic+da) >=15000 then 15000*emplr_pf.epf/100 else (basic+da)*emplr_pf.epf/100 end) EPF_Contribution,

       round(case when (basic+da) >=15000 then 15000*emplr_pf.admin_charges/100 else (basic+da)*emplr_pf.admin_charges/100 end) admin_charges_Contribution,

       round(case when (basic+da) >=15000 then 15000*emplr_pf.insp_charges/100 else (basic+da)*emplr_pf.insp_charges/100 end) insp_charges_Contribution,

       round(case when (basic+da) >=15000 then 15000*emplr_pf.edli/100 else (basic+da)*emplr_pf.edli/100 end) EDLI_Charges,

       (round(case when (basic+da)*12/100 <= 1800 then (basic+da)*12/100 else 1800 end) +

       round(case when (basic+da) >=15000 then 15000*emplr_pf.eps/100 else (basic+da)*emplr_pf.eps/100 end) +

       round(case when (basic+da) >=15000 then 15000*emplr_pf.epf/100 else (basic+da)*emplr_pf.epf/100 end) +

       round(case when (basic+da) >=15000 then 15000*emplr_pf.admin_charges/100 else (basic+da)*emplr_pf.admin_charges/100 end) +

       round(case when (basic+da) >=15000 then 15000*emplr_pf.insp_charges/100 else (basic+da)*emplr_pf.insp_charges/100 end) +

       round(case when (basic+da) >=15000 then 15000*emplr_pf.edli/100 else (basic+da)*emplr_pf.edli/100 end) ) Total

from (select e.temp_emp_id emp_id, e.emp_bid, e.emp_name, pay.pf_no pf_acc_no, pay.uan, la.dept_name, d.designation_name,

             round(case when ifnull(lc.lop_count,0) <= 0 then (pay.basic) else (pay.basic)-((pay.basic/(".$prev_month_days."))*lc.lop_count) end) basic,

                                round(case when ifnull(lc.lop_count,0) <= 0 then (pay.da) else (pay.da)-((pay.da/(".$prev_month_days."))*lc.lop_count) end) da,

             round(case when ifnull(lc.lop_count,0) <= 0 then (pay.other_allowance) else (pay.other_allowance)-((pay.other_allowance/(".$prev_month_days."))*lc.lop_count) end) other_allowances,

             round(case when ifnull(lc.lop_count,0) <= 0 then (pay.incr) else (pay.incr)-((pay.incr/(".$prev_month_days."))*lc.lop_count) end) incr,

             round(case when ifnull(lc.lop_count,0) <= 0 then (pay.co_allowance) else (pay.co_allowance)-((pay.co_allowance/(".$prev_month_days."))*lc.lop_count) end) co_allowance,             

                                      round(case when ifnull(lc.lop_count,0) <= 0 then (pay.special_allowance) else (pay.special_allowance)-((pay.special_allowance/(".$prev_month_days."))*lc.lop_count) end) special_allowance,

                                      round(case when ifnull(lc.lop_count,0) <= 0 then (pay.hra) else (pay.hra)-((pay.hra/(".$prev_month_days."))*lc.lop_count) end) hra,

                                      round(case when ifnull(lc.lop_count,0) <= 0 then (pay.cca) else (pay.cca)-((pay.cca/(".$prev_month_days."))*lc.lop_count) end) cca

      from employee_pay_details pay LEFT JOIN employee e ON pay.emp_bid = e.emp_bid

           LEFT JOIN emp_lop_calculation lc ON e.emp_bid = lc.emp_bid

                                        and lc.lop_month= ".(date('m',strtotime($month)))." 

                                        AND lc.lop_year = ".$year."

           LEFT JOIN line_of_activity la on e.activity_id = la.activity_id

           LEFT JOIN designation d on e.designation = d.designation_id

      where pay.employment_type = 0

     and pay.pf_consider = 1) salary,

     emplr_pf_contribution emplr_pf";
        /*$sql_query="select salary.*,
                case when (basic+da)*12/100 <= 1800 then (basic+da)*12/100 else 1800 end emplr_pf_contribution,
                case when (basic+da)*6.36/100 <=98 then (basic+da)*8.36/100 else 98 end empr_adm_contribution,
                case when (basic+da)*0.01/100 <=2 then (basic+da)*0.01/100 else 2 end empr_nsp_contribution,
                case when (basic+da)*0.005/100 <= 1 then (basic+da)*0.005/100 else 1 end empr_edli_contribution,
                case when (basic+da)*12/100 <= 1800 then (basic+da)*12/100 else 1800 end+
                case when (basic+da)*6.36/100 <=98 then (basic+da)*8.36/100 else 98 end +
                case when (basic+da)*0.01/100 <=2 then (basic+da)*0.01/100 else 2 end +
                case when (basic+da)*0.005/100 <= 1 then (basic+da)*0.005/100 else 1 end emplr_total_contribution
                from (
                select e.temp_emp_id, e.emp_bid, e.emp_name, la.dept_name, d.designation_name,
                case when ifnull(lc.lop_count,0) <= 0 then (pay.basic)
                     else (pay.basic)-((pay.basic/(".$prev_month_days."))*lc.lop_count)
                end basic,
                case when ifnull(lc.lop_count,0) <= 0 then (pay.da)
                     else (pay.da)-((pay.da/(".$prev_month_days."))*lc.lop_count)
                end da
                from employee e LEFT JOIN employee_pay_details pay ON pay.emp_bid = e.emp_bid
                LEFT JOIN emp_lop_calculation lc ON e.emp_bid = lc.emp_bid 
                                        and lc.lop_month= ".(date('m',strtotime($month)))." 
                                        AND lc.lop_year = ".$year."
                LEFT JOIN line_of_activity la on e.activity_id = la.activity_id
                LEFT JOIN designation d on e.designation = d.designation_id ) salary";*/
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
    }
    public function EmployeePFDataWhere($where,$year,$month)
    {
        $CI =& get_instance();
        $CI->load->model('Myfunctions');
        $prev_month_days = cal_days_in_month(CAL_GREGORIAN, date('m',strtotime($month)), $year);  
        //new query
        $sql_query="select salary.emp_id, salary.emp_bid, salary.emp_name, salary.pf_acc_no, salary.uan, salary.dept_name, salary.designation_name,

       (basic+da+incr+hra+cca+other_allowances+co_allowance+special_allowance) gross_wages,

       case when (basic+da) >=15000 then 15000 else (basic+da) end pf_wages,

       round(case when (basic+da)*12/100 <= 1800 then (basic+da)*12/100 else 1800 end) PF,

       round(case when (basic+da) >=15000 then 15000*emplr_pf.eps/100 else (basic+da)*emplr_pf.eps/100 end) EPS_Contribution,

       round(case when (basic+da) >=15000 then 15000*emplr_pf.epf/100 else (basic+da)*emplr_pf.epf/100 end) EPF_Contribution,

       round(case when (basic+da) >=15000 then 15000*emplr_pf.admin_charges/100 else (basic+da)*emplr_pf.admin_charges/100 end) admin_charges_Contribution,

       round(case when (basic+da) >=15000 then 15000*emplr_pf.insp_charges/100 else (basic+da)*emplr_pf.insp_charges/100 end) insp_charges_Contribution,

       round(case when (basic+da) >=15000 then 15000*emplr_pf.edli/100 else (basic+da)*emplr_pf.edli/100 end) EDLI_Charges,

       (round(case when (basic+da)*12/100 <= 1800 then (basic+da)*12/100 else 1800 end) +

       round(case when (basic+da) >=15000 then 15000*emplr_pf.eps/100 else (basic+da)*emplr_pf.eps/100 end) +

       round(case when (basic+da) >=15000 then 15000*emplr_pf.epf/100 else (basic+da)*emplr_pf.epf/100 end) +

       round(case when (basic+da) >=15000 then 15000*emplr_pf.admin_charges/100 else (basic+da)*emplr_pf.admin_charges/100 end) +

       round(case when (basic+da) >=15000 then 15000*emplr_pf.insp_charges/100 else (basic+da)*emplr_pf.insp_charges/100 end) +

       round(case when (basic+da) >=15000 then 15000*emplr_pf.edli/100 else (basic+da)*emplr_pf.edli/100 end) ) Total

from (select e.temp_emp_id emp_id, e.emp_bid, e.emp_name, pay.pf_no pf_acc_no, pay.uan, la.dept_name, d.designation_name,

             round(case when ifnull(lc.lop_count,0) <= 0 then (pay.basic) else (pay.basic)-((pay.basic/(".$prev_month_days."))*lc.lop_count) end) basic,

                                round(case when ifnull(lc.lop_count,0) <= 0 then (pay.da) else (pay.da)-((pay.da/(".$prev_month_days."))*lc.lop_count) end) da,

             round(case when ifnull(lc.lop_count,0) <= 0 then (pay.other_allowance) else (pay.other_allowance)-((pay.other_allowance/(".$prev_month_days."))*lc.lop_count) end) other_allowances,

             round(case when ifnull(lc.lop_count,0) <= 0 then (pay.incr) else (pay.incr)-((pay.incr/(".$prev_month_days."))*lc.lop_count) end) incr,

             round(case when ifnull(lc.lop_count,0) <= 0 then (pay.co_allowance) else (pay.co_allowance)-((pay.co_allowance/(".$prev_month_days."))*lc.lop_count) end) co_allowance,             

                                      round(case when ifnull(lc.lop_count,0) <= 0 then (pay.special_allowance) else (pay.special_allowance)-((pay.special_allowance/(".$prev_month_days."))*lc.lop_count) end) special_allowance,

                                      round(case when ifnull(lc.lop_count,0) <= 0 then (pay.hra) else (pay.hra)-((pay.hra/(".$prev_month_days."))*lc.lop_count) end) hra,

                                      round(case when ifnull(lc.lop_count,0) <= 0 then (pay.cca) else (pay.cca)-((pay.cca/(".$prev_month_days."))*lc.lop_count) end) cca

      from employee_pay_details pay LEFT JOIN employee e ON pay.emp_bid = e.emp_bid

           LEFT JOIN emp_lop_calculation lc ON e.emp_bid = lc.emp_bid

                                        and lc.lop_month= ".(date('m',strtotime($month)))." 

                                        AND lc.lop_year = ".$year."

           LEFT JOIN line_of_activity la on e.activity_id = la.activity_id

           LEFT JOIN designation d on e.designation = d.designation_id where $where

      and pay.employment_type = 0

     and pay.pf_consider = 1) salary,

     emplr_pf_contribution emplr_pf";
        //old query
        /*$sql_query="select salary.*,
                case when (basic+da)*12/100 <= 1800 then (basic+da)*12/100 else 1800 end emplr_pf_contribution,
                case when (basic+da)*6.36/100 <=98 then (basic+da)*8.36/100 else 98 end empr_adm_contribution,
                case when (basic+da)*0.01/100 <=2 then (basic+da)*0.01/100 else 2 end empr_nsp_contribution,
                case when (basic+da)*0.005/100 <= 1 then (basic+da)*0.005/100 else 1 end empr_edli_contribution,
                case when (basic+da)*12/100 <= 1800 then (basic+da)*12/100 else 1800 end+
                case when (basic+da)*6.36/100 <=98 then (basic+da)*8.36/100 else 98 end +
                case when (basic+da)*0.01/100 <=2 then (basic+da)*0.01/100 else 2 end +
                case when (basic+da)*0.005/100 <= 1 then (basic+da)*0.005/100 else 1 end emplr_total_contribution
                from (
                select e.temp_emp_id, e.emp_bid, e.emp_name, la.dept_name, d.designation_name,
                case when ifnull(lc.lop_count,0) <= 0 then (pay.basic)
                     else (pay.basic)-((pay.basic/(".$prev_month_days."))*lc.lop_count)
                end basic,
                case when ifnull(lc.lop_count,0) <= 0 then (pay.da)
                     else (pay.da)-((pay.da/(".$prev_month_days."))*lc.lop_count)
                end da
                from employee e LEFT JOIN employee_pay_details pay ON pay.emp_bid = e.emp_bid
                LEFT JOIN emp_lop_calculation lc ON e.emp_bid = lc.emp_bid 
                                        and lc.lop_month= ".(date('m',strtotime($month)))." 
                                        AND lc.lop_year = ".$year."
                LEFT JOIN line_of_activity la on e.activity_id = la.activity_id
                LEFT JOIN designation d on e.designation = d.designation_id where $where ) salary";*/
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
    }
	public function getSalariesData($where)
	{
		$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="select epd.*,e.education,e.designation,e.emp_id,e.date_of_join,e.emp_name,pt.payment_type, la.dept_name from employee_pay_details epd,employee e,payment_types pt, line_of_activity la where la.activity_id=e.activity_id and e.emp_bid=epd.emp_bid and epd.payment_type_id=pt.payment_type_id and ".$where;
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
	}
	public function activeEmployees()
	{
		$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="select e.*,loa.dept_name from employee e,line_of_activity loa where e.activity_id=loa.activity_id";
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
	}
	public function inActiveEmployees()
	{
		$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="select e.*,loa.dept_name from employee_log e,line_of_activity loa where e.activity_id=loa.activity_id";
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
	}
	public function newEmployees()
	{
		$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $d2 = date('Y-m-d', strtotime('-30 days'));
        $sql_query="select e.*,loa.dept_name from employee e,line_of_activity loa where e.activity_id=loa.activity_id and e.date>".$d2;
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        return $employees;
	}
	public function paymentModes()
	{
		$CI =& get_instance();
        $CI->load->model('Myfunctions');
        $sql_query="select payment_type from payment_types order by payment_type";
        $data=$CI->Myfunctions->getQueryDataList($sql_query);
        return $data;
	}
}
?>