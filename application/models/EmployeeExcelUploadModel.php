<?php
class EmployeeExcelUploadModel extends CI_Model
{
	public function __construct()
	{
		parent::__construct(); 
                
	}
 	public function allEmployees()
	{
                $CI =& get_instance();
                $CI->load->model('Myfunctions');
        $sql_query="select e.*,loa.dept_name from employee e,line_of_activity loa where e.activity_id=loa.activity_id";
        $sql_query1="select e.*,loa.dept_name from employee_log e,line_of_activity loa where e.activity_id=loa.activity_id";
        $employees=$CI->Myfunctions->getQueryDataList($sql_query);
        $employees_log=$CI->Myfunctions->getQueryDataList($sql_query1);
        foreach ($employees_log as $employee_log) {
        	array_push($employees, $employee_log);
        }
        return $employees;
	}
        function select()
         {
          $this->db->order_by('emp_id', 'DESC');
          $query = $this->db->get('employee');
          return $query;
         }

         function insert($data)
         {
                $this->db->insert_batch('new_excel_data', $data);
         }
         function insertbiometirc($data)
         {
                $this->db->insert_batch('biometirc_attendance', $data);
         }
         function insertSalaries($data)
         {
            $CI =& get_instance();
                $CI->load->model('Myfunctions');
                $employees=$CI->Myfunctions->addRecord('employee_pay_details',$data);
                echo json_encode($employees);
               // $this->db->insert_batch('employee_pay_details', $data);
         }
         function insertLeaves($data)
         {
             $CI =& get_instance();
                $CI->load->model('Myfunctions');
                $employees=$CI->Myfunctions->addRecord('leave_master',$data);
                echo json_encode($employees);
         }
         function insertLops($data)
         {
             $CI =& get_instance();
                $CI->load->model('Myfunctions');
                $employees=$CI->Myfunctions->addRecord('emp_lop_calculation',$data);
                echo json_encode($employees);
         }
        /*public function insert($data)
        {
                $CI =& get_instance();
                $CI->load->model('Myfunctions');
                $employees=$CI->Myfunctions->addRecord("employee",$data);
                return $employees;
        }*/
 
}


?>