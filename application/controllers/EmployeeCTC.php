<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'controllers/Global_System.php';

class EmployeeCTC extends Global_System {
	public function __construct(){
		parent::__construct(); 
		$this->logincheck();
		$this->load->library('session');
		$this->load->helper('url');
		$this->load->model('Employee_model');
		$this->load->model('Salary_model');
	}
	public function index()
	{
		$activities=$this->Employee_model->activities();
		$employees_bid=$this->Employee_model->employeesBid();
		$employees_name=$this->Employee_model->employeesName();
		$payment_modes=$this->Salary_model->paymentModes();
		$data['activities']= $activities;
		$data['bids']= $employees_bid;
		$data['names']=$employees_name;
		$data['payment_modes']=$payment_modes;
		$employees=array();
		$this->load->view('EmployeeSalaryDetails',$data);
	}
	public function All()
	{
		$activities=$this->Employee_model->activities();
		$employees_bid=$this->Employee_model->employeesBid();
		$employees_name=$this->Employee_model->employeesName();
		$payment_modes=$this->Salary_model->paymentModes();
		$employees=$this->Salary_model->allSalariesDetails();
		$data['activities']= $activities;
		$data['bids']= $employees_bid;
		$data['names']=$employees_name;
		$data['payment_modes']=$payment_modes;
		if ($employees) 
		{
			$data['employees'] = $employees;
		}
		$this->load->view('EmployeeSalaryDetails', $data);
	}
	public function Activity()
	{
		$activity_type=$this->input->get('type');
		$bid=$this->input->get('bid');
		$ename=$this->input->get('ename');
		$payment_mode=$this->input->get('payment_mode');
		$start_date=$this->input->get('from');
		$end_date=$this->input->get('to');
		$activities=$this->Employee_model->activities();
		$employees_bid=$this->Employee_model->employeesBid();
		$employees_name=$this->Employee_model->employeesName();
		$payment_modes=$this->Salary_model->paymentModes();
		$employees=array();
		if ($activity_type!=null && $payment_mode==null) {
			$employees=$this->Salary_model->lineOfActivitySalaries($activity_type);
		}
		else if ($bid!=null) {
			$employees=$this->Salary_model->employeeSalaryDataByBid($bid);
		}
		else if ($ename!=null) {
			$employees=$this->Salary_model->employeeSalaryDataByName($ename);
		}
		else if ($payment_mode!=null && $activity_type!=null)  {
			$employees=$this->Salary_model->paymentModeAndDeptSalaries($activity_type,$payment_mode);
		}
		else if ($payment_mode!=null)  {
			$employees=$this->Salary_model->paymentModeSalaries($payment_mode);
		}
		/*else if ($status!=null) {
			if ($status=="Active") {
				$employees=$this->Salary_model->activeEmployees();
			}
			else if ($status=="Inactive") {
				$employees=$this->Salary_model->inActiveEmployees();
			}
			else if ($status=="New") {
				$employees=$this->Salary_model->newEmployees();
			}
		}*/
		else if ($start_date!=null) {
			$employees=$this->Salary_model->getSalariesData("e.date_of_join>='".$start_date."' and e.date_of_join <='".$end_date."'");
		}
		else if ($end_date!="" && $start_date==null) {
			$employees=$this->Salary_model->getSalariesData("e.date_of_join <='".$end_date."'");
		}
		$data['activities']= $activities;
		$data['bids']= $employees_bid;
		$data['names']=$employees_name;
		$data['payment_modes']=$payment_modes;
		if (count($employees)) 
		{
			$data['employees'] = $employees;
		}
		$this->load->view('EmployeeSalaryDetails', $data);
	}

}
