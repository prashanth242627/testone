<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'controllers/Global_System.php';
class EmployeeLOPCalculation extends Global_System {
	
	public function __construct()
	{
		parent::__construct();
		$this->logincheck();
		$this->load->model('Myfunctions');
		ini_set('display_errors', 1);
	}
	public function index()
	{
		//$get_emp_leavemaster  = $this->Global_model->get_emp_leavemaster($id);
		$data=array('current_page'=>'EMP LOP Calculation','page_title'=>'EMP LOP Calculation','parent_menu'=>'','template'=>'masters/lop_calculation/add');
		$this->form_validation->set_rules('year', 'YEAR', 'required'); 
		$this->form_validation->set_rules('month', 'MONTH', 'required'); 
		if($this->form_validation->run() === FALSE)
		{
			$this->load->view('theme/template',$data);
		}
		else
		{	
			$emp_id  = $this->session->userdata('emp_id');
			$year = $this->input->post('year');
			$month = $this->input->post('month');
			$this->db->trans_begin();
			$insert_array = array('year'=>$year, 'month'=>$month,'emp_id'=>$emp_id);
			
			$this->all_actions('insert',$insert_array);
			if($this->db->trans_status()===FALSE)
			{
				$this->db->trans_rollback();
				$this->session->set_userdata(array('create_status'=>'failed'));
				$this->session->set_userdata(array('record_name'=>'failed to calculate LOPs'));
			}
			else
			{
				$this->db->trans_commit();
				$this->session->set_userdata(array('create_status'=>'success'));
				$this->session->set_userdata(array('record_name'=>'Success'));
			}
			redirect('EmployeeLOPCalculation');
			//print_r($insert_array);
		} 
		
	}
	function all_actions($action,$insert_array)
	{
		$emp_id  = $this->session->userdata('emp_id');
		$status = $insert_array['status'];
		if($action == "insert")
		{
			$month = $insert_array['month'];
			$year=$insert_array['year'];
			$month_no=date("m",strtotime($month));

			$last_day=cal_days_in_month(CAL_GREGORIAN,$month_no,$year);
			$last_date=date("$year-$month_no-$last_day");
			
			//$last_day_of_month=date("Y-m-j", strtotime("last day of previous month"));
			$last_1_day_of_month=date("Y-m-d", strtotime( $last_date . ' -1 day' ));
			$last_2_day_of_month=date("Y-m-d", strtotime( $last_date . ' -2 day' ));
			$last_3_day_of_month=date("Y-m-d", strtotime( $last_date . ' -3 day' ));
						
			$last_record_of_month=$this->Myfunctions->getQueryData("SELECT * FROM `biometirc_attendance` WHERE log_date='".$last_date."'");
			$last_record_of_month1=$this->Myfunctions->getQueryData("SELECT * FROM `biometirc_attendance` WHERE log_date='".$last_1_day_of_month."'");
			$last_record_of_month2=$this->Myfunctions->getQueryData("SELECT * FROM `biometirc_attendance` WHERE log_date='".$last_2_day_of_month."'");
			$last_record_of_month3=$this->Myfunctions->getQueryData("SELECT * FROM `biometirc_attendance` WHERE log_date='".$last_3_day_of_month."'");
			if (count($last_record_of_month) || count($last_record_of_month1) || count($last_record_of_month2) || count($last_record_of_month3)) {
				$employees_data=$this->Global_model->get_employees_data();

				foreach ($employees_data as $employee_data) {
					$diff=0;
					$lop=0;
					//lop entry to attendance marking table
						for ($day_no=1; $day_no <=$last_day ; $day_no++) { 
							$num_padded = sprintf("%02d", $day_no);
							$month_date=date("$year-$month_no-$num_padded");

							$get_attendance_marking=$this->db->query("SELECT * FROM attendance_marking WHERE emp_bid=".$employee_data->emp_bid." and leave_date='".$month_date."'")->row();
							$get_bio_attendance=$this->db->query("SELECT * FROM biometirc_attendance WHERE emp_bid=".$employee_data->emp_bid." and log_date='".$month_date."'")->row();
							$get_lop_id=$this->db->query("SELECT * FROM leave_types WHERE leave_type='LOP'")->row();

							if ($get_attendance_marking || $get_bio_attendance) {
			    				
			    			}
			    			else{
			    				$day_name1=date('l',strtotime($month_date));
			    				if ($day_name1!="Sunday") {
			    					$get_holiday_data1=$this->db->query("SELECT * FROM holidays WHERE from_date<='".$month_date."' and to_date >= '".$month_date."' and holiday_group_id=".$employee_data->holiday_group_id."")->row();
									if ($get_holiday_data1) {
			            				
			            			}
			            			else{
				    					$param= array('attendance_id'=>$this->Global_model->Max_count($this->config->item('attendance_marking_test'),'attendance_id'),'emp_bid'=>$employee_data->emp_bid,'emp_id'=>".$employee_data->emp_bid.",'leave_date'=>$month_date,'leave_type_id'=>$get_lop_id->leave_type_id,'no_leaves'=>1,"status"=>1,"date"=>date('Y-m-d'),'att_description'=>"Bio Not Present",'Created_By'=>1,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"));
										$this->Myfunctions->addRecord("attendance_marking_test",$param);
									}
			    				}
			    				
			    			}
						}	
						$lop_data=$this->Myfunctions->getData("emp_lop_calculation","emp_bid=".$employee_data->emp_bid." and lop_month=".$month_no."  and lop_year=".$year);
						$cls_data=$this->db->query("SELECT lm.used,lm.leave_type_id,lm.no_leaves,lm.emp_bid FROM leave_master `lm` LEFT JOIN leave_types lt ON lt.leave_type_id = lm.leave_type_id WHERE lt.leave_type='CL' and lm.`emp_bid` = ".$employee_data->emp_bid."")->row();
						$a_f_lop_count=$this->db->query("SELECT COUNT(*) as lop_count from attendance_marking WHERE emp_bid=".$employee_data->emp_bid." and leave_type_id=9 and MONTHNAME(leave_date)='".$month."' and year(leave_date)=".$year)->row();
						$a_h_lop_count=$this->db->query("SELECT COUNT(*) as lop_count from attendance_marking WHERE emp_bid=".$employee_data->emp_bid." and leave_type_id=10 and MONTHNAME(leave_date)='".$month."' and year(leave_date)=".$year)->row();
						//$sql_query="SELECT e.temp_emp_id,e.emp_name,ba.emp_bid,loa.dept_name,loa.from_time,loa.to_time,ba.log_date,d.designation_name,ba.log_date, MIN(ba.log_time) as in_time, MAX(ba.log_time) as out_time, TIMEDIFF(MAX(ba.log_time),MIN(ba.log_time)) as diffrence, (TIME_TO_SEC(MIN(ba.log_time))-TIME_TO_SEC(loa.from_time))/60 as buffer_time FROM line_of_activity loa, designation d,  biometirc_attendance ba left JOIN employee e on e.emp_bid=ba.emp_bid WHERE e.activity_id=loa.activity_id and d.designation_id=e.designation  and e.emp_bid=".$employee_data->emp_bid." and YEAR(ba.log_date)=".$year." AND MONTHNAME(ba.log_date)='".$month."' GROUP BY ba.log_date, ba.emp_bid order by buffer_time";
						$sql_query="SELECT e.temp_emp_id,e.emp_name,ba.emp_bid,loa.dept_name,ba.log_date,d.designation_name,ba.log_date, MIN(ba.log_time) as in_time, MAX(ba.log_time) as out_time, TIMEDIFF(MAX(ba.log_time),MIN(ba.log_time)) as diffrence, case when ba.log_date>='2019-04-12' and ba.log_date<='2019-05-31' then '09:30:00' else loa.from_time end as from_time,loa.to_time,case when ba.log_date>='2019-04-12' and ba.log_date<='2019-05-31' then (TIME_TO_SEC(MIN(ba.log_time))-TIME_TO_SEC('09:30:00'))/60 else (TIME_TO_SEC(MIN(ba.log_time))-TIME_TO_SEC(loa.from_time))/60 end as buffer_time FROM line_of_activity loa, designation d,  biometirc_attendance ba left JOIN employee e on e.emp_bid=ba.emp_bid WHERE e.activity_id=loa.activity_id and d.designation_id=e.designation  and e.emp_bid=".$employee_data->emp_bid." and YEAR(ba.log_date)=".$year." AND MONTHNAME(ba.log_date)='".$month."' GROUP BY ba.log_date, ba.emp_bid order by buffer_time";
		                $reports=$this->Myfunctions->getQueryDataList($sql_query);
	                if (count($reports)>0) {
	            		foreach ($reports as $report) {
	            			$day_name=date('l',strtotime($report['log_date']));
							if ($day_name!="Sunday") {
								$get_holiday_data=$this->db->query("SELECT * FROM holidays WHERE from_date<='".$report['log_date']."' and to_date >= '".$report['log_date']."' and holiday_group_id=".$employee_data->holiday_group_id."")->row();
								if ($get_holiday_data) {
		            				
		            			}
		            			else{
		            				if (round($report['buffer_time'])>0) {
		            					$diff+=round($report['buffer_time']);
			            			}
			            			if ($diff>25) {
			            				$lop+=0.5;
			            			}
		            			}
							}
	            		}
	            		$data1['updated_date']=date("Y-m-d H:i:s");
	            		$data1['updated_by']=$emp_id;
	                }
	                else{
	            		$data1['updated_date']=date("Y-m-d H:i:s");
	            		$data1['updated_by']=$emp_id;
	                }
					if (!empty($lop_data)) {
						$a_f_lop=$a_f_lop_count->lop_count;
						$a_f_lop=0;
						if ($a_h_lop_count->lop_count>0) {
							$a_h_lop=($a_h_lop_count->lop_count)/2;
						}
						else{
							$a_h_lop=0;
						}
						$total_lops=$lop+$a_h_lop+$a_f_lop;
						$cls_used=0;
						if (!empty($cls_data)) {
							$no_of_cls=$cls_data->no_leaves-$cls_data->used;
							if ($no_of_cls>$total_lops) {
								$cls_after_aviled=$no_of_cls-$total_lops;
								$cls_used=$total_lops;
								$total_lops=0;
							}
							else if ($no_of_cls<$total_lops) {
								$cls_after_aviled=0;
								$cls_used=$no_of_cls;
								$total_lops=$total_lops-$no_of_cls;
							}
							else if ($no_of_cls==$total_lops) {
								$cls_after_aviled=0;
								$total_lops=0;
								$cls_used=$total_lops;
							}
						}
						$data1['total_lop_count']=$lop+$a_h_lop+$a_f_lop;

						$data1['lop_count']=($lop+$a_h_lop+$a_f_lop)-$lop_data['bio_cls_adjusted'];
						$this->Myfunctions->updateRecord("emp_lop_calculation",$data1,"emp_bid=".$employee_data->emp_bid." and lop_month=".$month_no."  and lop_year=".$year);
					}
					else{
						$a_f_lop=$a_f_lop_count->lop_count;
						if ($a_h_lop_count->lop_count>0) {
							$a_h_lop=($a_h_lop_count->lop_count)/2;
						}
						else{
							$a_h_lop=0;
						}
						$total_lops=$lop+$a_h_lop+$a_f_lop;
						$late_lops=$lop;
						$cls_used=0;
						if (!empty($cls_data)) {
							$no_of_cls=$cls_data->no_leaves-$cls_data->used;

							if ($no_of_cls>$late_lops) {
								$cls_after_aviled=$no_of_cls-$late_lops;
								$cls_used=$late_lops;
								$total_lops=$total_lops-$late_lops;

							}
							else if ($no_of_cls<$late_lops) {
								if ($no_of_cls<0) {
									$cls_after_aviled=$no_of_cls;
									$cls_used=0;
									$total_lops=$total_lops-$late_lops;
								}
								else{
									$cls_after_aviled=0;
									$cls_used=$no_of_cls;
									$total_lops=($total_lops-$late_lops)-$no_of_cls;
								}
							}
							else if ($no_of_cls==$late_lops) {
								$cls_after_aviled=0;
								$total_lops=0;
								$cls_used=$total_lops-$late_lops;

							}
							$update_cls_data['no_leaves']=$cls_after_aviled;
							$update_cls_data['used']=$cls_used;
							$this->Myfunctions->updateRecord("leave_master",$update_cls_data,"leave_type_id=".$cls_data->leave_type_id." and `emp_bid` = ".$employee_data->emp_bid."");
						}
						$data1['total_lop_count']=$lop+$a_h_lop+$a_f_lop;
						$data1['bio_cls_adjusted']=$cls_used;
						$data1['lop_count']=$total_lops;
						$data1['emp_bid']=$employee_data->emp_bid;
						$data1['lop_month']=$month_no;
						$data1['lop_year']=$year;
						$data1['created_by']=$emp_id;
						$this->Myfunctions->addRecord('emp_lop_calculation',$data1);
						
					}
					print_r($data1);exit();
						$data_bkp['total_lop_count']=$lop+$a_h_lop+$a_f_lop;
						$data_bkp['bio_cls_adjusted']=$cls_used;
						$data_bkp['lop_count']=$total_lops;
						$data_bkp['emp_bid']=$employee_data->emp_bid;
						$data_bkp['lop_month']=$month_no;
						$data_bkp['lop_year']=$year;
						$data_bkp['created_by']=$emp_id;
						$this->Myfunctions->addRecord('emp_lop_calculation_backup',$data_bkp);
				}
			}
			else{
				$this->session->set_userdata(array('create_status'=>'failed'));
				$this->session->set_userdata(array('record_name'=>'Please Upload BioMetric Attendance'));
				redirect('EmployeeLOPCalculation');
			}
			}
			
			else {
					$this->session->set_userdata(array('create_status'=>'failed'));
					$this->session->set_userdata(array('record_name'=>'Contact System administrator'));	
					redirect('EmployeeLOPCalculation');
			}
	}
	public function activity()
	{
		$month=$this->input->get('month');
		$month_no=date("m",strtotime($month));
		$year=$this->input->get('year');
		$sql_query="select e.emp_bid,e.temp_emp_id,e.emp_name,d.designation_name,loa.dept_name,lop.lop_month,lop.total_lop_count,lop.lop_count,lop.bio_cls_adjusted,lop.lop_year from emp_lop_calculation lop left JOIN  employee e on e.emp_bid= lop.emp_bid left JOIN line_of_activity loa on e.activity_id=loa.activity_id left JOIN designation d on e.designation=d.designation_id where lop.lop_month=".$month_no." and lop.lop_year=".$year." and lop.total_lop_count<>0.0 order by length(e.emp_bid),e.emp_bid asc ";
		$lops_data=$this->Myfunctions->getQueryDataList($sql_query);
		$data=array('current_page'=>'EMP LOP Calculation','page_title'=>'EMP LOP Calculation','parent_menu'=>'','template'=>'masters/lop_calculation/add',"lops_data"=>$lops_data);
		$this->load->view('theme/template',$data);
	}
	
}
