<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MonthlyReports extends CI_Controller {
	public function __construct(){
		parent::__construct(); 
		$this->load->library('session');
		$this->load->helper('url');
		$this->load->model('Employee_model');
		$this->load->model("attendance_reports_model");
		$this->load->model("monthwise_attendance_model");
		$this->load->model("Holidays_model");
	}
	public function index()
	{
		$activities=$this->Employee_model->activities();
		$employees_bid=$this->Employee_model->employeesBid();
		$employees_name=$this->Employee_model->employeesName();
		$get_leave_types  = $this->Global_model->get_leave_types();
		$data=array('current_page'=>'MonthWise Attendance Comparisions','page_title'=>'MonthWise Attendance Comparisions','parent_menu'=>'','template'=>'MonthWiseAttendanceComparisions');
		$data['activities']=$activities;
		$data['leave_types']=$get_leave_types;
		$data['bids']= $employees_bid;
		$data['names']=$employees_name;
		$this->load->view('theme/template',$data);
	}
	public function All()
	{
		$activities=$this->Employee_model->activities();
		$reports=$this->monthwise_attendance_model->getAllReports();
		$employees_bid=$this->Employee_model->employeesBid();
		$employees_name=$this->Employee_model->employeesName();
		$get_leave_types  = $this->Global_model->get_leave_types();
		$data=array('current_page'=>'MonthWise Attendance Comparisions','page_title'=>'MonthWise Attendance Comparisions','parent_menu'=>'','template'=>'MonthWiseAttendanceComparisions');
		$data['leave_types']=$get_leave_types;
		$data['activities']=$activities;
		$data['bids']= $employees_bid;
		$data['names']=$employees_name;
		if ($reports) 
		{
			$data['reports'] = $reports;
		}
		$this->load->view('theme/template',$data);
	}
	public function Activity()
	{
		$activity_type=$this->input->get('type');
		$year=$this->input->get('year');
		$month=$this->input->get('month');		
		$activities=$this->Employee_model->activities();
		$employees_bid=$this->Employee_model->employeesBid();
		$employees_name=$this->Employee_model->employeesName();
		$get_leave_types  = $this->Global_model->get_leave_types();
		$a_reports=array();
		if ($activity_type!=null) {
			
			$a_reports=$this->monthwise_attendance_model->getAttendanceReportsByDept($activity_type);
		}
		else if ($ename!=null) {
			$a_reports=$this->monthwise_attendance_model->getAttendanceReportsByWhere("e.emp_name='".$ename."'");
		}
		else if ($l_type!=null) {
			$a_reports=$this->monthwise_attendance_model->getAttendanceReportsByWhere("lt.leave_type='".$l_type."'");
		}
		else if ($year!=null && $month!=null) {
			$a_reports=$this->monthwise_attendance_model->getAttendanceReportsByWhere("YEAR(ba.log_date)=".$year." and MONTHNAME(ba.log_date)='".$month."'");
		}
		else if ($month!=null & $year==null) {
			$a_reports=$this->monthwise_attendance_model->getAttendanceReportsByWhere("MONTHNAME(ba.log_date)='".$month."' and YEAR(ba.log_date)='".date('Y')."'");
		}
		$data=array('current_page'=>'MonthWise Attendance Comparisions','page_title'=>'MonthWise Attendance Comparisions','parent_menu'=>'','template'=>'MonthWiseAttendanceComparisions');
		$data['activities']= $activities;
		$data['bids']= $employees_bid;
		$data['names']=$employees_name;
		$data['leave_types']=$get_leave_types;
		$data['holidays']=$holidays;
		if (count($a_reports)) 
		{
			$data['reports'] = $a_reports;
			
		}
		$this->load->view('theme/template',$data);
	}

}
