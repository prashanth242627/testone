
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class EmployeeSalariesExcelUpload extends CI_Controller
{
 public function __construct()
 {
  parent::__construct();
  $this->load->model('EmployeeExcelUploadModel');
  $this->load->library('excel');
 }

 function index()
 {
  $this->load->view('paydetails_upload');
 }
 function leavesUpload(){
  $this->load->view('leaves_upload');
 }
 
 function fetch()
 {
  $data = $this->EmployeeExcelUploadModel->select();
  $output = '
  <h3 align="center">Total Data - '.$data->num_rows().'</h3>
  <table class="table table-striped table-bordered">
   <tr>
    <th>Customer Name</th>
    <th>Address</th>
    <th>City</th>
    <th>Postal Code</th>
    <th>Country</th>
   </tr>
  ';
  foreach($data->result() as $row)
  {
   $output .= '
   <tr>
    <td>'.$row->CustomerName.'</td>
    <td>'.$row->Address.'</td>
    <td>'.$row->City.'</td>
    <td>'.$row->PostalCode.'</td>
    <td>'.$row->Country.'</td>
   </tr>
   ';
  }
  $output .= '</table>';
  echo $output;
 }

 function employeesimport()
 {
  if(isset($_FILES["file"]["name"]))
  {
   $path = $_FILES["file"]["tmp_name"];
   $object = PHPExcel_IOFactory::load($path);
   foreach($object->getWorksheetIterator() as $worksheet)
   {
    $highestRow = $worksheet->getHighestRow();
    $highestColumn = $worksheet->getHighestColumn();
    for($row=2; $row<=$highestRow; $row++)
    {
     $emp_id = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
     $emp_bid = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
     $acc_no = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
     $emp_name = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
     $date_of_join = date('Y-m-d', PHPExcel_Shared_Date::ExcelToPHP($worksheet->getCellByColumnAndRow(4, $row)->getValue()));
     $subject = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
     $pf_acc_no = $worksheet->getCellByColumnAndRow(6, $row)->getValue();
     $group_name = $worksheet->getCellByColumnAndRow(7, $row)->getValue();
     $uan_no = $worksheet->getCellByColumnAndRow(8, $row)->getValue();
     $esi_no = $worksheet->getCellByColumnAndRow(9, $row)->getValue();
     $employment = $worksheet->getCellByColumnAndRow(10, $row)->getValue();
     $data[] = array(
      'emp_id'  => $emp_id,
      'emp_bid'   => $emp_bid,
      'emp_name'    => $emp_name,
      'acc_no'    => $acc_no,
      'date_of_join'  => $date_of_join,
      'subject'   => $subject,
      'pf_acc_no'  => $pf_acc_no,
      'group_name'   => $group_name,
      'uan_no'   => $uan_no,
      'esi_no'   => $esi_no,
      'employment'   => $employment
     );
    }
   }
   $this->EmployeeExcelUploadModel->insert($data);
   echo 'Data Imported successfully';
  } 
 }
 function bareportsimport()
 {
  if(isset($_FILES["file"]["name"]))
  {
   $path = $_FILES["file"]["tmp_name"];
   $object = PHPExcel_IOFactory::load($path);
   foreach($object->getWorksheetIterator() as $worksheet)
   {
    $highestRow = $worksheet->getHighestRow();
    $highestColumn = $worksheet->getHighestColumn();
    for($row=2; $row<=$highestRow; $row++)
    {
     $emp_bid = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
     $log_date = date('Y-m-d', PHPExcel_Shared_Date::ExcelToPHP($worksheet->getCellByColumnAndRow(1, $row)->getValue()));
     $log_time = PHPExcel_Style_NumberFormat::toFormattedString($worksheet->getCellByColumnAndRow(2, $row)->getValue(), 'hh:mm:ss');
     $data[] = array(
      'emp_bid'   => $emp_bid,
      'log_date'    => $log_date,
      'log_time'  => $log_time,
      'status'   => 1,
      'Created_By'  => 1,
      'Created_Datetime'   => date('Y-m-d h:m:s'),
      'Updated_By'  => 1,
      'Updated_Datetime'   => date('Y-m-d h:m:s'),
     );
    }
   }
   $this->EmployeeExcelUploadModel->insertbiometirc($data);
   echo "success";
  } 
 }
 public function lopExcelUpload()
 {
   if(isset($_FILES["file"]["name"]))
  {
   $path = $_FILES["file"]["tmp_name"];
   $object = PHPExcel_IOFactory::load($path);
   $sheet=1;
   $id=1;
   foreach($object->getWorksheetIterator() as $worksheet)
   {
    
    $highestRow = $worksheet->getHighestRow();
    $highestColumn = $worksheet->getHighestColumn();
    for($row=2; $row<=$highestRow; $row++)
    {
     $emp_bid = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
     $lop_month = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
     $total_lop_count = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
     $bio_cls_adjusted=$worksheet->getCellByColumnAndRow(3, $row)->getValue();
    $lop_count=$worksheet->getCellByColumnAndRow(4, $row)->getValue();

     $data= array(
      "lop_year"=>2019,
      'emp_bid'   => $emp_bid,
      'lop_month'    => $lop_month,
      'total_lop_count'=> $total_lop_count,
      'bio_cls_adjusted'  => $bio_cls_adjusted,
      'lop_count'=>$lop_count,
      'created_by'  => 1,
      'updated_by'  => 2,
      'updated_date'   => date('Y-m-d h:m:s'),
     );
     $this->EmployeeExcelUploadModel->insertLops($data);
    }
    
    
   }
   //
   
   echo "success";
  } 
 }
 public function exceslLeavesUpload()
 {
   if(isset($_FILES["file"]["name"]))
  {
   $path = $_FILES["file"]["tmp_name"];
   $object = PHPExcel_IOFactory::load($path);
   $sheet=1;
   $id=1;
   foreach($object->getWorksheetIterator() as $worksheet)
   {
    
    $highestRow = $worksheet->getHighestRow();
    $highestColumn = $worksheet->getHighestColumn();
    for($row=2; $row<=$highestRow; $row++)
    {
     $emp_bid = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
      $leave_type_id = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
     $no_leaves = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
     $used_leaves=$worksheet->getCellByColumnAndRow(3, $row)->getValue();
     $data= array(
      "calendar_year"=>2019,
      'emp_bid'   => $emp_bid,
      'leave_type_id'    => $leave_type_id,
      'used'=> $used_leaves,
      'no_leaves'  => $no_leaves,
      'Created_By'  => 1,
      'Created_Datetime'   => date('Y-m-d h:m:s'),
      'Updated_By'  => 2,
      'Updated_Datetime'   => date('Y-m-d h:m:s'),
     );
     $this->EmployeeExcelUploadModel->insertLeaves($data);
    }
    
    
   }
   //
   
   echo "success";
  } 
 }
 public function EmployeeSalaries($value='')
 {
   if(isset($_FILES["file"]["name"]))
  {
   $path = $_FILES["file"]["tmp_name"];
   $object = PHPExcel_IOFactory::load($path);
   foreach($object->getWorksheetIterator() as $worksheet)
   {
    $highestRow = $worksheet->getHighestRow();
    $highestColumn = $worksheet->getHighestColumn();
    for($row=2; $row<=$highestRow; $row++)
    {
     $emp_bid = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
     //$payment_type_id = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
     $payment_type_id=2;
     $acc_no = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
     $uan = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
     $pf_no = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
     $esi_no = ($worksheet->getCellByColumnAndRow(4, $row)->getValue())? $worksheet->getCellByColumnAndRow(4, $row)->getValue(): 0;
     $basic = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
     $other_allowance = ($worksheet->getCellByColumnAndRow(6, $row)->getValue())? $worksheet->getCellByColumnAndRow(6, $row)->getValue(): 0;
     $incr = ($worksheet->getCellByColumnAndRow(7, $row)->getValue())? $worksheet->getCellByColumnAndRow(7, $row)->getValue(): 0;
     $co_allowance = ($worksheet->getCellByColumnAndRow(8, $row)->getValue())? $worksheet->getCellByColumnAndRow(8, $row)->getValue(): 0;
     $da = ($worksheet->getCellByColumnAndRow(9, $row)->getValue())? $worksheet->getCellByColumnAndRow(9, $row)->getValue(): 0;
     $special_allowance = ($worksheet->getCellByColumnAndRow(10, $row)->getValue())? $worksheet->getCellByColumnAndRow(10, $row)->getValue(): 0;
     $hra = ($worksheet->getCellByColumnAndRow(11, $row)->getValue())? $worksheet->getCellByColumnAndRow(11, $row)->getValue(): 0;
     $cca = ($worksheet->getCellByColumnAndRow(12, $row)->getValue())? $worksheet->getCellByColumnAndRow(12, $row)->getValue(): 0;
    // $standard_instalmemt =($worksheet->getCellByColumnAndRow(13, $row)->getValue())? $worksheet->getCellByColumnAndRow(13, $row)->getValue(): 0;
     $standard_instalmemt=0;
     $inst_paid = 0;
     //$esi_consider = $worksheet->getCellByColumnAndRow(16, $row)->getValue();
     $esi_consider=0;
     //$sdscope = ($worksheet->getCellByColumnAndRow(18, $row)->getValue())? $worksheet->getCellByColumnAndRow(18, $row)->getValue(): 0;
     $sdscope=0;
     $pf_consider=1;
    // $pf_consider = $worksheet->getCellByColumnAndRow(20, $row)->getValue();
     $data= array(
      'emp_bid'   => $emp_bid,
      'payment_type_id'    => $payment_type_id,
      'acc_no'  => $acc_no,
      'uan'   => $uan,
      'pf_no'   => $pf_no,
      'esi_no'   => $esi_no,
      'other_allowance' => $other_allowance,
      'basic'   => $basic,
      'incr'   => $incr,
      'co_allowance'    => $co_allowance,
      'da'  => $da,
      'special_allowance'   => $special_allowance,
      'hra'   => $hra,
      'bonus'   => 0,
      'cca' => $cca,
      'standard_instalmemt'  => $standard_instalmemt,
      'inst_paid'   => $inst_paid,
      'esi_consider'   => $esi_consider,
      'is_pay_hold'    => 0,
      'sdscope'  => $sdscope,
      'pf_consider'=>$pf_consider,
      'inserted_date'   => date('Y-m-d h:m:s'),
      'inserted_by'=>1,
      'updated_by'  => 1,
      'updated_date'   => date('Y-m-d h:m:s'),
      
     );
     //echo json_encode($data);exit();
    $this->EmployeeExcelUploadModel->insertSalaries($data);
    }
   }
   //INSERT INTO `employee_pay_details` (`emp_bid`, `payment_type_id`, `acc_no`, `uan`, `pf_no`, `esi_no`, `basic`, `other_allowance`, `incr`, `co_allowance`, `da`, `special_allowance`, `hra`, `bonus`, `cca`, `standard_instalmemt`, `inst_paid`, `esi_consider`, `is_pay_hold`, `sdscope`, `pf_consider`, `inserted_date`, `inserted_by`, `updated_date`, `updated_by`) VALUES ('315', '2', '912010060520842', '100128432059', '510', '5209293355', '98400', '15000', '2400', '0', '5160', '10200', '0', '0', '0', '10', '0', '1', '0', '1', '1', '2018-12-09 17:51:42', '1', '2018-12-09 00:00:00', '1');
   echo "{'status':'success'}";
  }
 }
}

?>