<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'controllers/Global_System.php';
class Holidays extends Global_System {
	
	public function __construct()
	{
		parent::__construct();
		$this->logincheck();
		
	}
	public function index()
	{
		//$this->output->enable_profiler(TRUE);
		$get_holidays = $this->Global_model->get_values_where($this->config->item('holidays'),array('status'=>1));
		$get_holidays_log = $this->Global_model->get_values_where($this->config->item('holidays_log'), array('flag'=>2));
		$data=array('current_page'=>'Holidays','page_title'=>'Holidays','parent_menu'=>'','template'=>'masters/holidays/add','get_holidays'=>$get_holidays,'get_holidays_log'=>$get_holidays_log);
		//$this->load->view('theme/template',$data);
		$this->form_validation->set_rules('holiday_name','holiday_name','required');
		 if($this->form_validation->run() === FALSE)
		{
			$this->load->view('theme/template',$data);
		}
		else
		{	
			$emp_id  = $this->session->userdata('emp_id');
			$holiday_name = $this->input->post('holiday_name');
			$from_date = $this->input->post('from_date');
			$to_date = $this->input->post('to_date');
			$status = ($this->input->post('status'))?1:0;
			$this->db->trans_begin();
			$insert_array = array('holiday_name'=>$holiday_name,'from_date'=>$from_date,'to_date'=>$to_date, 'status'=>$status);
			$this->all_actions('insert',$insert_array);
			if($this->db->trans_status()===FALSE)
			{
				$this->db->trans_rollback();
				$this->session->set_userdata(array('create_status'=>'failed'));
				$this->session->set_userdata(array('record_name'=>'Holidays not inserted '));	
			}
			else
			{
				$this->db->trans_commit();
				$this->session->set_userdata(array('create_status'=>'success'));
				$this->session->set_userdata(array('record_name'=>'Holidays inserted successfully'));
			}
			redirect('Holidays');
		} 
		
	}
	function edit($id)
	{
		$table = $this->config->item('holidays');
		$where = array('holiday_id'=>base64_decode($id));
		$holiday_id = base64_decode($id);
		$getPpaymentTypeEdit = $this->Global_model->get_values_where_single($table,$where);
		$get_holidays = $this->Global_model->get_values_where($this->config->item('holidays'),array('status'=>1));
		$get_holidays_log = $this->Global_model->get_values_where($this->config->item('holidays_log'), array('flag'=>2));
			$data=array('current_page'=>'Holidays','page_title'=>'Holidays','parent_menu'=>'','template'=>'masters/holidays/add','get_holidays'=>$get_holidays,'getPpaymentTypeEdit'=>$getPpaymentTypeEdit,'get_holidays_log'=>$get_holidays_log);
		$this->form_validation->set_rules('holiday_name','holiday_name','required');
		if($this->form_validation->run() === FALSE)
		{
			$this->load->view('theme/template',$data);
		}
		else
		{
			$emp_id  = $this->session->userdata('emp_id');
			$holiday_name = $this->input->post('holiday_name');
			$from_date = $this->input->post('from_date');
			$to_date = $this->input->post('to_date');
			$status = ($this->input->post('status'))?1:0;
			$this->db->trans_begin();
			$edit_array = array('holiday_name'=>$holiday_name,'from_date'=>$from_date,'to_date'=>$to_date, 'status'=>$status,'holiday_id'=>$holiday_id);
			$this->all_actions('edit',$edit_array);
			if($this->db->trans_status()===FALSE)
			{
				$this->db->trans_rollback();
				$this->session->set_userdata(array('create_status'=>'failed'));
				$this->session->set_userdata(array('record_name'=>'Holidays not updated'));	
			}
			else
			{
				$this->db->trans_commit();
				$this->session->set_userdata(array('create_status'=>'success'));
				$this->session->set_userdata(array('record_name'=>'Holidays updated successfully'));
			}
			redirect('Holidays');
		}
		
	}
	function delete($id)
	{
			$holiday_id = base64_decode($id);
			$emp_id  = $this->session->userdata('emp_id');
			$this->db->trans_begin();
			$delete_array = array('holiday_id'=>$holiday_id);
			$this->all_actions('delete',$delete_array);
		if($this->db->trans_status()===FALSE)
		{
			$this->db->trans_rollback();
			$this->session->set_userdata(array('create_status'=>'failed'));
			$this->session->set_userdata(array('record_name'=>'Holidays not Deleted '));	
		}
		else
		{
			$this->db->trans_commit();
			$this->session->set_userdata(array('create_status'=>'success'));
			$this->session->set_userdata(array('record_name'=>'Holidays  Deleted successfully'));
		}
		redirect('Holidays');
		
	}
	function undo($id)
	{
				$emp_id  = $this->session->userdata('emp_id');
				$this->db->trans_begin();
				$holiday_id = base64_decode($id);
				$undo_array = array('holiday_id'=>$holiday_id);
				$this->all_actions('undo',$undo_array);
				
				if($this->db->trans_status()===FALSE)
				{
					$this->db->trans_rollback();
					$this->session->set_userdata(array('create_status'=>'failed'));
					$this->session->set_userdata(array('record_name'=>'Holidays not Undo '));	
				}
				else
				{
					$this->db->trans_commit();
					$this->session->set_userdata(array('create_status'=>'success'));
					$this->session->set_userdata(array('record_name'=>'Holidays undo successfully'));
				}
		
		redirect('Holidays');
	}
	function all_actions($action,$insert_array)
	{
		
		$emp_id  = $this->session->userdata('emp_id');
		$holiday_name = $insert_array['holiday_name'];
		
		$status = $insert_array['status'];
		if($action == "insert")
		{
			$from_date = $insert_array['from_date'];
			$param['insertdata'] = array('holiday_id'=>$this->Global_model->Max_count($this->config->item('holidays'),'holiday_id'),'holiday_name'=>$holiday_name,'from_date'=>$insert_array['from_date'],'to_date'=>$insert_array['to_date'],'status'=>$status,'Created_By'=>$emp_id,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"),'date'=> $this->localdate("Y-m-d"));
			$param['table'] = $this->config->item('holidays');
			$this->Global_model->insert_data($param);
		}
		else if($action == "edit")
		{
			$from_date = $insert_array['from_date'];
			$holiday_id = $insert_array['holiday_id'];
			/* Update log table start */
				$insertdata =$this->Global_model->get_values_where_single($this->config->item('holidays'), array('holiday_id'=>$holiday_id));
				
				$param['insertdata'] = array('holiday_id'=>$insertdata->holiday_id,'holiday_name'=>$insertdata->holiday_name,'from_date'=>$insertdata->from_date,'to_date'=>$insertdata->to_date,'status'=>$insertdata->status,'Created_By'=>$emp_id,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"),'Updated_By'=>$emp_id,'Updated_Datetime'=> $this->localdate("Y-m-d H:i:s"),'date'=> $this->localdate("Y-m-d"),'flag'=>1,'description'=>'EDIT');
				$param['table'] = $this->config->item('holidays_log');
			/* Update log table end */
			$data1 = array('holiday_id'=>$holiday_id,'holiday_name'=>$holiday_name,'from_date'=>$from_date,'to_date'=>$insert_array['to_date'],'status'=>$status,'Updated_By'=>$emp_id,'Updated_Datetime'=> $this->localdate("Y-m-d H:i:s"));
			$where = array('holiday_id'=>$holiday_id);
			$this->Global_model->update($this->config->item('holidays'),$data1,$where);
			$this->Global_model->insert_data($param);
		}
		else if($action == 'delete') {
			$holiday_id = $insert_array['holiday_id'];
			/* Update log table start */
				$insertdata =$this->Global_model->get_values_where_single($this->config->item('holidays'), array('holiday_id'=>$holiday_id));
				
				$param['insertdata'] = array('holiday_id'=>$insertdata->holiday_id,'holiday_name'=>$insertdata->holiday_name,'from_date'=>$insertdata->from_date,'to_date'=>$insertdata->to_date,'status'=>$insertdata->status,'Created_By'=>$emp_id,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"),'Updated_By'=>$emp_id,'Updated_Datetime'=> $this->localdate("Y-m-d H:i:s"),'date'=> $this->localdate("Y-m-d"),'flag'=>1,'flag'=>2,'description'=>'DELETE');
				$param['table'] = $this->config->item('holidays_log');
			/* Update log table end */
			$this->db->query("DELETE FROM ".$this->config->item('holidays')." WHERE holiday_id =".$holiday_id);
			$this->Global_model->insert_data($param);
		}
		else if($action == 'undo') {
			$holiday_id = $insert_array['holiday_id'];
			$where = array('holiday_id'=>$holiday_id);
			$getPaymentTypes = $this->db->query("SELECT * FROM ".$this->config->item('holidays_log')." WHERE holiday_id=".$holiday_id." ORDER BY `Created_Datetime` DESC")->first_row();
			if(!empty($getPaymentTypes)) { 
				$param['insertdata'] = array('holiday_id'=>$holiday_id,'holiday_name'=>$getPaymentTypes->holiday_name,'from_date'=>$getPaymentTypes->from_date,'status'=>$getPaymentTypes->status,'Created_By'=>$emp_id,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"),'Updated_By'=>$emp_id,'Updated_Datetime'=> $this->localdate("Y-m-d H:i:s"));
				$this->Global_model->update($this->config->item('holidays'),$param['insertdata'],$where);
			}
			else {
					$this->db->trans_rollback();
					$this->session->set_userdata(array('create_status'=>'failed'));
					$this->session->set_userdata(array('record_name'=>'Holidays not Undo '));	
					redirect('Holidays');
			}
		}
		else {
				$this->session->set_userdata(array('create_status'=>'failed'));
				$this->session->set_userdata(array('record_name'=>'Contact System administrator'));	
				redirect('Holidays');
		}	
	}
}
