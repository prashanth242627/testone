<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class EmployeeSavings extends CI_Controller {
	public function __construct(){
		parent::__construct(); 
		$this->load->library('session');
		$this->load->helper('url');
		$this->load->model('Employee_model');
		$this->load->model('Savings_model');
		$this->load->model('Salary_model');
	}
	public function index()
	{
		$activities=$this->Employee_model->activities();
		$employees_bid=$this->Employee_model->employeesBid();
		$employees_name=$this->Employee_model->employeesName();
		$payment_modes=$this->Savings_model->paymentModes();
		$data=array('current_page'=>'Savings Details','page_title'=>'Savings Details','parent_menu'=>'','template'=>'SavingsDetails');
		$data['activities']= $activities;
		$data['bids']= $employees_bid;
		$data['names']=$employees_name;
		$data['payment_modes']=$payment_modes;
		$employees=array();
		$this->load->view('theme/template',$data);
	}
	public function All()
	{
		$activities=$this->Employee_model->activities();
		$employees_bid=$this->Employee_model->employeesBid();
		$employees_name=$this->Employee_model->employeesName();
		$payment_modes=$this->Savings_model->paymentModes();
		$employees=$this->Savings_model->AllEmployeesSavingsData();
		$data=array('current_page'=>'Savings Details','page_title'=>'Savings Details','parent_menu'=>'','template'=>'SavingsDetails');
		$data['activities']= $activities;
		$data['bids']= $employees_bid;
		$data['names']=$employees_name;
		$data['payment_modes']=$payment_modes;
		if ($employees) 
		{
			$data['employees'] = $employees;
		}
		$this->load->view('theme/template',$data);
	}
	public function Activity()
	{
		$activity_type=$this->input->get('type');
		$bid=$this->input->get('bid');
		$ename=$this->input->get('ename');
		$payment_mode=$this->input->get('payment_mode');
		$start_date=$this->input->get('from');
		$end_date=$this->input->get('to');
		$activities=$this->Employee_model->activities();
		$employees_bid=$this->Employee_model->employeesBid();
		$employees_name=$this->Employee_model->employeesName();
		$payment_modes=$this->Savings_model->paymentModes();
		$employees=array();
		if ($activity_type!=null && $payment_mode==null) {
			$employees=$this->Savings_model->EmployeeSavingsDataWhere("tds_cal.dept_name='".$activity_type."'");
		}
		else if ($bid!=null) {
			$employees=$this->Savings_model->EmployeeSavingsDataWhere("tds_cal.emp_bid=".$bid);
		}
		else if ($ename!=null) {
			$employees=$this->Savings_model->EmployeeSavingsDataWhere("tds_cal.emp_name='".$ename."'");
		}
		else if ($payment_mode!=null)  {
			$employees=$this->Savings_model->EmployeeSavingsDataWhere($payment_mode);
		}
		$data=array('current_page'=>'Savings Details','page_title'=>'Savings Details','parent_menu'=>'','template'=>'SavingsDetails');
		$data['activities']= $activities;
		$data['bids']= $employees_bid;
		$data['names']=$employees_name;
		$data['payment_modes']=$payment_modes;
		if (count($employees)) 
		{
			$data['employees'] = $employees;
		}
		$this->load->view('theme/template',$data);
	}

}
