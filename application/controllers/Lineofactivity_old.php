<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'controllers/Global_System.php';
class Lineofactivity extends Global_System {
	
	public function __construct()
	{
		parent::__construct();
		$this->logincheck();
		
	}
	public function index()
	{
		//$this->output->enable_profiler(TRUE);
		$get_line_of_activity = $this->Global_model->get_values_where($this->config->item('line_of_activity'),array('status'=>1));
		$get_line_of_activity_log = $this->Global_model->get_values_where($this->config->item('line_of_activity_log'), array('flag'=>2));
		$data=array('current_page'=>'Line of Activity','page_title'=>'Line of Activity','parent_menu'=>'','template'=>'masters/lineofactivity/add','get_line_of_activity'=>$get_line_of_activity,'get_line_of_activity_log'=>$get_line_of_activity_log);
		//$this->load->view('theme/template',$data);
		$this->form_validation->set_rules('dept_name','dept_name','required');
		 if($this->form_validation->run() === FALSE)
		{
			$this->load->view('theme/template',$data);
		}
		else
		{	
			$emp_id  = $this->session->userdata('emp_id');
			$dept_name = $this->input->post('dept_name');
			$dept_id = $this->input->post('dept_id');
			$status = ($this->input->post('status'))?1:0;
			$this->db->trans_begin();
			$insert_array = array('dept_name'=>$dept_name,'dept_id'=>$dept_id, 'status'=>$status);
			$this->all_actions('insert',$insert_array);
			if($this->db->trans_status()===FALSE)
			{
				$this->db->trans_rollback();
				$this->session->set_userdata(array('create_status'=>'failed'));
				$this->session->set_userdata(array('record_name'=>'Line of Activity not inserted '));	
			}
			else
			{
				$this->db->trans_commit();
				$this->session->set_userdata(array('create_status'=>'success'));
				$this->session->set_userdata(array('record_name'=>'Line of Activity inserted successfully'));
			}
			redirect('Lineofactivity');
		} 
		
	}
	function edit($id)
	{
		$table = $this->config->item('line_of_activity');
		$where = array('activity_id'=>base64_decode($id));
		$activity_id = base64_decode($id);
		$getPpaymentTypeEdit = $this->Global_model->get_values_where_single($table,$where);
		$get_line_of_activity = $this->Global_model->get_values_where($this->config->item('line_of_activity'),array('status'=>1));
		$get_line_of_activity_log = $this->Global_model->get_values_where($this->config->item('line_of_activity_log'), array('flag'=>2));
			$data=array('current_page'=>'Line of Activity','page_title'=>'Line of Activity','parent_menu'=>'','template'=>'masters/lineofactivity/add','get_line_of_activity'=>$get_line_of_activity,'getPpaymentTypeEdit'=>$getPpaymentTypeEdit,'get_line_of_activity_log'=>$get_line_of_activity_log);
		$this->form_validation->set_rules('dept_name','dept_name','required');
		if($this->form_validation->run() === FALSE)
		{
			$this->load->view('theme/template',$data);
		}
		else
		{
			$emp_id  = $this->session->userdata('emp_id');
			$dept_name = $this->input->post('dept_name');
			$dept_id = $this->input->post('dept_id');
			$status = ($this->input->post('status'))?1:0;
			$this->db->trans_begin();
			$edit_array = array('dept_name'=>$dept_name,'dept_id'=>$dept_id, 'status'=>$status,'activity_id'=>$activity_id);
			$this->all_actions('edit',$edit_array);
			if($this->db->trans_status()===FALSE)
			{
				$this->db->trans_rollback();
				$this->session->set_userdata(array('create_status'=>'failed'));
				$this->session->set_userdata(array('record_name'=>'Line of Activity not updated'));	
			}
			else
			{
				$this->db->trans_commit();
				$this->session->set_userdata(array('create_status'=>'success'));
				$this->session->set_userdata(array('record_name'=>'Line of Activity updated successfully'));
			}
			redirect('Lineofactivity');
		}
		
	}
	function delete($id)
	{
			$activity_id = base64_decode($id);
			$emp_id  = $this->session->userdata('emp_id');
			$this->db->trans_begin();
			$delete_array = array('activity_id'=>$activity_id);
			$this->all_actions('delete',$delete_array);
		if($this->db->trans_status()===FALSE)
		{
			$this->db->trans_rollback();
			$this->session->set_userdata(array('create_status'=>'failed'));
			$this->session->set_userdata(array('record_name'=>'Line of Activity not Deleted '));	
		}
		else
		{
			$this->db->trans_commit();
			$this->session->set_userdata(array('create_status'=>'success'));
			$this->session->set_userdata(array('record_name'=>'Line of Activity  Deleted successfully'));
		}
		redirect('Lineofactivity');
		
	}
	function undo($id)
	{
				$emp_id  = $this->session->userdata('emp_id');
				$this->db->trans_begin();
				$activity_id = base64_decode($id);
				$undo_array = array('activity_id'=>$activity_id);
				$this->all_actions('undo',$undo_array);
				
				if($this->db->trans_status()===FALSE)
				{
					$this->db->trans_rollback();
					$this->session->set_userdata(array('create_status'=>'failed'));
					$this->session->set_userdata(array('record_name'=>'Line of Activity not Undo '));	
				}
				else
				{
					$this->db->trans_commit();
					$this->session->set_userdata(array('create_status'=>'success'));
					$this->session->set_userdata(array('record_name'=>'Line of Activity undo successfully'));
				}
		
		redirect('Lineofactivity');
	}
	function all_actions($action,$insert_array)
	{
		
		$emp_id  = $this->session->userdata('emp_id');
		$dept_name = $insert_array['dept_name'];
		
		$status = $insert_array['status'];
		if($action == "insert")
		{
			$dept_id = $insert_array['dept_id'];
			$param['insertdata'] = array('activity_id'=>$this->Global_model->Max_count($this->config->item('line_of_activity'),'activity_id'),'dept_name'=>$dept_name,'dept_id'=>$insert_array['dept_id'],'status'=>$status,'Created_By'=>$emp_id,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"),'date'=> $this->localdate("Y-m-d"));
			$param['table'] = $this->config->item('line_of_activity');
			$this->Global_model->insert_data($param);
		}
		else if($action == "edit")
		{
			$dept_id = $insert_array['dept_id'];
			$activity_id = $insert_array['activity_id'];
			/* Update log table start */
				$insertdata =$this->Global_model->get_values_where_single($this->config->item('line_of_activity'), array('activity_id'=>$activity_id));
				
				$param['insertdata'] = array('activity_id'=>$insertdata->activity_id,'dept_name'=>$insertdata->dept_name,'dept_id'=>$insertdata->dept_id,'status'=>$insertdata->status,'Created_By'=>$emp_id,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"),'Updated_By'=>$emp_id,'Updated_Datetime'=> $this->localdate("Y-m-d H:i:s"),'date'=> $this->localdate("Y-m-d"),'flag'=>1,'description'=>'EDIT');
				$param['table'] = $this->config->item('line_of_activity_log');
			/* Update log table end */
			$data1 = array('activity_id'=>$activity_id,'dept_name'=>$dept_name,'dept_id'=>$dept_id,'status'=>$status,'Updated_By'=>$emp_id,'Updated_Datetime'=> $this->localdate("Y-m-d H:i:s"));
			$where = array('activity_id'=>$activity_id);
			$this->Global_model->update($this->config->item('line_of_activity'),$data1,$where);
			$this->Global_model->insert_data($param);
		}
		else if($action == 'delete') {
			$activity_id = $insert_array['activity_id'];
			/* Update log table start */
				$insertdata =$this->Global_model->get_values_where_single($this->config->item('line_of_activity'), array('activity_id'=>$activity_id));
				
				$param['insertdata'] = array('activity_id'=>$insertdata->activity_id,'dept_name'=>$insertdata->dept_name,'dept_id'=>$insert_array['dept_id'],'status'=>$insertdata->status,'Created_By'=>$emp_id,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"),'Updated_By'=>$emp_id,'Updated_Datetime'=> $this->localdate("Y-m-d H:i:s"),'date'=> $this->localdate("Y-m-d"),'flag'=>1,'flag'=>2,'description'=>'DELETE');
				$param['table'] = $this->config->item('line_of_activity_log');
			/* Update log table end */
			$this->db->query("DELETE FROM ".$this->config->item('line_of_activity')." WHERE activity_id =".$activity_id);
			$this->Global_model->insert_data($param);
		}
		else if($action == 'undo') {
			$activity_id = $insert_array['activity_id'];
			$where = array('activity_id'=>$activity_id);
			$getPaymentTypes = $this->db->query("SELECT * FROM ".$this->config->item('line_of_activity_log')." WHERE activity_id=".$activity_id." ORDER BY `Created_Datetime` DESC")->first_row();
			if(!empty($getPaymentTypes)) { 
				$param['insertdata'] = array('activity_id'=>$activity_id,'dept_name'=>$getPaymentTypes->dept_name,'dept_id'=>$getPaymentTypes->dept_id,'status'=>$getPaymentTypes->status,'Created_By'=>$emp_id,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"),'Updated_By'=>$emp_id,'Updated_Datetime'=> $this->localdate("Y-m-d H:i:s"));
				$this->Global_model->update($this->config->item('line_of_activity'),$param['insertdata'],$where);
			}
			else {
					$this->db->trans_rollback();
					$this->session->set_userdata(array('create_status'=>'failed'));
					$this->session->set_userdata(array('record_name'=>'Line of Activity not Undo '));	
					redirect('Lineofactivity');
			}
		}
		else {
				$this->session->set_userdata(array('create_status'=>'failed'));
				$this->session->set_userdata(array('record_name'=>'Contact System administrator'));	
				redirect('Lineofactivity');
		}	
	}
}
