<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ManualVSBioMetricReports extends CI_Controller {
	public function __construct(){
		parent::__construct(); 
		$this->load->library('session');
		$this->load->helper('url');
		$this->load->model('Employee_model');
		$this->load->model("attendance_reports_model");
	}
	public function index()
	{
		$activities=$this->Employee_model->activities();
		$employees_bid=$this->Employee_model->employeesBid();
		$employees_name=$this->Employee_model->employeesName();
		$get_leave_types  = $this->Global_model->get_leave_types();
		$data=array('current_page'=>'BioMetric Reports','page_title'=>'BioMetric Reports','parent_menu'=>'','template'=>'manual_bio_metric_diff');
		$data['activities']=$activities;
		$data['leave_types']=$get_leave_types;
		$data['bids']= $employees_bid;
		$data['names']=$employees_name;
		$data['reports']=$get_current_date_reports;
		$this->load->view('theme/template',$data);
	}
	public function Activity()
	{
		$bid=$this->input->get('bid');
		$ename=$this->input->get('ename');
		$year=$this->input->get('year');
		$month=$this->input->get('month');
		$activities=$this->Employee_model->activities();
		$employees_bid=$this->Employee_model->employeesBid();
		$employees_name=$this->Employee_model->employeesName();
		$get_leave_types  = $this->Global_model->get_leave_types();

		$data['reports'] = array();
		if ($year!=null && $month!=null && $bid!=null) {
			$a_reports=$this->attendance_reports_model->getManulaAndBioMetricReportsByBidMonth($bid,$month,$year);
		}
		else if ($year!=null && $month!=null && $ename!=null) {
			$a_reports=$this->attendance_reports_model->getManulaAndBioMetricReportsByNameMonth($ename,$month,$year);
		}

		$data=array('current_page'=>'Dashboard','page_title'=>'Dashboard','parent_menu'=>'','template'=>'manual_bio_metric_diff');
		$data['activities']= $activities;
		$data['bids']= $employees_bid;
		$data['names']=$employees_name;
		$data['leave_types']=$get_leave_types;
		if (count($a_reports)) 
		{
			$data['reports'] = $a_reports;
		}
		$this->load->view('theme/template',$data);
	}
}
