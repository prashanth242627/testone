<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ListOfAbsenties extends CI_Controller {
	public function __construct(){
		parent::__construct(); 
		$this->load->library('session');
		$this->load->helper('url');
		$this->load->model('list_of_absenties_model');
	}
	public function index()
	{
		$activities=$this->list_of_absenties_model->activities();
		$employees_bid=$this->list_of_absenties_model->employeesBid();
		$employees_name=$this->list_of_absenties_model->employeesName();
		$leave_type=$this->list_of_absenties_model->leaveType();
		$no_of_leaves=$this->list_of_absenties_model->noOfLeaves();
		$data['activities']=$activities;
		$data['bids']= $employees_bid;
		$data['names']=$employees_name;
		$data['leave_type']=$leave_type;
		//$data['no_of_leaves']=$no_of_leaves;
		$this->load->view('list_of_absenties',$data);
	}
	public function All()
	{
		$activities=$this->list_of_absenties_model->activities();
		$employees=$this->list_of_absenties_model->allEmployees();
		$employees_bid=$this->list_of_absenties_model->employeesBid();
		$employees_name=$this->list_of_absenties_model->employeesName();
		$no_of_leaves=$this->list_of_absenties_model->noOfLeaves();
		$leave_type=$this->list_of_absenties_model->leaveType();
		$data['activities']=$activities;
		$data['bids']= $employees_bid;
		$data['names']=$employees_name;
		$data['leave_type']=$leave_type;
		$data['no_of_leaves']=$no_of_leaves;
		if ($employees) 
		{
			$data['employees'] = $employees;
		}
		$this->load->view('list_of_absenties', $data);
	}
	public function Activity()
	{
		$activity_type=$this->input->get('type');
		$bid=$this->input->get('bid');
		$ename=$this->input->get('ename');
		$status=$this->input->get('status');
		$start_date=$this->input->get('from');
		$end_date=$this->input->get('to');
		$no_of_leaves=array();
		$no_of_leaves=$this->list_of_absenties_model->noOfLeaves();
		$activities=$this->list_of_absenties_model->activities();
		$employees_bid=$this->list_of_absenties_model->employeesBid();
		$employees_name=$this->list_of_absenties_model->employeesName();
		$leave_type=$this->list_of_absenties_model->leaveType();
		
		if ($activity_type!=null) {
			
			$no_of_leaves=$this->list_of_absenties_model->lineOfActivityEmployees($activity_type);
		}
		else if ($bid!=null) {
			$no_of_leaves=$this->list_of_absenties_model->noOfLeavesByWhere("e.emp_bid=".$bid);
		}
		else if ($ename!=null) {
			$no_of_leaves=$this->list_of_absenties_model->noOfLeavesByWhere("e.emp_name='".$ename."'");
		}
		else if ($status!=null) {
			if ($status=="Active") {
				$no_of_leaves=$this->list_of_absenties_model->activeEmployees();
			}
			else if ($status=="Inactive") {
				$no_of_leaves=$this->list_of_absenties_model->inActiveEmployees();
			}
			else if ($status=="New") {
				$no_of_leaves=$this->list_of_absenties_model->newEmployees();
			}
		}
		else if ($start_date!=null) {
			$no_of_leaves=$this->list_of_absenties_model->noOfLeavesByWhere("date_of_join>='".$start_date."' and date_of_join <='".$end_date."'");
		}
		else if ($end_date!="" && $start_date==null) {
			$no_of_leaves=$this->list_of_absenties_model->getEmployeesData("date_of_join <='".$end_date."'");
		}
		$data['activities']= $activities;
		$data['bids']= $employees_bid;
		$data['names']=$employees_name;
		$data['leave_type']=$leave_type;
		if (count($no_of_leaves)) 
		{
			$data['no_of_leaves'] = $no_of_leaves;
			
    		$this->load->view('list_of_absenties', $data);
		}else{
			$this->load->view('list_of_absenties',$data);
		}
	}
	public function Salaries()
	{
		$activities=$this->list_of_absenties_model->activities();
		$employees_bid=$this->list_of_absenties_model->employeesBid();
		$employees_name=$this->list_of_absenties_model->employeesName();
		$payment_modes=$this->list_of_absenties_model->paymentModes();
		$data['activities']= $activities;
		$data['bids']= $employees_bid;
		$data['names']=$employees_name;
		$data['payment_modes']=$payment_modes;
		$employees=array();
		$this->load->view('list_of_absenties',$data);
	}

}
