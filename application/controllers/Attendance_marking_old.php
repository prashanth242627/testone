<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'controllers/Global_System.php';
class Attendance_marking extends Global_System {
	
	public function __construct()
	{
		parent::__construct();
		$this->logincheck();
		
	}
	public function index()
	{
		//$this->output->enable_profiler(TRUE);
		$get_attendance_marking = $this->Global_model->get_attendance_marking();
		$get_attendance_marking_log = $this->Global_model->get_attendance_marking_log();
		$get_employees = $this->Global_model->get_values_where($this->config->item('employee'),array('status'=>1));
		$get_holidays = $this->Global_model->get_values_where($this->config->item('holidays'),array('status'=>1));
		$get_leave_types  = $this->Global_model->get_leave_types();
		$get_current_date_leaves = $this->Global_model->get_current_date_leaves_count();
		$get_current_date_leave_emps=$this->Global_model->get_current_date_leave_emps();
		//$get_emp_leavemaster  = $this->Global_model->get_emp_leavemaster($id);
		$data=array('current_page'=>'Attendance Marking','page_title'=>'Attendance Marking','parent_menu'=>'','template'=>'masters/attendance_marking/add','get_attendance_marking'=>$get_attendance_marking,'get_attendance_marking_log'=>$get_attendance_marking_log,'get_employees'=>$get_employees,'get_holidays'=>$get_holidays,"get_leave_types"=>$get_leave_types,"get_current_date_leaves"=>$get_current_date_leaves,"get_current_date_leave_emps"=>$get_current_date_leave_emps);
		$this->form_validation->set_rules('emp_name','emp_name','required');
		
		if($this->form_validation->run() === FALSE)
		{
			$this->load->view('theme/template',$data);
		}
		else
		{	
			$emp_id  = $this->session->userdata('emp_id');
			$emp_bid = $this->input->post('emp_bid');
			$leave_date = $this->input->post('leave_date');
			$leave_master_id = $this->input->post('leave_master_id');
			$no_leaves = $this->input->post('no_leaves');
			$lemp_id = $this->input->post('emp_id');
			$this->db->trans_begin();
			$insert_array = array('emp_bid'=>$emp_bid, 'leave_date'=>$leave_date, 'leave_master_id'=>$leave_master_id,'no_leaves'=>$no_leaves,'emp_id'=>$lemp_id);
			
			$this->all_actions('insert',$insert_array);
			if($this->db->trans_status()===FALSE)
			{
				$this->db->trans_rollback();
				$this->session->set_userdata(array('create_status'=>'failed'));
				$this->session->set_userdata(array('record_name'=>'Attendance Marking not inserted '));	
			}
			else
			{
				$this->db->trans_commit();
				$this->session->set_userdata(array('create_status'=>'success'));
				$this->session->set_userdata(array('record_name'=>'Attendance Marking inserted successfully'));
			}
			$get_leave_types  = $this->Global_model->get_leave_types();
			$data['get_leave_types'] = $get_leave_types;
			redirect('attendance_marking');
		} 
		
	}
	function edit($id)
	{
		$table = $this->config->item('attendance_marking');
		$where = array('leave_master_id'=>base64_decode($id));
		$leave_master_id = base64_decode($id);
		$getAttendanceMarkingEdit = $this->Global_model->get_leave_master_single($leave_master_id);
		$get_attendance_marking = $this->Global_model->get_attendance_marking();
		$get_attendance_marking_log = $this->Global_model->get_attendance_marking_log();
		$get_current_date_leave_emps=$this->Global_model->get_current_date_leave_emps();

		$get_employees = $this->Global_model->get_values_where($this->config->item('employee'),array('status'=>1));
		$get_holidays = $this->Global_model->get_values_where($this->config->item('holidays'),array('status'=>1));
		$data=array('current_page'=>'Attendance Marking','page_title'=>'Attendance Marking','parent_menu'=>'','template'=>'masters/attendance_marking/add','get_attendance_marking'=>$get_attendance_marking,'getAttendanceMarkingEdit'=>$getAttendanceMarkingEdit,'get_attendance_marking_log'=>$get_attendance_marking_log,'get_employees'=>$get_employees,'get_holidays'=>$get_holidays);
		$this->form_validation->set_rules('emp_name','emp_name','required');
		$get_leave_types  = $this->Global_model->get_leave_types();
		$get_current_date_leaves = $this->Global_model->get_current_date_leaves_count();
		$data['get_current_date_leave_emps']=$get_current_date_leave_emps;
		$data['get_leave_types'] = $get_leave_types;
		$data['get_current_date_leaves'] = $get_current_date_leaves;
		if($this->form_validation->run() === FALSE)
		{
			$this->load->view('theme/template',$data);
		}
		else
		{
			$emp_id  = $this->session->userdata('emp_id');
			$emp_bid = $this->input->post('emp_bid');
			$activity_id = $this->input->post('activity_id');
			$leave_type_id = $this->input->post('leave_type_id');
			$no_leaves = $this->input->post('no_leaves');
			$calendar_year = $this->input->post('calendar_year');
			$lemp_id = $this->input->post('emp_id');
			$this->db->trans_begin();
			$edit_array = array('activity_id'=>$activity_id,'emp_bid'=>$emp_bid, 'leave_type_id'=>$leave_type_id, 'no_leaves'=>$no_leaves,'emp_id'=>$lemp_id,'attendance_id'=>$attendance_id,'calendar_year'=>$calendar_year);
			$this->all_actions('edit',$edit_array);
			if($this->db->trans_status()===FALSE)
			{
				$this->db->trans_rollback();
				$this->session->set_userdata(array('create_status'=>'failed'));
				$this->session->set_userdata(array('record_name'=>'Attendance Marking not updated'));	
			}
			else
			{
				$this->db->trans_commit();
				$this->session->set_userdata(array('create_status'=>'success'));
				$this->session->set_userdata(array('record_name'=>'Attendance Marking updated successfully'));
			}
			redirect('attendance_marking');
		}
		
	}
	function delete($id)
	{
			$attendance_id = base64_decode($id);
			$emp_id  = $this->session->userdata('emp_id');
			$this->db->trans_begin();
			$delete_array = array('attendance_id'=>$attendance_id);
			$this->all_actions('delete',$delete_array);
		if($this->db->trans_status()===FALSE)
		{
			$this->db->trans_rollback();
			$this->session->set_userdata(array('create_status'=>'failed'));
			$this->session->set_userdata(array('record_name'=>'Attendance Marking not Deleted '));	
		}
		else
		{
			$this->db->trans_commit();
			$this->session->set_userdata(array('create_status'=>'success'));
			$this->session->set_userdata(array('record_name'=>'Attendance Marking  Deleted successfully'));
		}
		redirect('attendance_marking');
		
	}
	function undo($id)
	{
				$emp_id  = $this->session->userdata('emp_id');
				$this->db->trans_begin();
				$attendance_id = base64_decode($id);
				$undo_array = array('attendance_id'=>$attendance_id);
				$this->all_actions('undo',$undo_array);
				
				if($this->db->trans_status()===FALSE)
				{
					$this->db->trans_rollback();
					$this->session->set_userdata(array('create_status'=>'failed'));
					$this->session->set_userdata(array('record_name'=>'Attendance Marking not Undo '));	
				}
				else
				{
					$this->db->trans_commit();
					$this->session->set_userdata(array('create_status'=>'success'));
					$this->session->set_userdata(array('record_name'=>'Attendance Marking undo successfully'));
				}
		
		redirect('attendance_marking');
	}
	function all_actions($action,$insert_array)
	{
		
		$emp_id  = $this->session->userdata('emp_id');
		
		$status = $insert_array['status'];
		if($action == "insert")
		{
			$attendance_id = $insert_array['attendance_id'];
			$param['insertdata'] = array('attendance_id'=>$this->Global_model->Max_count($this->config->item('attendance_marking'),'attendance_id'),'emp_id'=>$insert_array['emp_id'],'leave_date'=>$insert_array['leave_date'],'leave_master_id'=>$insert_array['leave_master_id'],'no_leaves'=>$insert_array['no_leaves'],'Created_By'=>$emp_id,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"),'date'=> $this->localdate("Y-m-d"));
			$param['table'] = $this->config->item('attendance_marking');
			$this->Global_model->insert_data($param);
			$this->Global_model->empleaveupdate($insert_array['emp_id'],$insert_array['leave_master_id'],$insert_array['no_leaves']);
		}
		else if($action == "edit")
		{
  		    $attendance_id = $insert_array['attendance_id'];
			$data1 = array('attendance_id'=>$attendance_id,'emp_id'=>$insert_array['emp_id'],'leave_type_id'=>$insert_array['leave_type_id'],'no_leaves'=>$insert_array['no_leaves'],'Updated_By'=>$emp_id,'Updated_Datetime'=> $this->localdate("Y-m-d H:i:s"));
			$where = array('attendance_id'=>$attendance_id);
			$this->db->query("INSERT INTO `attendance_marking_log`(`attendance_id`, `emp_id`, `leave_type_id`, `no_leaves`, `calendar_year`,`date`, `flag`, `description`, `Created_By`, `Created_Datetime`, `Updated_By`, `Updated_Datetime`) (SELECT attendance_id, emp_id, leave_type_id, no_leaves, calendar_year, date, '1', 'EDIT', Created_By, Created_Datetime, Updated_By, Updated_Datetime FROM attendance_marking WHERE attendance_id= ".$attendance_id.")");
			$this->Global_model->update($this->config->item('attendance_marking'),$data1,$where);
		}
		else if($action == 'delete') {
			$attendance_id = $insert_array['attendance_id'];
			$this->db->query("INSERT INTO `attendance_marking_log`(`attendance_id`, `emp_id`, `leave_type_id`, `no_leaves`,  `calendar_year`,`date`, `flag`, `description`, `Created_By`, `Created_Datetime`, `Updated_By`, `Updated_Datetime`) (SELECT attendance_id, emp_id, leave_type_id, no_leaves,calendar_year, date, '2', 'DELETE', $emp_id, now(), $emp_id, now() FROM attendance_marking WHERE attendance_id= ".$attendance_id.")");
			$this->db->query("DELETE FROM ".$this->config->item('attendance_marking')." WHERE attendance_id =".$attendance_id);
		}
		else if($action == 'undo') {
			$attendance_id = $insert_array['attendance_id'];
			$where = array('attendance_id'=>$attendance_id);
			$getPaymentTypes = $this->db->query("SELECT * FROM ".$this->config->item('attendance_marking_log')." WHERE attendance_id=".$attendance_id." ORDER BY `Created_Datetime` DESC")->first_row();
			if(!empty($getPaymentTypes)) { 
				$param['insertdata'] = array('emp_id'=>$getPaymentTypes->emp_id,'leave_type_id'=>$getPaymentTypes->leave_type_id,'no_leaves'=>$getPaymentTypes->no_leaves,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"),'Updated_By'=>$emp_id,'Updated_Datetime'=> $this->localdate("Y-m-d H:i:s"));
				$this->Global_model->update($this->config->item('attendance_marking'),$param['insertdata'],$where);
			}
			else {
					$this->db->trans_rollback();
					$this->session->set_userdata(array('create_status'=>'failed'));
					$this->session->set_userdata(array('record_name'=>'Attendance Marking not Undo '));	
					redirect('attendance_marking');
			}
			
		}
		else {
				$this->session->set_userdata(array('create_status'=>'failed'));
				$this->session->set_userdata(array('record_name'=>'Contact System administrator'));	
				redirect('attendance_marking');
		}	
	}
	function getEmployeeDetails()
	{
		$emp_bid = $this->input->post('emp_bid');
		$get_attendance_markings = $this->db->query("SELECT * FROM `employee` e LEFT JOIN line_of_activity l ON e.`activity_id` = l.activity_id LEFT JOIN subject_details s ON e.subject_details_id = s.subject_details_id WHERE e.`emp_bid` = '".$emp_bid."'")->row();
		$data['emp_name'] = $get_attendance_markings->emp_name;
		$data['dept_name'] = $get_attendance_markings->dept_name;
		$data['sub_name'] = $get_attendance_markings->sub_name;
		$data['emp_id'] = $get_attendance_markings->emp_id;
		$get_emp_leavemaster  = $this->Global_model->get_emp_leavemaster($data['emp_id']);
		$get_emp_leavemaster_data = "";
		foreach($get_emp_leavemaster as $row)
		{
			$get_emp_leavemaster_data .= "<option value='".$row->leave_master_id."'>".$row->leave_type."</option>";
		}
		$data['get_emp_leavemaster'] = $get_emp_leavemaster_data;
		$year=date("Y");
		$get_employees_leaves_count_types=$this->Global_model->get_employees_leaves_count_type($data['emp_id'],$year);
		/*$employee_leave_types_id="<th>Leave Type</th>";
		$total_leaves_id="<th>Total Leaves</th>";
		$remaining_leaves_id="<th>Used Leaves</th>";*/
		$employee_leave_types_id="";
		$leave_type_value="";
		$total_leaves_id="";
		$remaining_leaves_id="";

		foreach ($get_employees_leaves_count_types as $get_employees_leaves_count_type) {
			$no_leaves=$get_employees_leaves_count_type->no_leaves;
			$used_count=$get_employees_leaves_count_type->used_count;
			$remain_leaves=$no_leaves-$used_count;
			$employee_leave_types_id.="<th>".$get_employees_leaves_count_type->leave_type."(".$remain_leaves.")</th>";
			$total_leaves_id.="<th>".$get_employees_leaves_count_type->no_leaves."</th>";
			$remaining_leaves_id.="<th>".$get_employees_leaves_count_type->used_count."</th>";
			$leave_type_value.="<td><input type='radio' name='leave_type' value='".$get_employees_leaves_count_type->leave_master_id."'></td>";
		}
		$data['employee_leave_types_id'] = $employee_leave_types_id;
		$data['total_leaves_id'] = $total_leaves_id;
		$data['remaining_leaves_id'] = $remaining_leaves_id;
		$data['leave_type_value']=$leave_type_value;
		echo json_encode($data);
	}
	function SummaryDetails()
	{
		$summary_date = $this->input->post('summary_date');
		//$summary_date=date("Y-m-d",$summary_date);
		$get_current_date_leaves = $this->Global_model->get_leaves_count_by_date($summary_date);
		$get_current_date_leave_emps=$this->Global_model->get_leave_emps_by_date($summary_date);
		$get_leave_types  = $this->Global_model->get_leave_types();
		$get_current_date_leaves_string="";
			$total_leaves=0;
		   foreach ($get_leave_types as $get_leave_type) {
		   	if (count($get_current_date_leaves)) {
			  	foreach ($get_current_date_leaves as $get_current_date_leave) {
			  		if ($get_leave_type->leave_type===$get_current_date_leave->leave_type) {
			  			$total_leaves+=$get_current_date_leave->leaves_count;
			  			$get_current_date_leaves_string.= "<td>".$get_current_date_leave->leaves_count."</td>";
			  		}
			  		else{
			  			$get_current_date_leaves_string.= "<td>"."0"."</td>";
			  		}
			  	}
		  	}
		  	else{
		  		$get_current_date_leaves_string.= "<td>"."0"."</td>";
		  	}
		  } 
		  $get_current_date_leaves_string.= "<th>".$total_leaves."</th>";
		$get_current_date_leave_emps_string="";
		if (count($get_current_date_leave_emps)>0) {
		  	$l=1;
		  	foreach ($get_current_date_leave_emps as $get_current_date_leave_emp) {
		  		$get_current_date_leave_emps_string.= "<tr><td>".$l++."</td><td>".$get_current_date_leave_emp->emp_bid."</td><td>".$get_current_date_leave_emp->emp_name."</td><td>".$get_current_date_leave_emp->emp_id."</td><td>".$get_current_date_leave_emp->leave_date."</td><td>".$get_current_date_leave_emp->dept_name."</td><td>".$get_current_date_leave_emp->sub_name."</td><td>".$get_current_date_leave_emp->leave_type."</td></tr>";
		  	}
		}
		else{
			$get_current_date_leave_emps_string.= "<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr><tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
		}
		$data['get_current_date_leave_emps']=$get_current_date_leaves_string;
		$data['get_current_date_leaves'] = $get_current_date_leave_emps_string;
		echo json_encode($data);
	}
	public function add_marking()
	{
		$leave_type=$this->input->post("leave_type");
		$data=$this->input->post();
		if ($leave_type!=null) {

			$data['status']=true;
			echo json_encode($data);
		}
		else{
			$data['status']=false;
			echo json_encode($data);
		}
		/*$param['insertdata'] = array('attendance_id'=>$this->Global_model->Max_count($this->config->item('attendance_marking'),'attendance_id'),'emp_id'=>$insert_array['emp_id'],'leave_date'=>$insert_array['leave_date'],'leave_master_id'=>$insert_array['leave_master_id'],'no_leaves'=>$insert_array['no_leaves'],'Created_By'=>$emp_id,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"),'date'=> $this->localdate("Y-m-d"));
		$param['table'] = $this->config->item('attendance_marking');
		$this->Global_model->insert_data($param);
		$this->Global_model->empleaveupdate($insert_array['emp_id'],$insert_array['leave_master_id'],$insert_array['no_leaves']);*/
		
	}
}
