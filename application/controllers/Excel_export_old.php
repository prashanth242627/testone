<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Excel_export extends CI_Controller {
 
 function index()
 {
  $this->load->model("excel_export_model");
  $data["employee_data"] = $this->excel_export_model->fetch_data();
  $this->load->view("excel_export_view", $data);
 }
 function action(){
 	$post_data=$this->input->post('employees_data');
 	if($post_data!=""){
 		$this->load->model("excel_export_model");
		$this->load->library("excel");
		$object = new PHPExcel();
		$object->setActiveSheetIndex(0);
		$table_columns = array("BID","E.Name", "D.O.J", "D.O.B", "Address", "Higher Education","Experience","Mobile No","Designation","Department","Line of Activity","Last Working Day","Transportation","Status");
		$column = 0;

		foreach($table_columns as $field)
		{
		$object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
		$column++;
		}
		$excel_row = 2;
		$i=1;
		foreach($post_data as $row)
		{
			if (isset($row['flag'])) {
				$status="Inactive";
			}
			else{
				$status="Active";
			}
			$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $row["emp_bid"]);
			$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row['emp_name']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row['date_of_join']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row['date_of_birth']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row['address']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row['education']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row['experience']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $row['mobile']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, $row['designation']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row, $row['dept_name']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(10, $excel_row, $row['activity_id']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(11, $excel_row, "last working date");
			$object->getActiveSheet()->setCellValueByColumnAndRow(12, $excel_row, "transportation");
			$object->getActiveSheet()->setCellValueByColumnAndRow(13, $excel_row, $status);
			$excel_row++;
		}

		$object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="Employee Data.xls"');
		$object_writer->save('php://output');
	}
 }
 function action1()
 {
  $this->load->model("excel_export_model");
  $this->load->library("excel");
  $object = new PHPExcel();

  $object->setActiveSheetIndex(0);

  $table_columns = array("BID","E.Name", "D.O.J", "D.O.B", "Address", "Higher Education","Experience","Mobile No","Designation","Department","Line of Activity","Last Working Day","Transportation","Status");

  $column = 0;

  foreach($table_columns as $field)
  {
   $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
   $column++;
  }

  $employee_data = $this->excel_export_model->allEmployees();

  $excel_row = 2;
  $i=1;
		  foreach($employee_data as $row)
		  {
		  	if (isset($row['flag'])) {
				$status="Inactive";
			}
			else{
				$status="Active";
			}
   			$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $row["emp_bid"]);
			$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row['emp_name']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row['date_of_join']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row['date_of_birth']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row['address']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row['education']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row['experience']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $row['mobile']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, $row['designation']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row, $row['dept_name']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(10, $excel_row, $row['activity_id']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(11, $excel_row, "last working date");
			$object->getActiveSheet()->setCellValueByColumnAndRow(12, $excel_row, "transportation");
			$object->getActiveSheet()->setCellValueByColumnAndRow(13, $excel_row, $status);
			$excel_row++;
  		}

  $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
  header('Content-Type: application/vnd.ms-excel');
  header('Content-Disposition: attachment;filename="Employee Data.xls"');
  $object_writer->save('php://output');
 }

 
 
}
?>