<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'controllers/Global_System.php';
class Login extends Global_System {
	
	public function __construct()
	{
		parent::__construct();
	}
	public function index()
	{
		$data=array('current_page'=>'Login','page_title'=>'','parent_menu'=>'');
		$this->form_validation->set_rules('username','username','required');
		$this->form_validation->set_rules('password','password','required');
		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view('Login/login',$data);
		} else {
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			$loginDetails = $this->Common_model->locgincheck($username,$password);
			if(!empty($loginDetails)) {
				$emp_id = $loginDetails->emp_id;
				$role_id = $loginDetails->role_id;
				$this->session->set_userdata('emp_id', $emp_id);
				$this->session->set_userdata('role_id', $role_id);
				redirect('Dashboard');
			} 
			else 
			{
				$this->session->set_userdata(array('create_status'=>'failed'));
				$this->session->set_userdata(array('record_name'=>'Incorrect login credentials'));
				redirect('Login');
			}
		}
	}
	function logouts()
	{
		$this->session->sess_destroy();
        redirect(base_url());
	}
}
