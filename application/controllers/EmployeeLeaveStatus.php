<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'controllers/Global_System.php';
	class EmployeeLeaveStatus extends Global_System {
	public function __construct(){
		parent::__construct(); 
		$this->load->library('session');
		$this->load->helper('url');
		$this->load->model('Employee_leave_model');
	}
	public function index()
	{
		$activities=$this->Employee_leave_model->activities();
		$employees_bid=$this->Employee_leave_model->employeesBid();
		$employees_name=$this->Employee_leave_model->employeesName();
		$data=array('current_page'=>'Employee Leave Status','page_title'=>'Employee Leave Status','parent_menu'=>'','template'=>'emp_leave_status');
		$data['activities']=$activities;
		$data['bids']= $employees_bid;
		$data['names']=$employees_name;
		$this->load->view('theme/template',$data);
	}
	public function All()
	{
		$activities=$this->Employee_leave_model->activities();
		$employees=$this->Employee_leave_model->allEmployees();
		$employees_bid=$this->Employee_leave_model->employeesBid();
		$employees_name=$this->Employee_leave_model->employeesName();
		$data=array('current_page'=>'Employee Leave Status','page_title'=>'Employee Leave Status','parent_menu'=>'','template'=>'emp_leave_status');
		$data['activities']=$activities;
		$data['bids']= $employees_bid;
		$data['names']=$employees_name;
		if ($employees) 
		{
			$data['employees'] = $employees;
		}
		$this->load->view('theme/template',$data);
	}
	public function Activity()
	{
		$activity_type=$this->input->get('type');
		$bid=$this->input->get('bid');
		$ename=$this->input->get('ename');
		$no_of_leaves=array();
		$activities=$this->Employee_leave_model->activities();
		$employees_bid=$this->Employee_leave_model->employeesBid();
		$employees_name=$this->Employee_leave_model->employeesName();
		if ($activity_type!=null) {
			
			$no_of_leaves=$this->Employee_leave_model->lineOfActivityEmployees($activity_type);
		}
		else if ($bid!=null) {
			$no_of_leaves=$this->Employee_leave_model->noOfLeavesByWhere("e.emp_bid=".$bid);
		}
		else if ($ename!=null) {
			$no_of_leaves=$this->Employee_leave_model->noOfLeavesByWhere("e.emp_name='".$ename."'");
		}
		$data=array('current_page'=>'Employee Leave Status','page_title'=>'Employee Leave Status','parent_menu'=>'','template'=>'emp_leave_status');
		$data['activities']= $activities;
		$data['bids']= $employees_bid;
		$data['names']=$employees_name;
		if (count($no_of_leaves)) 
		{
			$data['employees'] = $no_of_leaves;
		}
		$this->load->view('theme/template',$data);
	}
}
