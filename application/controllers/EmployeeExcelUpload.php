
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class EmployeeExcelUpload extends CI_Controller
{
 public function __construct()
 {
  parent::__construct();
  $this->load->model('EmployeeExcelUploadModel');
  $this->load->library('excel');
 }

 function index()
 {
  $this->load->view('EmployeeExcelUploadView');
 }
 
 function fetch()
 {
  $data = $this->EmployeeExcelUploadModel->select();
  $output = '
  <h3 align="center">Total Data - '.$data->num_rows().'</h3>
  <table class="table table-striped table-bordered">
   <tr>
    <th>Customer Name</th>
    <th>Address</th>
    <th>City</th>
    <th>Postal Code</th>
    <th>Country</th>
   </tr>
  ';
  foreach($data->result() as $row)
  {
   $output .= '
   <tr>
    <td>'.$row->CustomerName.'</td>
    <td>'.$row->Address.'</td>
    <td>'.$row->City.'</td>
    <td>'.$row->PostalCode.'</td>
    <td>'.$row->Country.'</td>
   </tr>
   ';
  }
  $output .= '</table>';
  echo $output;
 }

 function employeesimport()
 {
  if(isset($_FILES["file"]["name"]))
  {
   $path = $_FILES["file"]["tmp_name"];
   $object = PHPExcel_IOFactory::load($path);
   foreach($object->getWorksheetIterator() as $worksheet)
   {
    $highestRow = $worksheet->getHighestRow();
    $highestColumn = $worksheet->getHighestColumn();
    for($row=2; $row<=$highestRow; $row++)
    {
     $temp_emp_id = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
     $emp_bid = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
     $emp_name = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
     $date_of_join =date('Y-m-d', PHPExcel_Shared_Date::ExcelToPHP($worksheet->getCellByColumnAndRow(3, $row)->getValue()));
     $date_of_birth = date('Y-m-d', PHPExcel_Shared_Date::ExcelToPHP($worksheet->getCellByColumnAndRow(4, $row)->getValue()));
   //  $sex = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
     $activity_id = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
     $designation = $worksheet->getCellByColumnAndRow(6, $row)->getValue();
     $data[] = array(
      'temp_emp_id'  => $temp_emp_id,
      'emp_bid'   => $emp_bid,
      'emp_name'    => $emp_name,
      'date_of_join'  => $date_of_join,
      'date_of_birth'   => $date_of_birth,
      //'sex'  => $sex,
      'activity_id'   => $activity_id,
      'designation'   => $designation
     );
    }
   }
  // print_r($data);  
  $this->EmployeeExcelUploadModel->insert($data);
   echo 'Data Imported successfully';
  } 
 }
 function bareportsimport()
 {
  if(isset($_FILES["file"]["name"]))
  {
   $path = $_FILES["file"]["tmp_name"];
   $object = PHPExcel_IOFactory::load($path);
   foreach($object->getWorksheetIterator() as $worksheet)
   {
    $highestRow = $worksheet->getHighestRow();
    $highestColumn = $worksheet->getHighestColumn();
    for($row=2; $row<=$highestRow; $row++)
    {
     $emp_bid = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
     $log_date = date('Y-m-d', PHPExcel_Shared_Date::ExcelToPHP($worksheet->getCellByColumnAndRow(2, $row)->getValue()));
     $log_time = PHPExcel_Style_NumberFormat::toFormattedString($worksheet->getCellByColumnAndRow(3, $row)->getValue(), 'hh:mm:ss');
     $data[] = array(
      'emp_bid'   => $emp_bid,
      'log_date'    => $log_date,
      'log_time'  => $log_time,
      'status'   => 1,
      'Created_By'  => 1,
      'Created_Datetime'   => date('Y-m-d h:m:s'),
      'Updated_By'  => 1,
      'Updated_Datetime'   => date('Y-m-d h:m:s'),
     );
    }
   }
   $this->EmployeeExcelUploadModel->insertbiometirc($data);
   echo "success";
  } 
 }
}

?>