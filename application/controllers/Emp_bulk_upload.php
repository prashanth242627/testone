<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'controllers/Global_System.php';
class Emp_bulk_upload extends Global_System {
	
	public function __construct()
	{
		parent::__construct();
		$this->logincheck();
	}
	public function index()
	{
				$file = $_FILES["emp_bluk"]["tmp_name"];
				$this->load->library('excel');
				$objPHPExcel = PHPExcel_IOFactory::load($file);
				$get_sheetData = array();
				$get_sheets = $objPHPExcel->getAllSheets();
				$cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();
				foreach ($cell_collection as $cell) {
					$column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
					$row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
					$data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();
					if ($row == 1) {
						$header[$row][$column] = $data_value;
					} else {
						$arr_data[$row][$column] = $data_value;
						$arr_data1[$column][$row] = $data_value;
					}
				}
				$header = $header[1];
				$values = $arr_data;
				$this->db->trans_begin();
				$excelHeadingNames = array('S NO', 'EMP ID', 'BID', 'EMP NAME', 'REPT TIME FROM', 'RPT TIME TO', 'Sex', 'Date Of Joining', 'Date Of Birth', 'Mobile No', 'Alternate No', 'Emergency No', 'DESIG', 'DEPT', 'SUBJECT', 'EDUCATION', 'Experience', 'HOUSE', 'TRANSPORT', 'Email', 'Address', 'Spouse Name', 'Spouse PhoneNo', 'Spouse Ocupa');
				if(count(array_diff($header,$excelHeadingNames))==0)
				{
					$S_NO = array_search('S_NO', $header);
					$EMP_ID = array_search('EMP ID', $header);
					$BID = array_search('BID', $header);
					$EMP_NAME = array_search('EMP NAME', $header);
					$REPT_TIME_FROM = array_search('REPT TIME FROM', $header);
					$RPT_TIME_TO = array_search('RPT TIME TO', $header);
					$Sex = array_search('Sex', $header);
					$Date_Of_Joining = array_search('Date Of Joining', $header);
					$Date_Of_Birth = array_search('Date Of Birth', $header);
					$Mobile_No = array_search('Mobile No', $header);
					$Alternate_No = array_search('Alternate No', $header);
					$Emergency_No = array_search('Emergency No', $header);
					$DESIG = array_search('DESIG', $header);
					$DEPT = array_search('DEPT', $header);
					$SUBJECT = array_search('SUBJECT', $header);
					$EDUCATION = array_search('EDUCATION', $header);
					$Experience = array_search('Experience', $header);
					$HOUSE = array_search('HOUSE', $header);
					$TRANSPORT = array_search('TRANSPORT', $header);
					$Email = array_search('Email', $header);
					$Address = array_search('Address', $header);
					$Spouse_Name = array_search('Spouse Name', $header);
					$Spouse_PhoneNo = array_search('Spouse PhoneNo', $header);
					$Spouse_Ocupa = array_search('Spouse Ocupa', $header);
					foreach($arr_data1[$EMP_ID] as $key=>$value)
					{
						$S_NO1 =$arr_data1[$S_NO][$key];
						$EMP_ID1 =$arr_data1[$EMP_ID][$key];
						$BID1 =$arr_data1[$BID][$key];
						$EMP_NAME1 =$arr_data1[$EMP_NAME][$key];
						$REPT_TIME_FROM1 =$arr_data1[$REPT_TIME_FROM][$key];
						$RPT_TIME_TO1 =$arr_data1[$RPT_TIME_TO][$key];
						$Sex1 =$arr_data1[$Sex][$key];
						$Date_Of_Joining1 =date('Y-m-d', PHPExcel_Shared_Date::ExcelToPHP($arr_data1[$Date_Of_Joining][$key]));
						$Date_Of_Birth1 = date('Y-m-d', PHPExcel_Shared_Date::ExcelToPHP($arr_data1[$Date_Of_Birth][$key]));
						$Mobile_No1 =$arr_data1[$Mobile_No][$key];
						$Alternate_No1 =$arr_data1[$Alternate_No][$key];
						$Emergency_No1 =$arr_data1[$Emergency_No][$key];
						$DESIG1 =$arr_data1[$DESIG][$key];
						$DEPT1 =$arr_data1[$DEPT][$key];
						$SUBJECT1 =$arr_data1[$SUBJECT][$key];
						$EDUCATION1 =$arr_data1[$EDUCATION][$key];
						$Experience1 =$arr_data1[$Experience][$key];
						$HOUSE1 =$arr_data1[$HOUSE][$key];
						$TRANSPORT1 =$arr_data1[$TRANSPORT][$key];
						$Email1 =$arr_data1[$Email][$key];
						$Address1 =$arr_data1[$Address][$key];
						$Spouse_Name1 =$arr_data1[$Spouse_Name][$key];
						$Spouse_PhoneNo1 =$arr_data1[$Spouse_PhoneNo][$key];
						$Spouse_Ocupa1 =$arr_data1[$Spouse_Ocupa][$key];
						
						
						 $getEmployee = $this->Global_model->get_values_where_single('employee', array('temp_emp_id'=>$EMP_ID1));
						if(COUNT($getEmployee) > 0) {
							$this->session->set_userdata(array('create_status'=>'failed'));
							$this->session->set_userdata(array('record_name'=>'emp_id already exist please remove form excel and upload'));
							redirect('Employee');
						}
						else {
							
						} 
						$getEmployee1 = $this->Global_model->get_values_where_single('employee', array('emp_bid'=>$BID1));
						if(COUNT($getEmployee1) > 0) {
							$this->session->set_userdata(array('create_status'=>'failed'));
							$this->session->set_userdata(array('record_name'=>'emp_bid already exist please remove form excel and upload'));
							redirect('Employee');
						}
						else {
							
						}
						$subjectDetails = $this->Global_model->get_values_where_single('subject_details', array('sub_name'=>$SUBJECT1));
						if($subjectDetails->sub_name == $SUBJECT1) {
							$subject_details_id = $subjectDetails->subject_details_id;
						}
						else {
							$this->session->set_userdata(array('create_status'=>'failed'));
							$this->session->set_userdata(array('record_name'=>'subject name not exist please remove form excel and upload'));
							redirect('Employee');
						}
						$getEmployeeEmail = $this->Global_model->get_values_where_single('employee', array('email'=>$Email1));
						if($getEmployeeEmail->email == $Email1) {
						$this->session->set_userdata(array('create_status'=>'failed'));
						$this->session->set_userdata(array('record_name'=>'email already please remove form excel and upload'));
						redirect('Employee');
						}
						
						$get_house_details	= $this->db->query("SELECT * FROM `transport_details` WHERE `residence_details_id` =1 and transport_name = '".$HOUSE1."'")->row();
						if(count($get_house_details) == 0) {
							$this->session->set_userdata(array('create_status'=>'failed'));
							$this->session->set_userdata(array('record_name'=>'house name not exist please remove form excel and upload'));
							redirect('Employee');
						} else {
							$residence_details_id = $get_house_details->residence_details_id;
						}
						$get_transport_details		= $this->db->query("SELECT * FROM `transport_details` WHERE `residence_details_id` =2 and transport_name = '".$TRANSPORT1."'")->row();
						if(count($get_transport_details) == 0)
						{
							$this->session->set_userdata(array('create_status'=>'failed'));
							$this->session->set_userdata(array('record_name'=>'Transport Details name not exist please remove form excel and upload'));
							redirect('Employee');
						} else  {
							$transport_details_id = $get_transport_details->residence_details_id;
						}
						
						/* $get_reportingslots	= $this->db->query("SELECT * FROM `reportingslots0` WHERE from_time='".$REPT_TIME_FROM1."' AND to_time= '".$RPT_TIME_TO1."'")->row();
						if(count($get_reportingslots) == 0) {
							$this->session->set_userdata(array('create_status'=>'failed'));
							$this->session->set_userdata(array('record_name'=>'Reporting slots Details name not exist please remove form excel and upload'));
							redirect('Employee');
						} else { 
							$reportingslots_id = $get_reportingslots->reportingslots_id;
						} */
						$reportingslots_id =1;
						$get_line_of_activity	= $this->db->query("SELECT * FROM `line_of_activity` WHERE dept_name='".$DEPT1."'")->row();
						if(count($get_line_of_activity) == 0) {
							$this->session->set_userdata(array('create_status'=>'failed'));
							$this->session->set_userdata(array('record_name'=>'Reporting slots Details name not exist please remove form excel and upload'));
							redirect('Employee');
						} else { 
							$activity_id = $get_line_of_activity->activity_id;
						}
						
						$emp_id = $this->Global_model->Max_count($this->config->item('employee'),'emp_id');
						$this->db->query("INSERT INTO `employee`(`emp_id`, `temp_emp_id`, `emp_name`, `education`, `date_of_birth`, `date_of_join`, `emp_bid`, `experience`, `sex`, `mobile`, `email`, `reportingslots_id`, `activity_id`, `subject_details_id`, `residence_details_id`, `transport_details_id`, `alternate_no`, `emergency_no`, `designation`, `address`, `spouse_name`, `spouse_phoneno`, `spouse_occupancy`, `status`, `date`) VALUES(($emp_id+1),'$EMP_ID1','$EMP_NAME1','$EDUCATION1','$Date_Of_Birth1','$Date_Of_Joining1','$BID1',$Experience1,'$Sex1','$Mobile_No1','$Email1',$reportingslots_id,$activity_id,$subject_details_id,$residence_details_id,$transport_details_id,'$Alternate_No1','$Emergency_No1','$DESIG1','$Address1','$Spouse_Name1','$Spouse_PhoneNo1','$Spouse_Ocupa1',1,now())");  
					}
					 if($this->db->trans_status()===FALSE)
					{
						$this->db->trans_rollback();
						$this->session->set_userdata(array('create_status'=>'failed'));
						$this->session->set_userdata(array('record_name'=>'Employee not inserted '));
					}
					else
					{
						$this->db->trans_commit();
						$this->session->set_userdata(array('create_status'=>'success'));
						$this->session->set_userdata(array('record_name'=>'Employee inserted successfully'));
					}
					redirect('Employee'); 
				}
				else {
					$this->session->set_userdata(array('create_status'=>'failed'));
				$this->session->set_userdata(array('record_name'=>'Please check excel heading is mismatching'));
					redirect('Employee');
				}
				
	}
	function payscalbulk()
	{
				$file = $_FILES["emp_bluk"]["tmp_name"];
				$this->load->library('excel');
				$objPHPExcel = PHPExcel_IOFactory::load($file);
				$get_sheetData = array();
				$get_sheets = $objPHPExcel->getAllSheets();
				$cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();
				foreach ($cell_collection as $cell) {
					$column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
					$row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
					$data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();
					if ($row == 1) {
						$header[$row][$column] = $data_value;
					} else {
						$arr_data[$row][$column] = $data_value;
						$arr_data1[$column][$row] = $data_value;
					}
				}
				$header = $header[1];
				$values = $arr_data;
				$excelHeadingNames = array('S.No','E.BID','Ename','Mode of payment','Bank A/c Number','UAN','PF Number','ESI Number','Basic','INCR','D.A','H.R.A','C.C.A','Other alowances','Head/HOD/Co ordinator alowances','Special alowances','Bopnus','Any other alowances','ESI to concider','Is Payment to hold','10% s.d Scope','Standard Instalmemts','PF to consider');
				if(count(array_diff($header,$excelHeadingNames))==0)
				{
					$SNo =array_search('S.No',$header);
					$EBID =array_search('E.BID',$header);
					$Ename =array_search('Ename',$header);
					$Mode_of_payment =array_search('Mode of payment',$header);
					$Bank_Ac_Number =array_search('Bank A/c Number',$header);
					$UAN =array_search('UAN',$header);
					$PF_Number =array_search('PF Number',$header);
					$ESI_Number =array_search('ESI Number',$header);
					$Basic =array_search('Basic',$header);
					$INCR =array_search('INCR',$header);
					$DA =array_search('D.A',$header);
					$HRA =array_search('H.R.A',$header);
					$CCA =array_search('C.C.A',$header);
					$Other_alowances =array_search('Other alowances',$header);
					$HDD_ordinator_alowances =array_search('Head/HOD/Co ordinator alowances',$header);
					$Special_alowances =array_search('Special alowances',$header);
					$Bopnus =array_search('Bopnus',$header);
					$Any_other_alowances =array_search('Any other alowances',$header);
					$ESI_to_concider =array_search('ESI to concider',$header);
					$Is_Payment_to_hold =array_search('Is Payment to hold',$header);
					$sd_Scope =array_search('10% s.d Scope',$header);
					$Standard_Instalmemts =array_search('Standard Instalmemts',$header);
					$PF_to_consider =array_search('PF to consider',$header);
				
					foreach($arr_data1[$EBID] as $key=>$value)
					{
					$SNo1 = $arr_data1[$SNo][$key];
					$EBID1 = $arr_data1[$EBID][$key];
					$Ename1 = $arr_data1[$Ename][$key];
					$Mode_of_payment1 = $arr_data1[$Mode_of_payment][$key];
					$Bank_Ac_Number1 = $arr_data1[$Bank_Ac_Number][$key];
					$UAN1 = $arr_data1[$UAN][$key];
					$PF_Number1 = $arr_data1[$PF_Number][$key];
					$ESI_Number1 = $arr_data1[$ESI_Number][$key];
					$Basic1 = $arr_data1[$Basic][$key];
					$INCR1 = $arr_data1[$INCR][$key];
					$DA1 = $arr_data1[$DA][$key];
					$HRA1 = $arr_data1[$HRA][$key];
					$CCA1 = $arr_data1[$CCA][$key];
					$Other_alowances1 = $arr_data1[$Other_alowances][$key];
					$HDD_ordinator_alowances1 = $arr_data1[$HDD_ordinator_alowances][$key];
					$Special_alowances1 = $arr_data1[$Special_alowances][$key];
					$Bopnus1 = $arr_data1[$Bopnus][$key];
					$Any_other_alowances1 = $arr_data1[$Any_other_alowances][$key];
					$ESI_to_concider1 = $arr_data1[$ESI_to_concider][$key];
					$Is_Payment_to_hold1 = $arr_data1[$Is_Payment_to_hold][$key];
					$sd_Scope1 = $arr_data1[$sd_Scope][$key];
					$Standard_Instalmemts1 = $arr_data1[$Standard_Instalmemts][$key];
					}
					
					echo $SNo1."--".$EBID1."--".$Ename1."--".$Mode_of_payment1."--".$Bank_Ac_Number1."--".$UAN1."--".$PF_Number1."--".$ESI_Number1."--".$Basic1."--".$INCR1."--".$DA1."--".$HRA1."--".$CCA1."--".$Other_alowances1."--".$HDD_ordinator_alowances1."--".$Special_alowances1."--".$Bopnus1."--".$Any_other_alowances1."--".$ESI_to_concider1."--".$Is_Payment_to_hold1."--".$sd_Scope1."--".$Standard_Instalmemts1."--";
				}
				
	}
}
