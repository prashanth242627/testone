<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employee_report extends CI_Controller {
	public function __construct(){
		parent::__construct(); 
		$this->load->library('session');
		$this->load->helper('url');
		$this->load->model('Employee_model');
	}
	public function index()
	{
		$activities=$this->Employee_model->activities();
		$employees_bid=$this->Employee_model->employeesBid();
		$employees_name=$this->Employee_model->employeesName();
		$data['activities']=$activities;
		$data['bids']= $employees_bid;
		$data['names']=$employees_name;
		$data['template']="AvtiveEmployeedetails";
		$this->load->view('theme/template',$data);
	}
	public function All()
	{
		
		$activities=$this->Employee_model->activities();
		$employees=$this->Employee_model->allEmployees();
		$employees_bid=$this->Employee_model->employeesBid();
		$employees_name=$this->Employee_model->employeesName();
		if ($employees) 
		{
			$data['employees'] = $employees;
			$data['activities']= $activities;
			$data['bids']= $employees_bid;
			$data['names']=$employees_name;
    		$this->load->view('AvtiveEmployeedetails', $data);
		}else{
			$this->load->view('AvtiveEmployeedetails');
		}

	}
	public function Activity()
	{
		$activity_type=$this->input->get('type');
		$bid=$this->input->get('bid');
		$ename=$this->input->get('ename');
		$status=$this->input->get('status');
		$start_date=$this->input->get('from');
		$end_date=$this->input->get('to');
		$activities=$this->Employee_model->activities();
		$employees_bid=$this->Employee_model->employeesBid();
		$employees_name=$this->Employee_model->employeesName();
		$employees=array();
		if ($activity_type!=null) {
			
			$employees=$this->Employee_model->lineOfActivityEmployees($activity_type);
		}
		else if ($bid!=null) {
			$employees=$this->Employee_model->employeeData("emp_bid='".$bid."'");
		}
		else if ($ename!=null) {
			$employees=$this->Employee_model->employeeData("emp_name='".$ename."'");
		}
		else if ($status!=null) {
			if ($status=="Active") {
				$employees=$this->Employee_model->activeEmployees();
			}
			else if ($status=="Inactive") {
				$employees=$this->Employee_model->inActiveEmployees();
			}
			else if ($status=="New") {
				$employees=$this->Employee_model->newEmployees();
			}
		}
		else if ($start_date!=null) {
			$employees=$this->Employee_model->getEmployeesData("date_of_join>='".$start_date."' and date_of_join <='".$end_date."'");
		}
		else if ($end_date!="" && $start_date==null) {
			$employees=$this->Employee_model->getEmployeesData("date_of_join <='".$end_date."'");
		}
		$data['activities']= $activities;
		$data['bids']= $employees_bid;
		$data['names']=$employees_name;
		if (count($employees)) 
		{
			$data['employees'] = $employees;
			
    		$this->load->view('AvtiveEmployeedetails', $data);
		}else{
			$this->load->view('AvtiveEmployeedetails',$data);
		}
	}

}
