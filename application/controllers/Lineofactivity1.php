<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'controllers/Global_System.php';
class Lineofactivity extends Global_System {
	
	public function __construct()
	{
		parent::__construct();
		$this->logincheck();
	}
	public function index()
	{
		//$this->output->enable_profiler(TRUE);
		$get_lineofactivity = $this->Global_model->get_values($this->config->item('line_of_activity'));
		$data=array('current_page'=>'Line of Activity','page_title'=>'Line of Activity','parent_menu'=>'','template'=>'masters/lineofactivity/list','get_lineofactivity'=>$get_lineofactivity);
		$this->load->view('theme/template',$data);
		
	}
	function add()
	{
		//$this->output->enable_profiler(TRUE);
		$get_lineofactivity = $this->Global_model->get_values('line_of_activity');
		$data=array('current_page'=>'Line of Activity','page_title'=>'Line of Activity','parent_menu'=>'','template'=>'masters/lineofactivity/add','get_lineofactivity'=>$get_lineofactivity);
		//$this->load->view('theme/template',$data);
		$this->form_validation->set_rules('dept_name','dept_name','required');
		 if($this->form_validation->run() === FALSE)
		{
			$this->load->view('theme/template',$data);
		}
		else
		{	
			$emp_id  = $this->session->userdata('emp_id');
			$dept_name = $this->input->post('dept_name');
			$dept_id = $this->input->post('dept_id');
			$status = ($this->input->post('status'))?1:0;
			$this->db->trans_begin();
			$param['insertdata'] = array('activity_id'=>$this->Global_model->rows_count($this->config->item('line_of_activity')),'dept_name'=>$dept_name,'dept_id'=>$dept_id,'status'=>$status,'Created_By'=>$emp_id,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"));
			$param['table'] = $this->config->item('line_of_activity');
			$this->Global_model->insert_data($param);
			if($this->db->trans_status()===FALSE)
			{
				$this->db->trans_rollback();
				$this->session->set_userdata(array('create_status'=>'failed'));
				$this->session->set_userdata(array('record_name'=>'Line of activity not inserted '));	
			}
			else
			{
				$this->db->trans_commit();
				$this->session->set_userdata(array('create_status'=>'success'));
				$this->session->set_userdata(array('record_name'=>'Line of activity inserted successfully'));
			}
			$this->load->view('theme/template',$data);
		} 
	}
	function edit($id)
	{
		$table = $this->config->item('line_of_activity');
		$where = array('activity_id'=>base64_decode($id));
		$get_lineofactivity = $this->Global_model->get_values_where($table,$where);
		$data=array('current_page'=>'Line of Activity','page_title'=>'Line of Activity','parent_menu'=>'','template'=>'masters/lineofactivity/edit','get_lineofactivity'=>$get_lineofactivity);
		$this->form_validation->set_rules('dept_name','dept_name','required');
		if($this->form_validation->run() === FALSE)
		{
			$this->load->view('theme/template',$data);
		}
		else
		{
			$emp_id  = $this->session->userdata('emp_id');
			$dept_name = $this->input->post('dept_name');
			$dept_id = $this->input->post('dept_id');
			$status = ($this->input->post('status'))?1:0;
			$activity_id = $this->input->post('activity_id');
			$this->db->trans_begin();
			/* Update log table start */
				$insertdata =$this->Global_model->get_values_where_single($this->config->item('line_of_activity'), array('activity_id'=>$activity_id));
				
				$param['insertdata'] = array('activity_id'=>$insertdata->activity_id,'dept_name'=>$insertdata->dept_name,'dept_id'=>$insertdata->dept_id,'status'=>$insertdata->status,'Created_By'=>$emp_id,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"),'Updated_By'=>$emp_id,'Updated_Datetime'=> $this->localdate("Y-m-d H:i:s"),'flag'=>1,'description'=>'EDIT');
				
				$param['table'] = $this->config->item('line_of_activity_log');
			/* Update log table end */
			
			$data1 = array('activity_id'=>$activity_id,'dept_name'=>$dept_name,'dept_id'=>$dept_id,'status'=>$status,'Updated_By'=>$emp_id,'Updated_Datetime'=> $this->localdate("Y-m-d H:i:s"));
			$where = array('activity_id'=>$activity_id);
			$this->Global_model->update($this->config->item('line_of_activity'),$data1,$where);
			
			if($this->db->trans_status()===FALSE)
			{
				$this->db->trans_rollback();
				$this->session->set_userdata(array('create_status'=>'failed'));
				$this->session->set_userdata(array('record_name'=>'Line of activity not updated '));	
			}
			else
			{
				$this->Global_model->insert_data($param);
				$this->db->trans_commit();
				$this->session->set_userdata(array('create_status'=>'success'));
				$this->session->set_userdata(array('record_name'=>'Line of activity updated successfully'));
			}
			redirect('Lineofactivity');
		}
		
	}
	function delete($id)
	{
		$activity_id = base64_decode($id);
		$emp_id  = $this->session->userdata('emp_id');
			$this->db->trans_begin();
			/* Update log table start */
				$insertdata =$this->Global_model->get_values_where_single($this->config->item('line_of_activity'), array('activity_id'=>$activity_id));
				
				$param['insertdata'] = array('activity_id'=>$insertdata->activity_id,'dept_name'=>$insertdata->dept_name,'dept_id'=>$insertdata->dept_id,'status'=>$insertdata->status,'Created_By'=>$emp_id,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"),'Updated_By'=>$emp_id,'Updated_Datetime'=> $this->localdate("Y-m-d H:i:s"),'flag'=>2,'description'=>'DELETE');
				
				$param['table'] = $this->config->item('line_of_activity_log');
			/* Update log table end */
		$this->db->query("DELETE FROM ".$this->config->item('line_of_activity')." WHERE activity_id =".$activity_id);
		if($this->db->trans_status()===FALSE)
		{
			$this->db->trans_rollback();
			$this->session->set_userdata(array('create_status'=>'failed'));
			$this->session->set_userdata(array('record_name'=>'Line of activity not Deleted '));	
		}
		else
		{
			$this->Global_model->insert_data($param);
			$this->db->trans_commit();
			$this->session->set_userdata(array('create_status'=>'success'));
			$this->session->set_userdata(array('record_name'=>'Line of activity Deleted successfully'));
		}
		redirect('Lineofactivity');
		
	}
	function undo($id)
	{
		$emp_id  = $this->session->userdata('emp_id');
		$this->db->trans_begin();
		$activity_id = base64_decode($id);
		$where = array('activity_id'=>base64_decode($id));
		$get_lineofactivity = $this->db->query("SELECT * FROM ".$this->config->item('line_of_activity_log')." WHERE activity_id=".base64_decode($id)." ORDER BY `Created_Datetime` DESC")->first_row();
		$param['insertdata'] = array('activity_id'=>$activity_id,'dept_name'=>$get_lineofactivity->dept_name,'dept_id'=>$get_lineofactivity->dept_id,'status'=>$get_lineofactivity->status,'Created_By'=>$emp_id,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"),'Updated_By'=>$emp_id,'Updated_Datetime'=> $this->localdate("Y-m-d H:i:s"));
		$this->Global_model->update($this->config->item('line_of_activity'),$param['insertdata'],$where);
		if($this->db->trans_status()===FALSE)
		{
			$this->db->trans_rollback();
			$this->session->set_userdata(array('create_status'=>'failed'));
			$this->session->set_userdata(array('record_name'=>'Line of activity not Undo '));	
		}
		else
		{
			$this->db->trans_commit();
			$this->session->set_userdata(array('create_status'=>'success'));
			$this->session->set_userdata(array('record_name'=>'Line of activity undo successfully'));
		}
		redirect('Lineofactivity');
	}
	
}
