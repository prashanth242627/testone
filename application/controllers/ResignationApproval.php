<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'controllers/Global_System.php';
class ResignationApproval extends Global_System {
	
	public function __construct()
	{
		parent::__construct();
		$this->logincheck();
		$this->load->model("Myfunctions");

	}
	public function index()
	{
		$sql_query="select e.temp_emp_id,e.emp_name, e.date_of_join, d.designation_name, l.dept_name,r.* FROM resignations r LEFT JOIN employee e on r.emp_bid=e.emp_bid LEFT JOIN designation d on (e.designation = d.designation_id)
     LEFT JOIN line_of_activity l on (e.activity_id = l.activity_id) order by created_date";
		$get_resignations  = $this->Myfunctions->getQueryDataList($sql_query);

		$data=array('current_page'=>'Resignations','page_title'=>'Resignations','parent_menu'=>'','template'=>'masters/resignations/add','get_resignations'=>$get_resignations);
		$this->form_validation->set_rules('emp_name','emp_name','required');
		if($this->form_validation->run() === FALSE)
		{
			$this->load->view('theme/template',$data);
		}
		else
		{	
			$emp_id  = $this->session->userdata('emp_id');
			$emp_bid = $this->input->post('emp_bid');
			$activity_id = $this->input->post('activity_id');
			$leave_type_id = $this->input->post('leave_type_id');
			$no_leaves = $this->input->post('no_leaves');
			$calendar_year = $this->input->post('calendar_year');
			$lemp_id = $this->input->post('emp_id');
			$this->db->trans_begin();
			$insert_array = array('activity_id'=>$activity_id,'emp_bid'=>$emp_bid, 'leave_type_id'=>$leave_type_id, 'no_leaves'=>$no_leaves,'emp_id'=>$lemp_id,'calendar_year'=>$calendar_year);
			$this->all_actions('insert',$insert_array);
			if($this->db->trans_status()===FALSE)
			{
				$this->db->trans_rollback();
				$this->session->set_userdata(array('create_status'=>'failed'));
				$this->session->set_userdata(array('record_name'=>'Leave Master not inserted '));	
			}
			else
			{
				$this->db->trans_commit();
				$this->session->set_userdata(array('create_status'=>'success'));
				$this->session->set_userdata(array('record_name'=>'Leave Master inserted successfully'));
			}
			redirect('leave_master');
		} 
		
	}
	function edit($id)
	{
		$table = 'resignations';
		$resignation_id = base64_decode($id);

		$single_sql_query="select e.temp_emp_id,e.emp_name, e.date_of_join, d.designation_name, l.dept_name,r.* FROM resignations r LEFT JOIN employee e on r.emp_bid=e.emp_bid LEFT JOIN designation d on (e.designation = d.designation_id)
     LEFT JOIN line_of_activity l on (e.activity_id = l.activity_id) where r.id=$resignation_id order by created_date";
		$getresignationedit  = $this->Myfunctions->getQueryDataList($single_sql_query);
		$sql_query="select e.temp_emp_id,e.emp_name, e.date_of_join, d.designation_name, l.dept_name,r.* FROM resignations r LEFT JOIN employee e on r.emp_bid=e.emp_bid LEFT JOIN designation d on (e.designation = d.designation_id)
     LEFT JOIN line_of_activity l on (e.activity_id = l.activity_id) order by created_date";
		$get_resignations  = $this->Myfunctions->getQueryDataList($sql_query);
		$data=array('current_page'=>'Resignations','page_title'=>'Resignations','parent_menu'=>'','template'=>'masters/resignations/add','get_resignations'=>$get_resignations,'getresignationedit'=>$getresignationedit[0]);
		$this->form_validation->set_rules('emp_bid','emp_bid','required');
		if($this->form_validation->run() === FALSE)
		{
			$this->load->view('theme/template',$data);
		}
		else
		{
			$emp_id  = $this->session->userdata('emp_id');
			$emp_bid = $this->input->post('emp_bid');
			$resignation_status = $this->input->post('resignation_status');
			$hr_comments = $this->input->post('hr_comments');
			$date_of_approve = $this->input->post('date_of_approve');
			$resignation_id = base64_decode($id);
			$this->db->trans_begin();
			$edit_array = array('date_of_approve'=>$date_of_approve,'emp_bid'=>$emp_bid, 'resignation_status'=>$resignation_status, 'hr_comments'=>$hr_comments,'emp_id'=>$emp_id,'resignation_id'=>$resignation_id);
			$this->all_actions('edit',$edit_array);
			if($this->db->trans_status()===FALSE)
			{
				$this->db->trans_rollback();
				$this->session->set_userdata(array('create_status'=>'failed'));
				$this->session->set_userdata(array('record_name'=>'Resignation Approval not updated'));	
			}
			else
			{
				$this->db->trans_commit();
				$this->session->set_userdata(array('create_status'=>'success'));
				$this->session->set_userdata(array('record_name'=>'Resignation Approval updated successfully'));
			}

			redirect('ResignationApproval');
		}
		
	}
	function delete($id)
	{
			$leave_master_id = base64_decode($id);
			$emp_id  = $this->session->userdata('emp_id');
			$this->db->trans_begin();
			$delete_array = array('leave_master_id'=>$leave_master_id);
			$this->all_actions('delete',$delete_array);
		if($this->db->trans_status()===FALSE)
		{
			$this->db->trans_rollback();
			$this->session->set_userdata(array('create_status'=>'failed'));
			$this->session->set_userdata(array('record_name'=>'Leave Master not Deleted '));	
		}
		else
		{
			$this->db->trans_commit();
			$this->session->set_userdata(array('create_status'=>'success'));
			$this->session->set_userdata(array('record_name'=>'Leave Master  Deleted successfully'));
		}
		redirect('leave_master');
		
	}
	function undo($id)
	{
				$emp_id  = $this->session->userdata('emp_id');
				$this->db->trans_begin();
				$leave_master_id = base64_decode($id);
				$undo_array = array('leave_master_id'=>$leave_master_id);
				$this->all_actions('undo',$undo_array);
				
				if($this->db->trans_status()===FALSE)
				{
					$this->db->trans_rollback();
					$this->session->set_userdata(array('create_status'=>'failed'));
					$this->session->set_userdata(array('record_name'=>'Leave Master not Undo '));	
				}
				else
				{
					$this->db->trans_commit();
					$this->session->set_userdata(array('create_status'=>'success'));
					$this->session->set_userdata(array('record_name'=>'Leave Master undo successfully'));
				}
		
		redirect('leave_master');
	}
	function all_actions($action,$insert_array)
	{
		
		$emp_id  = $this->session->userdata('emp_id');
		
		$status = $insert_array['status'];
		if($action == "insert")
		{
			$leave_master_id = $insert_array['leave_master_id'];
			$param['insertdata'] = array('leave_master_id'=>$this->Global_model->Max_count($this->config->item('leave_master'),'leave_master_id'),'emp_bid'=>$insert_array['emp_bid'],'leave_type_id'=>$insert_array['leave_type_id'],'no_leaves'=>$insert_array['no_leaves'],'calendar_year'=>$insert_array['calendar_year'],'Created_By'=>$emp_id,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"),'date'=> $this->localdate("Y-m-d"));
			$param['table'] = $this->config->item('leave_master');
			$this->Global_model->insert_data($param);
		}
		else if($action == "edit")
		{
  		    $resignation_id = $insert_array['resignation_id'];
			$data1 = array('date_of_approve'=>$insert_array['date_of_approve'],'status'=>$insert_array['resignation_status'],'hr_comments'=>$insert_array['hr_comments'],'update_by'=>$emp_id,'updated_date'=> $this->localdate("Y-m-d H:i:s"));
			$where = array('id'=>$resignation_id);
			$this->Global_model->update('resignations',$data1,$where);

		}
		else if($action == 'delete') {
			$leave_master_id = $insert_array['leave_master_id'];
			$this->db->query("INSERT INTO `leave_master_log`(`leave_master_id`, `emp_bid`, `leave_type_id`, `no_leaves`,  `calendar_year`,`date`, `flag`, `description`, `Created_By`, `Created_Datetime`, `Updated_By`, `Updated_Datetime`) (SELECT leave_master_id, emp_bid, leave_type_id, no_leaves,calendar_year, date, '2', 'DELETE', $emp_id, now(), $emp_id, now() FROM leave_master WHERE leave_master_id= ".$leave_master_id.")");
			$this->db->query("DELETE FROM ".$this->config->item('leave_master')." WHERE leave_master_id =".$leave_master_id);
		}
		else if($action == 'undo') {
			$leave_master_id = $insert_array['leave_master_id'];
			$where = array('leave_master_id'=>$leave_master_id);
			$getPaymentTypes = $this->db->query("SELECT * FROM ".$this->config->item('leave_master_log')." WHERE leave_master_id=".$leave_master_id." ORDER BY `Created_Datetime` DESC")->first_row();
			if(!empty($getPaymentTypes)) { 
				$param['insertdata'] = array('emp_bid'=>$getPaymentTypes->emp_bid,'leave_type_id'=>$getPaymentTypes->leave_type_id,'no_leaves'=>$getPaymentTypes->no_leaves,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"),'Updated_By'=>$emp_id,'Updated_Datetime'=> $this->localdate("Y-m-d H:i:s"));
				$this->Global_model->update($this->config->item('leave_master'),$param['insertdata'],$where);
			}
			else {
					$this->db->trans_rollback();
					$this->session->set_userdata(array('create_status'=>'failed'));
					$this->session->set_userdata(array('record_name'=>'Resignation Approval not Undo '));	
					redirect('ResignationApproval');
			}
			
		}
		else {
				$this->session->set_userdata(array('create_status'=>'failed'));
				$this->session->set_userdata(array('record_name'=>'Contact System administrator'));	
				redirect('ResignationApproval');
		}	
	}
	function getEmployeeDetails()
	{
		$emp_bid = $this->input->post('emp_bid');
		$get_leave_masters = $this->db->query("SELECT * FROM `employee` e LEFT JOIN line_of_activity l ON e.`activity_id` = l.activity_id WHERE e.`emp_bid` = '".$emp_bid."'")->row();
		$data['emp_name'] = $get_leave_masters->emp_name;
		$data['dept_name'] = $get_leave_masters->dept_name;
		$data['emp_id'] = $get_leave_masters->emp_id;
		
		echo json_encode($data);
	}
}
