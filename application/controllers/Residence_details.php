<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'controllers/Global_System.php';
class Residence_details extends Global_System {
	
	public function __construct()
	{
		parent::__construct();
		$this->logincheck();
		
	}
	public function index()
	{
		//$this->output->enable_profiler(TRUE);
		$get_residence_details = $this->Global_model->get_values_where($this->config->item('residence_details'),array('status'=>1));
		$get_residence_details_log = $this->Global_model->get_values_where($this->config->item('residence_details_log'), array('flag'=>2));
		$data=array('current_page'=>'Residence Details','page_title'=>'Residence Details','parent_menu'=>'','template'=>'masters/Residence_details/add','get_residence_details'=>$get_residence_details,'get_residence_details_log'=>$get_residence_details_log);
		//$this->load->view('theme/template',$data);
		$this->form_validation->set_rules('residence_name','residence_name','required');
		 if($this->form_validation->run() === FALSE)
		{
			$this->load->view('theme/template',$data);
		}
		else
		{	
			$emp_id  = $this->session->userdata('emp_id');
			$residence_name = $this->input->post('residence_name');
			$residence_id = $this->input->post('residence_id');
			$status = ($this->input->post('status'))?1:0;
			$this->db->trans_begin();
			$insert_array = array('residence_name'=>$residence_name,'residence_id'=>$residence_id, 'status'=>$status);
			$this->all_actions('insert',$insert_array);
			if($this->db->trans_status()===FALSE)
			{
				$this->db->trans_rollback();
				$this->session->set_userdata(array('create_status'=>'failed'));
				$this->session->set_userdata(array('record_name'=>'Residence Details not inserted '));	
			}
			else
			{
				$this->db->trans_commit();
				$this->session->set_userdata(array('create_status'=>'success'));
				$this->session->set_userdata(array('record_name'=>'Residence Details inserted successfully'));
			}
			redirect('Residence_details');
		} 
		
	}
	function edit($id)
	{
		$table = $this->config->item('residence_details');
		$where = array('residence_details_id'=>base64_decode($id));
		$residence_details_id = base64_decode($id);
		$getresidence_detailsEdit = $this->Global_model->get_values_where_single($table,$where);
		$get_residence_details = $this->Global_model->get_values_where($this->config->item('residence_details'),array('status'=>1));
		$get_residence_details_log = $this->Global_model->get_values_where($this->config->item('residence_details_log'), array('flag'=>2));
			$data=array('current_page'=>'Residence Details','page_title'=>'Residence Details','parent_menu'=>'','template'=>'masters/residence_details/add','get_residence_details'=>$get_residence_details,'getresidence_detailsEdit'=>$getresidence_detailsEdit,'get_residence_details_log'=>$get_residence_details_log);
		$this->form_validation->set_rules('residence_name','residence_name','required');
		if($this->form_validation->run() === FALSE)
		{
			$this->load->view('theme/template',$data);
		}
		else
		{
			/* echo "<pre>";
			print_r($this->input->post());
			exit(); */
			$emp_id  = $this->session->userdata('emp_id');
			$residence_name = $this->input->post('residence_name');
			$residence_id = $this->input->post('residence_id');
			$status = ($this->input->post('status'))?1:0;
			$this->db->trans_begin();
			$edit_array = array('residence_name'=>$residence_name,'residence_id'=>$residence_id, 'status'=>$status,'residence_details_id'=>$residence_details_id);
			$this->all_actions('edit',$edit_array);
			if($this->db->trans_status()===FALSE)
			{
				$this->db->trans_rollback();
				$this->session->set_userdata(array('create_status'=>'failed'));
				$this->session->set_userdata(array('record_name'=>'Residence Details not updated'));	
			}
			else
			{
				$this->db->trans_commit();
				$this->session->set_userdata(array('create_status'=>'success'));
				$this->session->set_userdata(array('record_name'=>'Residence Details updated successfully'));
			}
			redirect('Residence_details');
		}
		
	}
	function delete($id)
	{
			$residence_details_id = base64_decode($id);
			$emp_id  = $this->session->userdata('emp_id');
			$this->db->trans_begin();
			$delete_array = array('residence_details_id'=>$residence_details_id);
			$this->all_actions('delete',$delete_array);
		if($this->db->trans_status()===FALSE)
		{
			$this->db->trans_rollback();
			$this->session->set_userdata(array('create_status'=>'failed'));
			$this->session->set_userdata(array('record_name'=>'Residence Details not Deleted '));	
		}
		else
		{
			$this->db->trans_commit();
			$this->session->set_userdata(array('create_status'=>'success'));
			$this->session->set_userdata(array('record_name'=>'Residence Details  Deleted successfully'));
		}
		redirect('Residence_details');
		
	}
	function undo($id)
	{
				$emp_id  = $this->session->userdata('emp_id');
				$this->db->trans_begin();
				$residence_details_id = base64_decode($id);
				$undo_array = array('residence_details_id'=>$residence_details_id);
				$this->all_actions('undo',$undo_array);
				
				if($this->db->trans_status()===FALSE)
				{
					$this->db->trans_rollback();
					$this->session->set_userdata(array('create_status'=>'failed'));
					$this->session->set_userdata(array('record_name'=>'Residence Details not Undo '));	
				}
				else
				{
					$this->db->trans_commit();
					$this->session->set_userdata(array('create_status'=>'success'));
					$this->session->set_userdata(array('record_name'=>'Residence Details undo successfully'));
				}
		
		redirect('Residence_details');
	}
	function all_actions($action,$insert_array)
	{
		
		$emp_id  = $this->session->userdata('emp_id');
		$residence_name = $insert_array['residence_name'];
		
		$status = $insert_array['status'];
		if($action == "insert")
		{
			$residence_id = $insert_array['residence_id'];
			$param['insertdata'] = array('residence_details_id'=>$this->Global_model->Max_count($this->config->item('residence_details'),'residence_details_id'),'residence_name'=>$residence_name,'residence_id'=>$insert_array['residence_id'],'status'=>$status,'Created_By'=>$emp_id,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"),'date'=> $this->localdate("Y-m-d"));
			$param['table'] = $this->config->item('residence_details');
			$this->Global_model->insert_data($param);
		}
		else if($action == "edit")
		{
			$residence_id = $insert_array['residence_id'];
			$residence_details_id = $insert_array['residence_details_id'];
			/* Update log table start */
				$insertdata =$this->Global_model->get_values_where_single($this->config->item('residence_details'), array('residence_details_id'=>$residence_details_id));
				
				$param['insertdata'] = array('residence_details_id'=>$insertdata->residence_details_id,'residence_name'=>$insertdata->residence_name,'residence_id'=>$insertdata->residence_id,'status'=>$insertdata->status,'Created_By'=>$emp_id,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"),'Updated_By'=>$emp_id,'Updated_Datetime'=> $this->localdate("Y-m-d H:i:s"),'date'=> $this->localdate("Y-m-d"),'flag'=>1,'description'=>'EDIT');
				
				$param['table'] = $this->config->item('residence_details_log');
			/* Update log table end */
			$data1 = array('residence_details_id'=>$residence_details_id,'residence_name'=>$residence_name,'residence_id'=>$residence_id,'status'=>$status,'Updated_By'=>$emp_id,'Updated_Datetime'=> $this->localdate("Y-m-d H:i:s"));
			$where = array('residence_details_id'=>$residence_details_id);
			$this->Global_model->update($this->config->item('residence_details'),$data1,$where);
			$this->Global_model->insert_data($param);
		}
		else if($action == 'delete') {
			$residence_details_id = $insert_array['residence_details_id'];
			/* Update log table start */
				$insertdata =$this->Global_model->get_values_where_single($this->config->item('residence_details'), array('residence_details_id'=>$residence_details_id));
				
				$param['insertdata'] = array('residence_details_id'=>$insertdata->residence_details_id,'residence_name'=>$insertdata->residence_name,'residence_id'=>$insertdata->residence_id,'status'=>$insertdata->status,'Created_By'=>$emp_id,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"),'Updated_By'=>$emp_id,'Updated_Datetime'=> $this->localdate("Y-m-d H:i:s"),'date'=> $this->localdate("Y-m-d"),'flag'=>1,'flag'=>2,'description'=>'DELETE');
				$param['table'] = $this->config->item('residence_details_log');
			/* Update log table end */
			$this->db->query("DELETE FROM ".$this->config->item('residence_details')." WHERE residence_details_id =".$residence_details_id);
			$this->Global_model->insert_data($param);
		}
		else if($action == 'undo') {
			$residence_details_id = $insert_array['residence_details_id'];
			$where = array('residence_details_id'=>$residence_details_id);
			$getPaymentTypes = $this->db->query("SELECT * FROM ".$this->config->item('residence_details_log')." WHERE residence_details_id=".$residence_details_id." ORDER BY `Created_Datetime` DESC")->first_row();
			if(!empty($getPaymentTypes)) { 
				$param['insertdata'] = array('residence_details_id'=>$residence_details_id,'residence_name'=>$getPaymentTypes->residence_name,'residence_id'=>$getPaymentTypes->residence_id,'status'=>$getPaymentTypes->status,'Created_By'=>$emp_id,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"),'Updated_By'=>$emp_id,'Updated_Datetime'=> $this->localdate("Y-m-d H:i:s"));
				$this->Global_model->update($this->config->item('residence_details'),$param['insertdata'],$where);
			}
			else {
					$this->db->trans_rollback();
					$this->session->set_userdata(array('create_status'=>'failed'));
					$this->session->set_userdata(array('record_name'=>'Residence Details not Undo '));	
					redirect('Residence_details');
			}
		}
		else {
				$this->session->set_userdata(array('create_status'=>'failed'));
				$this->session->set_userdata(array('record_name'=>'Contact System administrator'));	
				redirect('Residence_details');
		}	
	}
}
