<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'controllers/Global_System.php';
class Leave_types extends Global_System {
	
	public function __construct()
	{
		parent::__construct();
		$this->logincheck();
		
	}
	public function index()
	{
		//$this->output->enable_profiler(TRUE);
		$get_leave_types = $this->Global_model->get_values_where($this->config->item('leave_types'),array('status'=>1));
		$get_leave_types_log = $this->Global_model->get_values_where($this->config->item('leave_types_log'), array('flag'=>2));
		$data=array('current_page'=>'Leave Types','page_title'=>'Leave Types','parent_menu'=>'','template'=>'masters/leave_types/add','get_leave_types'=>$get_leave_types,'get_leave_types_log'=>$get_leave_types_log);
		//$this->load->view('theme/template',$data);
		$this->form_validation->set_rules('leave_type','leave_type','required');
		 if($this->form_validation->run() === FALSE)
		{
			$this->load->view('theme/template',$data);
		}
		else
		{	
			$emp_id  = $this->session->userdata('emp_id');
			$leave_type = $this->input->post('leave_type');
			$status = ($this->input->post('status'))?1:0;
			$this->db->trans_begin();
			$insert_array = array('leave_type'=>$leave_type, 'status'=>$status);
			$this->all_actions('insert',$insert_array);
			if($this->db->trans_status()===FALSE)
			{
				$this->db->trans_rollback();
				$this->session->set_userdata(array('create_status'=>'failed'));
				$this->session->set_userdata(array('record_name'=>'Leave Types not inserted '));	
			}
			else
			{
				$this->db->trans_commit();
				$this->session->set_userdata(array('create_status'=>'success'));
				$this->session->set_userdata(array('record_name'=>'Leave Types inserted successfully'));
			}
			redirect('Leave_types');
		} 
		
	}
	function edit($id)
	{
		$table = $this->config->item('leave_types');
		$where = array('leave_type_id'=>base64_decode($id));
		$leave_type_id = base64_decode($id);
		$getPpaymentTypeEdit = $this->Global_model->get_values_where_single($table,$where);
		$get_leave_types = $this->Global_model->get_values_where($this->config->item('leave_types'),array('status'=>1));
		$get_leave_types_log = $this->Global_model->get_values_where($this->config->item('leave_types_log'), array('flag'=>2));
			$data=array('current_page'=>'Leave Types','page_title'=>'Leave Types','parent_menu'=>'','template'=>'masters/leave_types/add','get_leave_types'=>$get_leave_types,'getPpaymentTypeEdit'=>$getPpaymentTypeEdit,'get_leave_types_log'=>$get_leave_types_log);
		$this->form_validation->set_rules('leave_type','leave_type','required');
		if($this->form_validation->run() === FALSE)
		{
			$this->load->view('theme/template',$data);
		}
		else
		{
			$emp_id  = $this->session->userdata('emp_id');
			$leave_type = $this->input->post('leave_type');
			$status = ($this->input->post('status'))?1:0;
			$this->db->trans_begin();
			$edit_array = array('leave_type'=>$leave_type, 'status'=>$status,'leave_type_id'=>$leave_type_id);
			$this->all_actions('edit',$edit_array);
			if($this->db->trans_status()===FALSE)
			{
				$this->db->trans_rollback();
				$this->session->set_userdata(array('create_status'=>'failed'));
				$this->session->set_userdata(array('record_name'=>'Leave Types not updated'));	
			}
			else
			{
				$this->db->trans_commit();
				$this->session->set_userdata(array('create_status'=>'success'));
				$this->session->set_userdata(array('record_name'=>'Leave Types updated successfully'));
			}
			redirect('Leave_types');
		}
		
	}
	function delete($id)
	{
			$leave_type_id = base64_decode($id);
			$emp_id  = $this->session->userdata('emp_id');
			$this->db->trans_begin();
			$delete_array = array('leave_type_id'=>$leave_type_id);
			$this->all_actions('delete',$delete_array);
		if($this->db->trans_status()===FALSE)
		{
			$this->db->trans_rollback();
			$this->session->set_userdata(array('create_status'=>'failed'));
			$this->session->set_userdata(array('record_name'=>'Leave Types not Deleted '));	
		}
		else
		{
			$this->db->trans_commit();
			$this->session->set_userdata(array('create_status'=>'success'));
			$this->session->set_userdata(array('record_name'=>'Leave Types  Deleted successfully'));
		}
		redirect('Leave_types');
		
	}
	function undo($id)
	{
				$emp_id  = $this->session->userdata('emp_id');
				$this->db->trans_begin();
				$leave_type_id = base64_decode($id);
				$undo_array = array('leave_type_id'=>$leave_type_id);
				$this->all_actions('undo',$undo_array);
				
				if($this->db->trans_status()===FALSE)
				{
					$this->db->trans_rollback();
					$this->session->set_userdata(array('create_status'=>'failed'));
					$this->session->set_userdata(array('record_name'=>'Leave Types not Undo '));	
				}
				else
				{
					$this->db->trans_commit();
					$this->session->set_userdata(array('create_status'=>'success'));
					$this->session->set_userdata(array('record_name'=>'Leave Types undo successfully'));
				}
		
		redirect('Leave_types');
	}
	function all_actions($action,$insert_array)
	{
		
		$emp_id  = $this->session->userdata('emp_id');
		$leave_type = $insert_array['leave_type'];
		$status = $insert_array['status'];
		if($action == "insert")
		{
			$param['insertdata'] = array('leave_type_id'=>$this->Global_model->Max_count($this->config->item('leave_types'),'leave_type_id'),'leave_type'=>$leave_type,'status'=>$status,'Created_By'=>$emp_id,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"),'date'=> $this->localdate("Y-m-d"));
			$param['table'] = $this->config->item('leave_types');
			$this->Global_model->insert_data($param);
		}
		else if($action == "edit")
		{
			
			$leave_type_id = $insert_array['leave_type_id'];
			/* Update log table start */
				$insertdata =$this->Global_model->get_values_where_single($this->config->item('leave_types'), array('leave_type_id'=>$leave_type_id));
				
				$param['insertdata'] = array('leave_type_id'=>$insertdata->leave_type_id,'leave_type'=>$insertdata->leave_type,'status'=>$insertdata->status,'Created_By'=>$emp_id,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"),'Updated_By'=>$emp_id,'Updated_Datetime'=> $this->localdate("Y-m-d H:i:s"),'date'=> $this->localdate("Y-m-d"),'flag'=>1,'description'=>'EDIT');
				$param['table'] = $this->config->item('leave_types_log');
			/* Update log table end */
			$data1 = array('leave_type_id'=>$leave_type_id,'leave_type'=>$leave_type,'status'=>$status,'Updated_By'=>$emp_id,'Updated_Datetime'=> $this->localdate("Y-m-d H:i:s"));
			$where = array('leave_type_id'=>$leave_type_id);
			$this->Global_model->update($this->config->item('leave_types'),$data1,$where);
			$this->Global_model->insert_data($param);
		}
		else if($action == 'delete') {
			$leave_type_id = $insert_array['leave_type_id'];
			/* Update log table start */
				$insertdata =$this->Global_model->get_values_where_single($this->config->item('leave_types'), array('leave_type_id'=>$leave_type_id));
				
				$param['insertdata'] = array('leave_type_id'=>$insertdata->leave_type_id,'leave_type'=>$insertdata->leave_type,'status'=>$insertdata->status,'Created_By'=>$emp_id,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"),'Updated_By'=>$emp_id,'Updated_Datetime'=> $this->localdate("Y-m-d H:i:s"),'date'=> $this->localdate("Y-m-d"),'flag'=>1,'flag'=>2,'description'=>'DELETE');
				$param['table'] = $this->config->item('leave_types_log');
			/* Update log table end */
			$this->db->query("DELETE FROM ".$this->config->item('leave_types')." WHERE leave_type_id =".$leave_type_id);
			$this->Global_model->insert_data($param);
		}
		else if($action == 'undo') {
			$leave_type_id = $insert_array['leave_type_id'];
			$where = array('leave_type_id'=>$leave_type_id);
			$getPaymentTypes = $this->db->query("SELECT * FROM ".$this->config->item('leave_types_log')." WHERE leave_type_id=".$leave_type_id." ORDER BY `Created_Datetime` DESC")->first_row();
			if(!empty($getPaymentTypes)) { 
				$param['insertdata'] = array('leave_type_id'=>$leave_type_id,'leave_type'=>$getPaymentTypes->leave_type,'status'=>$getPaymentTypes->status,'Created_By'=>$emp_id,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"),'Updated_By'=>$emp_id,'Updated_Datetime'=> $this->localdate("Y-m-d H:i:s"));
				$this->Global_model->update($this->config->item('leave_types'),$param['insertdata'],$where);
			}
			else {
					$this->db->trans_rollback();
					$this->session->set_userdata(array('create_status'=>'failed'));
					$this->session->set_userdata(array('record_name'=>'Leave Types not Undo '));	
					redirect('Leave_types');
			}
		}
		else {
				$this->session->set_userdata(array('create_status'=>'failed'));
				$this->session->set_userdata(array('record_name'=>'Contact System administrator'));	
				redirect('Leave_types');
		}	
	}
}
