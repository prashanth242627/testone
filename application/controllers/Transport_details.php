<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'controllers/Global_System.php';
class Transport_details extends Global_System {
	
	public function __construct()
	{
		parent::__construct();
		$this->logincheck();
		
	}
	public function index()
	{
		//$this->output->enable_profiler(TRUE);
		$get_transport_details = $this->Global_model->get_values_where($this->config->item('transport_details'),array('status'=>1));
		$get_transport_details_log = $this->Global_model->get_values_where($this->config->item('transport_details_log'), array('flag'=>2));
		$get_residence_name = $this->Global_model->get_values_where($this->config->item('residence_details'),array('status'=>1));
		$data=array('current_page'=>'Transport Details','page_title'=>'Transport Details','parent_menu'=>'','template'=>'masters/transport_details/add','get_transport_details'=>$get_transport_details,'get_transport_details_log'=>$get_transport_details_log,'get_residence_name'=>$get_residence_name);
		$this->form_validation->set_rules('transport_name','transport_name','required');
		if($this->form_validation->run() === FALSE)
		{
			$this->load->view('theme/template',$data);
		}
		else
		{	
			$emp_id  = $this->session->userdata('emp_id');
			$residence_details_id = $this->input->post('residence_details_id');
			$transport_name = $this->input->post('transport_name');
			$status = ($this->input->post('status'))?1:0;
			$this->db->trans_begin();
			$insert_array = array('residence_details_id'=>$residence_details_id,'transport_name'=>$transport_name, 'status'=>$status);
			$this->all_actions('insert',$insert_array);
			if($this->db->trans_status()===FALSE)
			{
				$this->db->trans_rollback();
				$this->session->set_userdata(array('create_status'=>'failed'));
				$this->session->set_userdata(array('record_name'=>'Transport Details not inserted '));	
			}
			else
			{
				$this->db->trans_commit();
				$this->session->set_userdata(array('create_status'=>'success'));
				$this->session->set_userdata(array('record_name'=>'Transport Details inserted successfully'));
			}
			redirect('Transport_details');
		} 
		
	}
	function edit($id)
	{
		$table = $this->config->item('transport_details');
		$where = array('transport_details_id'=>base64_decode($id));
		$transport_details_id = base64_decode($id);
		$gethoureMasterEdit = $this->Global_model->get_values_where_single($table,$where);
		$get_transport_details = $this->Global_model->get_values_where($this->config->item('transport_details'),array('status'=>1));
		$get_transport_details_log = $this->Global_model->get_values_where($this->config->item('transport_details_log'), array('flag'=>2));
		$get_residence_name = $this->Global_model->get_values_where($this->config->item('residence_details'),array('status'=>1));
			$data=array('current_page'=>'Transport Details','page_title'=>'Transport Details','parent_menu'=>'','template'=>'masters/transport_details/add','get_transport_details'=>$get_transport_details,'gethoureMasterEdit'=>$gethoureMasterEdit,'get_transport_details_log'=>$get_transport_details_log,'get_residence_name'=>$get_residence_name);
		$this->form_validation->set_rules('transport_name','transport_name','required');
		if($this->form_validation->run() === FALSE)
		{
			$this->load->view('theme/template',$data);
		}
		else
		{
			$emp_id  = $this->session->userdata('emp_id');
			$residence_details_id = $this->input->post('residence_details_id');
			$transport_name = $this->input->post('transport_name');
			$status = ($this->input->post('status'))?1:0;
			$this->db->trans_begin();
			$edit_array = array('residence_details_id'=>$residence_details_id,'transport_name'=>$transport_name,'status'=>$status,'transport_details_id'=>$transport_details_id);
			$this->all_actions('edit',$edit_array);
			if($this->db->trans_status()===FALSE)
			{
				$this->db->trans_rollback();
				$this->session->set_userdata(array('create_status'=>'failed'));
				$this->session->set_userdata(array('record_name'=>'Transport Details not updated'));	
			}
			else
			{
				$this->db->trans_commit();
				$this->session->set_userdata(array('create_status'=>'success'));
				$this->session->set_userdata(array('record_name'=>'Transport Details updated successfully'));
			}
			redirect('Transport_details');
		}
		
	}
	function delete($id)
	{
			$transport_details_id = base64_decode($id);
			$emp_id  = $this->session->userdata('emp_id');
			$this->db->trans_begin();
			$delete_array = array('transport_details_id'=>$transport_details_id);
			$this->all_actions('delete',$delete_array);
		if($this->db->trans_status()===FALSE)
		{
			$this->db->trans_rollback();
			$this->session->set_userdata(array('create_status'=>'failed'));
			$this->session->set_userdata(array('record_name'=>'Transport Details not Deleted '));	
		}
		else
		{
			$this->db->trans_commit();
			$this->session->set_userdata(array('create_status'=>'success'));
			$this->session->set_userdata(array('record_name'=>'Transport Details  Deleted successfully'));
		}
		redirect('Transport_details');
		
	}
	function undo($id)
	{
				$emp_id  = $this->session->userdata('emp_id');
				$this->db->trans_begin();
				$transport_details_id = base64_decode($id);
				$undo_array = array('transport_details_id'=>$transport_details_id);
				$this->all_actions('undo',$undo_array);
				
				if($this->db->trans_status()===FALSE)
				{
					$this->db->trans_rollback();
					$this->session->set_userdata(array('create_status'=>'failed'));
					$this->session->set_userdata(array('record_name'=>'Transport Details not Undo '));	
				}
				else
				{
					$this->db->trans_commit();
					$this->session->set_userdata(array('create_status'=>'success'));
					$this->session->set_userdata(array('record_name'=>'Transport Details undo successfully'));
				}
		
		redirect('Transport_details');
	}
	function all_actions($action,$insert_array)
	{
		
		$emp_id  = $this->session->userdata('emp_id');
		$transport_details_id = $insert_array['transport_details_id'];
		$status = $insert_array['status'];
		if($action == "insert")
		{
			$residence_details_id = $insert_array['residence_details_id'];
			$param['insertdata'] = array('transport_details_id'=>$this->Global_model->Max_count($this->config->item('transport_details'),'transport_details_id'),'residence_details_id'=>$residence_details_id,'transport_name'=>$insert_array['transport_name'], 'status'=>1,'Created_By'=>$emp_id,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"),'date'=> $this->localdate("Y-m-d"));
			$param['table'] = $this->config->item('transport_details');
			$this->Global_model->insert_data($param);
		}
		else if($action == "edit")
		{
			
			$transport_details_id = $insert_array['transport_details_id'];
			/* Update log table start */
				$insertdata =$this->Global_model->get_values_where_single($this->config->item('transport_details'), array('transport_details_id'=>$transport_details_id));
				
				$param['insertdata'] = array('transport_details_id'=>$insertdata->transport_details_id,'residence_details_id'=>$insertdata->residence_details_id,'transport_name'=>$insertdata->transport_name,  'status'=>$insertdata->status,'Created_By'=>$emp_id,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"),'Updated_By'=>$emp_id,'Updated_Datetime'=> $this->localdate("Y-m-d H:i:s"),'date'=> $this->localdate("Y-m-d"),'flag'=>1,'description'=>'EDIT');
				$param['table'] = $this->config->item('transport_details_log');
			/* Update log table end */
			$data1 = array('transport_details_id'=>$transport_details_id,'residence_details_id'=>$insert_array['residence_details_id'],'transport_name'=>$insert_array['transport_name'], 'status'=>$status,'Updated_By'=>$emp_id,'Updated_Datetime'=> $this->localdate("Y-m-d H:i:s"));
			$where = array('transport_details_id'=>$transport_details_id);
			$this->Global_model->update($this->config->item('transport_details'),$data1,$where);
			$this->Global_model->insert_data($param);
		}
		else if($action == 'delete') {
			$transport_details_id = $insert_array['transport_details_id'];
			/* Update log table start */
				$insertdata =$this->Global_model->get_values_where_single($this->config->item('transport_details'), array('transport_details_id'=>$transport_details_id));
				
				$param['insertdata'] = array('transport_details_id'=>$insertdata->transport_details_id,'residence_details_id'=>$insertdata->residence_details_id,'transport_name'=>$insertdata->transport_name,  'status'=>$insertdata->status,'Created_By'=>$emp_id,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"),'Updated_By'=>$emp_id,'Updated_Datetime'=> $this->localdate("Y-m-d H:i:s"),'date'=> $this->localdate("Y-m-d"),'flag'=>1,'flag'=>2,'description'=>'DELETE');
				$param['table'] = $this->config->item('transport_details_log');
			/* Update log table end */
			$this->db->query("DELETE FROM ".$this->config->item('transport_details')." WHERE transport_details_id =".$transport_details_id);
			$this->Global_model->insert_data($param);
		}
		else if($action == 'undo') {
			$transport_details_id = $insert_array['transport_details_id'];
			$where = array('transport_details_id'=>$transport_details_id);
			$get_transport_details = $this->db->query("SELECT * FROM ".$this->config->item('transport_details_log')." WHERE transport_details_id=".$transport_details_id." ORDER BY `Created_Datetime` DESC")->first_row();
			if(!empty($get_transport_details)) { 
				$param['insertdata'] = array('transport_details_id'=>$transport_details_id,'transport_name'=>$get_transport_details->transport_name,'residence_details_id'=>$get_transport_details->residence_details_id,'status'=>$get_transport_details->status,'Created_By'=>$emp_id,'Created_Datetime'=> $this->localdate("Y-m-d H:i:s"),'Updated_By'=>$emp_id,'Updated_Datetime'=> $this->localdate("Y-m-d H:i:s"));
				$this->Global_model->update($this->config->item('transport_details'),$param['insertdata'],$where);
			}
			else {
					$this->db->trans_rollback();
					$this->session->set_userdata(array('create_status'=>'failed'));
					$this->session->set_userdata(array('record_name'=>'Transport Details not Undo '));	
					redirect('Transport_details');
			}
		}
		else {
				$this->session->set_userdata(array('create_status'=>'failed'));
				$this->session->set_userdata(array('record_name'=>'Contact System administrator'));	
				redirect('Transport_details');
		}	
	}
	function getEmployeeName()
	{
		$residence_details_id = $this->input->post('residence_details_id');
		$get_transport_detailss = $this->Global_model->get_values_where_single($this->config->item('residence_details'),array('residence_details_id'=>$residence_details_id));
		echo $get_transport_detailss->emp_name;
	}
}
