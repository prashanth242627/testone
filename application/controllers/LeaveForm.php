<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'controllers/Global_System.php';
class LeaveForm extends Global_System {
	public function __construct(){
		parent::__construct(); 
		$this->logincheck();
		$this->load->library('session');
		$this->load->helper('url');
		$this->load->model('Employee_leave_model');
		$this->load->model('Myfunctions');
	}
	public function index()
	{
		$employees_bid=$this->Employee_leave_model->employeesBid();
		$employees_name=$this->Employee_leave_model->employeesName();
		$get_leave_types=$this->Myfunctions->getQueryDataList("SELECT * FROM leave_types WHERE leave_type NOT in ('LOP','H.LOP','OD')");
		$data=array('current_page'=>'LeaveForm','page_title'=>'LeaveForm','parent_menu'=>'','template'=>'masters/leave_form/add');
		$data['get_employees']= $employees_bid;
		$data['leave_types']=$get_leave_types;
		$data['names']=$employees_name;
		$this->load->view('theme/template',$data);
	}
	public function Activity()
	{
		$bid=$this->input->get('bid');
		$ename=$this->input->get('ename');
		$month=$this->input->get('leavedate');
		$year=$this->input->get('year');
		$data['employees']=array();
		$get_leave_types=$this->Myfunctions->getQueryDataList("SELECT * FROM leave_types WHERE leave_type NOT in ('LOP','H.LOP','OD')");
		$employees_bid=$this->Employee_leave_model->employeesBid();
		$employees_name=$this->Employee_leave_model->employeesName();
		
		
		$data=array('current_page'=>'LeaveForm','page_title'=>'LeaveForm','parent_menu'=>'','template'=>'masters/leave_form/add');
		if (count($no_of_leaves)) 
		{
			$data['employee'] = $no_of_leaves[0];
		}
		$data['activities']=$activities;
		$data['get_employees']= $employees_bid;
		$data['leave_types']=$get_leave_types;
		$data['names']=$employees_name;
		$this->load->view('theme/template',$data);
	}
	function CheckAttendaceMarking()
	{
		$emp_id  = $this->session->userdata('emp_id');
		$emp_bid = $this->input->post('emp_bid');
		$emp_name= $this->input->post('emp_name');
		$leave_date = $this->input->post('leave_date');
		$from_date=$this->input->post('leave_date_from');
		$to_date=$this->input->post('leave_date_to');
		$leave_date_from = strtotime($this->input->post('leave_date_from'));
		$leave_date_to = strtotime($this->input->post('leave_date_to'));
		$leave_type_id=$this->input->post('leave_type_id');
		$this->db->trans_begin();
		
		//echo $this->all_actions('insert',$insert_array);
		//$diff = $leave_date_to->diff($leave_date_from)->format("%a");
		$days_between = ceil(($leave_date_to - $leave_date_from) / 86400);
		
		if ($days_between>=0) {
			$insert_array = array('emp_bid'=>$emp_bid, 'leave_date_from'=>$from_date,'leave_date_to'=>$to_date, 'leave_type_id'=>$leave_type_id,'no_leaves'=>($days_between+1),'emp_id'=>$emp_id,"emp_name"=>$emp_name);
			echo $this->all_actions('insert',$insert_array);
		}
		else{
			echo json_encode(array("msg"=>"Selected Dates are Invalied","status"=>false));
		}
	}
	function all_actions($action,$insert_array)
	{
		$emp_id  = $this->session->userdata('emp_id');
		$emp_bid=$insert_array['emp_bid'];
		$leave_date_from=$insert_array['leave_date_from'];
		$leave_date_to=$insert_array['leave_date_to'];
		$leave_type_id=$insert_array['leave_type_id'];
		$no_leaves=$insert_array['no_leaves'];
		$emp_name=$insert_array['emp_name'];
		if($action == "insert")
		{
			$emp_holiday_group_id=0;
			$get_employee_data = $this->Global_model->get_values_where_single($this->config->item('employee'),array('status'=>1,"emp_bid"=>$emp_bid));
			$emp_holiday_group_id=$get_employee_data->holiday_group_id;
			$get_cl_type = $this->Global_model->get_values_where_single($this->config->item('leave_types'),array('status'=>1,"leave_type"=>"CL"));
			$get_h_cl_type = $this->Global_model->get_values_where_single($this->config->item('leave_types'),array('status'=>1,"leave_type"=>"H.CL"));
			$get_el_type = $this->Global_model->get_values_where_single($this->config->item('leave_types'),array('status'=>1,"leave_type"=>"EL"));
			$get_h_el_type = $this->Global_model->get_values_where_single($this->config->item('leave_types'),array('status'=>1,"leave_type"=>"H.EL"));
			$get_sl_type = $this->Global_model->get_values_where_single($this->config->item('leave_types'),array('status'=>1,"leave_type"=>"SL"));
			$get_h_sl_type = $this->Global_model->get_values_where_single($this->config->item('leave_types'),array('status'=>1,"leave_type"=>"H.SL"));
			$get_leave_date_data=$this->Global_model->get_values_where_single($this->config->item('attendance_marking'),array('emp_bid'=>$emp_bid,"leave_date"=>$insert_array['leave_date_from']));
			$get_holiday_data=$this->db->query("SELECT * FROM holidays WHERE from_date<='".$insert_array['leave_date_from']."' and to_date >= '".$insert_array['leave_date_from']."' and holiday_group_id=".$emp_holiday_group_id)->row();

			$getparam="emp_bid=$emp_bid&emp_name=$emp_name&leave_date_from=$leave_date_from&leave_date_to=$leave_date_to&leave_type_id=$leave_type_id&no_leaves=$no_leaves";
			$day_name=date('l',strtotime($insert_array['leave_date_from']));
			if ($day_name=="Sunday") {
				return json_encode(array("status"=>false,"msg"=>"Selected Date is Sunday"));
			}
			else if (!empty($get_holiday_data)) {
				return json_encode(array("status"=>false,"msg"=>"Selected Date is Holiday($get_holiday_data->holiday_name)"));
			}
			else if (!empty($get_leave_date_data)) {
				$get_type_data=$this->Global_model->get_values_where_single($this->config->item('leave_types'),array('leave_type_id'=>$get_leave_date_data->leave_type_id));
				return json_encode(array("status"=>false,"msg"=>"Already $get_type_data->leave_type applied for this date"));
			}
			else if ($get_cl_type->leave_type_id==$insert_array['leave_type_id'] || $get_h_cl_type->leave_type_id==$insert_array['leave_type_id']) {
				$cls_data=$this->db->query("SELECT lm.used,lm.leave_type_id,lm.no_leaves FROM leave_master `lm` LEFT JOIN leave_types lt ON lt.leave_type_id = lm.leave_type_id WHERE lt.leave_type='CL' and lm.`emp_bid` = '".$emp_bid."'")->row();
				if (!empty($cls_data) && ($cls_data->no_leaves-$cls_data->used)>0 && (($cls_data->no_leaves-$cls_data->used)>=$insert_array['no_leaves'])) {
            		return json_encode(array("status"=>true,"msg"=>"Success","getparam"=>$getparam));
				}
				else{
					return json_encode(array("status"=>false,"msg"=>"Employee don't Have Enough CLs Data"));
				}
			}
			else if ($get_el_type->leave_type_id==$insert_array['leave_type_id'] || $get_h_el_type->leave_type_id==$insert_array['leave_type_id']) {

				$els_data=$this->db->query("SELECT lm.used,lm.leave_type_id,lm.no_leaves FROM leave_master `lm` LEFT JOIN leave_types lt ON lt.leave_type_id = lm.leave_type_id WHERE lt.leave_type='EL' and lm.`emp_bid` = '".$emp_bid."'")->row();
				if (!empty($els_data) && ($els_data->no_leaves-$els_data->used)>0 && (($els_data->no_leaves-$els_data->used)>=$insert_array['no_leaves'])) {
            		return json_encode(array("status"=>true,"msg"=>"Success","getparam"=>$getparam));
				}
				else{
					return json_encode(array("status"=>false,"msg"=>"Employee don't Have Enough ELs Data"));
				}
			}
			else if ($get_sl_type->leave_type_id==$insert_array['leave_type_id'] || $get_h_sl_type->leave_type_id==$insert_array['leave_type_id']) {

				$sls_data=$this->db->query("SELECT lm.used,lm.leave_type_id,lm.no_leaves FROM leave_master `lm` LEFT JOIN leave_types lt ON lt.leave_type_id = lm.leave_type_id WHERE lt.leave_type='SL' and lm.`emp_bid` = '".$emp_bid."'")->row();
				if (!empty($sls_data) && ($sls_data->no_leaves-$sls_data->used)>0 && (($sls_data->no_leaves-$sls_data->used)>=$insert_array['no_leaves'])) {
					
            		return json_encode(array("status"=>true,"msg"=>"Success","getparam"=>$getparam));
				}
				else{
					return json_encode(array("status"=>false,"msg"=>"Employee don't Have Enough SLs Data"));
				}
			}
			else {
				return json_encode(array("status"=>false,"msg"=>"Invalid Leave type","insert_array"=>$insert_array));
			}
			//$this->Global_model->insert_data($param);
			/*$this->Global_model->empleaveupdate($insert_array['emp_id'],$insert_array['leave_master_id'],$insert_array['no_leaves']);*/
		}
		else {
				$this->session->set_userdata(array('create_status'=>'failed'));
				$this->session->set_userdata(array('record_name'=>'Contact System administrator'));	
				redirect('LeaveForm');
		}	
	}
	function getEmployeeDetails()
	{
		$emp_bid = $this->input->post('emp_bid');
		$emp_name = $this->input->post('emp_name');
		if ($emp_bid!=null) {
			$get_attendance_markings = $this->db->query("SELECT * FROM `employee` e LEFT JOIN line_of_activity l ON e.`activity_id` = l.activity_id LEFT JOIN subject_details s ON e.subject_details_id = s.subject_details_id WHERE e.`emp_bid` = '".$emp_bid."'")->row();
			$get_cl_data=$this->db->query("SELECT lm.used,lm.leave_type_id,lm.no_leaves FROM leave_master `lm` LEFT JOIN leave_types lt ON lt.leave_type_id = lm.leave_type_id WHERE lt.leave_type='CL' and lm.`emp_bid` = '".$emp_bid."'")->row();
			$get_el_data=$this->db->query("SELECT lm.used,lm.leave_type_id,lm.no_leaves FROM leave_master `lm` LEFT JOIN leave_types lt ON lt.leave_type_id = lm.leave_type_id WHERE lt.leave_type='EL' and lm.`emp_bid` = '".$emp_bid."'")->row();
			$get_sl_data=$this->db->query("SELECT lm.used,lm.leave_type_id,lm.no_leaves FROM leave_master `lm` LEFT JOIN leave_types lt ON lt.leave_type_id = lm.leave_type_id WHERE lt.leave_type='SL' and lm.`emp_bid` = '".$emp_bid."'")->row();
		}
		else if ($emp_name!=null) {
			$get_attendance_markings = $this->db->query("SELECT * FROM `employee` e LEFT JOIN line_of_activity l ON e.`activity_id` = l.activity_id LEFT JOIN subject_details s ON e.subject_details_id = s.subject_details_id  WHERE e.`emp_name` = '".$emp_name."'")->row();
			$get_cl_data=$this->db->query("SELECT lm.used,lm.leave_type_id,lm.no_leaves FROM leave_master `lm` LEFT JOIN leave_types lt ON lt.leave_type_id = lm.leave_type_id LEFT JOIN employee e on lm.`emp_bid` =e.emp_bid  WHERE lt.leave_type='CL' and e.emp_name= '".$emp_name."'")->row();
			$get_el_data=$this->db->query("SELECT lm.used,lm.leave_type_id,lm.no_leaves FROM leave_master `lm` LEFT JOIN leave_types lt ON lt.leave_type_id = lm.leave_type_id LEFT JOIN employee e on lm.`emp_bid` =e.emp_bid WHERE lt.leave_type='EL' and e.emp_name= '".$emp_name."'")->row();
			$get_sl_data=$this->db->query("SELECT lm.used,lm.leave_type_id,lm.no_leaves FROM leave_master `lm` LEFT JOIN leave_types lt ON lt.leave_type_id = lm.leave_type_id LEFT JOIN employee e on lm.`emp_bid` =e.emp_bid WHERE lt.leave_type='SL' and e.emp_name= '".$emp_name."'")->row();
		}
		
		
		$data['emp_bid'] = $get_attendance_markings->emp_bid;
		$data['emp_name'] = $get_attendance_markings->emp_name;
		$data['dept_name'] = $get_attendance_markings->dept_name;
		$data['sub_name'] = $get_attendance_markings->sub_name;
		$data['emp_id'] = $get_attendance_markings->emp_id;
		if (!empty($get_cl_data)) {
			$data['cl']['opb']=$get_cl_data->no_leaves+$get_cl_data->used;
			$data['cl']['used']=$get_cl_data->used;
			$data['cl']['clb']=$get_cl_data->no_leaves;
			
		}
		else{
			$data['cl']['opb']=0;
			$data['cl']['used']=0;
			$data['cl']['clb']=0;
		}
		if (!empty($get_sl_data)) {
			$data['sl']['opb']=$get_sl_data->no_leaves+$get_sl_data->used;
			$data['sl']['used']=$get_sl_data->used;
			$data['sl']['clb']=$get_sl_data->no_leaves;
			
		}
		else{
			$data['sl']['opb']=0;
			$data['sl']['used']=0;
			$data['sl']['clb']=0;
		}
		if (!empty($get_el_data)) {
			$data['el']['opb']=$get_el_data->no_leaves+$get_el_data->used;
			$data['el']['used']=$get_el_data->used;
			$data['el']['clb']=$get_el_data->no_leaves;
			
		}
		else{
			$data['el']['opb']=0;
			$data['el']['used']=0;
			$data['el']['clb']=0;
		}
		$year=date("Y");
		$get_emp_leavemaster=$this->Myfunctions->getQueryDataList("SELECT * FROM leave_types WHERE leave_type NOT in ('LOP','H.LOP','OD')");
		$get_emp_leavemaster_data = "";
		foreach($get_emp_leavemaster as $row)
		{
			$no_leaves=$row['no_leaves'];
			$used_count=$row['used_count'];
			$remaining=$no_leaves-$used_count;
			$get_emp_leavemaster_data.= "<option value='".$row['leave_type_id']."'>".$row['leave_type']."</option>";
			//$get_emp_leavemaster_data .= "<option value='".$rowleave_type_id."'>".$rowleave_type."(".$no_leaves."/".$used_count."/".$remaining.")</option>";
		}
		$data['get_emp_leavemaster'] = $get_emp_leavemaster_data;
		
		$get_employees_leaves_count_types=$this->Global_model->get_employees_leaves_count_type($emp_bid,$year);
		$employee_leave_types_id="<th>Leave Type</th>";
		$total_leaves_id="<th>Total Leaves</th>";
		$remaining_leaves_id="<th>Used Leaves</th>";
		foreach ($get_employees_leaves_count_types as $get_employees_leaves_count_type) {
			$employee_leave_types_id.="<th>".$get_employees_leaves_count_type->leave_type."</th>";
			$total_leaves_id.="<th>".$get_employees_leaves_count_type->no_leaves."</th>";
			$remaining_leaves_id.="<th>".$get_employees_leaves_count_type->used_count."</th>";
		}
		$data['employee_leave_types_id'] = $employee_leave_types_id;
		$data['total_leaves_id'] = $total_leaves_id;
		$data['remaining_leaves_id'] = $remaining_leaves_id;
		echo json_encode($data);
	}
	function LeaveFormDownload()
	{
		$emp_id  = $this->session->userdata('emp_id');
		$emp_bid = $this->input->post('emp_bid');
		$emp_name=$this->input->post('emp_name');
		$leave_date = $this->input->post('leave_date');
		$leave_type_id=$this->input->post('leave_type_id');
		$no_leaves = $this->input->post('no_leaves');
		$lemp_id = $this->input->post('emp_id');
		$this->db->trans_begin();
		$insert_array = array('emp_bid'=>$emp_bid, 'leave_date'=>$leave_date, 'leave_type_id'=>$leave_type_id,'no_leaves'=>1,'emp_id'=>$lemp_id);
		
		 $this->all_actions('insert',$insert_array);
		if($this->db->trans_status()===FALSE)
		{
			$this->db->trans_rollback();
			$this->session->set_userdata(array('create_status'=>'failed'));
			$this->session->set_userdata(array('record_name'=>'Attendance Marking not inserted '));	
		}
		else
		{
			$this->db->trans_commit();
			$this->session->set_userdata(array('create_status'=>'success'));
			$this->session->set_userdata(array('record_name'=>'Attendance Marking inserted successfully'));
		}
	}

}
