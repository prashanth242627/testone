<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'controllers/Global_System.php';
class EmpPFContribution extends Global_System {
	public function __construct(){
		parent::__construct(); 
		$this->logincheck();
		$this->load->library('session');
		$this->load->helper('url');
		$this->load->model('Employee_model');
		$this->load->model('Employee_pf_model');
		$this->load->model('Salary_model');
	}
	public function index()
	{
		$activities=$this->Employee_model->activities();
		$employees_bid=$this->Employee_model->employeesBid();
		$employees_name=$this->Employee_model->employeesName();
		$payment_modes=$this->Employee_pf_model->paymentModes();
		$data=array('current_page'=>'Employee PF Details','page_title'=>'Employee PF Details','parent_menu'=>'','template'=>'EmployeePFDetails');
		$data['activities']= $activities;
		$data['bids']= $employees_bid;
		$data['names']=$employees_name;
		$data['payment_modes']=$payment_modes;
		$employees=array();
		$this->load->view('theme/template',$data);
	}
	public function All()
	{

		$month=$this->input->get('month');
		$year=$this->input->get('year');
		$activities=$this->Employee_model->activities();
		$employees_bid=$this->Employee_model->employeesBid();
		$employees_name=$this->Employee_model->employeesName();
		$payment_modes=$this->Employee_pf_model->paymentModes();
		if ($year!=null && $month!=null) {

			$employees=$this->Employee_pf_model->AllEmployeesPFData($year,$month);
		}
		$data=array('current_page'=>'Employee PF Details','page_title'=>'Employee PF Details','parent_menu'=>'','template'=>'EmployeePFDetails');
		$data['activities']= $activities;
		$data['bids']= $employees_bid;
		$data['names']=$employees_name;
		$data['payment_modes']=$payment_modes;
		if ($employees) 
		{
			$data['employees'] = $employees;
		}
		$this->load->view('theme/template',$data);
	}
	public function Activity()
	{
		$activity_type=$this->input->get('type');
		$bid=$this->input->get('bid');
		$ename=$this->input->get('ename');
		$month=$this->input->get('month');
		$year=$this->input->get('year');
		$activities=$this->Employee_model->activities();
		$employees_bid=$this->Employee_model->employeesBid();
		$employees_name=$this->Employee_model->employeesName();
		$payment_modes=$this->Employee_pf_model->paymentModes();
		$employees=array();
		if ($year!=null && $month!=null) {
			if ($activity_type!=null) {
				if ($activity_type=="All") {
					$employees=$this->Employee_pf_model->AllEmployeesPFData($year,$month);
				}
				else{
					$employees=$this->Employee_pf_model->EmployeePFDataWhere("la.dept_name='".$activity_type."'",$year,$month);
				}
				
			}
			else if ($ename!=null) {
				if ($ename=="All") {
					$employees=$this->Employee_pf_model->AllEmployeesPFData($year,$month);
				}
				else{
					$employees=$this->Employee_pf_model->EmployeePFDataWhere("e.emp_name='".$ename."'",$year,$month);
				}
				
			}
			else if ($bid!=null) {
				if ($bid=="All") {
					$employees=$this->Employee_pf_model->AllEmployeesPFData($year,$month);
				}
				else{
					$employees=$this->Employee_pf_model->EmployeePFDataWhere("e.emp_bid=".$bid,$year,$month);
				}
			}
		}
		$data=array('current_page'=>'Employee PF Details','page_title'=>'Employee PF Details','parent_menu'=>'','template'=>'EmployeePFDetails');
		$data['activities']= $activities;
		$data['bids']= $employees_bid;
		$data['names']=$employees_name;
		$data['payment_modes']=$payment_modes;
		if (count($employees)) 
		{
			$data['employees'] = $employees;
		}
		$this->load->view('theme/template',$data);
	}

}
