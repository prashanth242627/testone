<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'controllers/Global_System.php';

class HolidaysList extends Global_System
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Holidays_model');
		$this->logincheck();
	}
	public function index()
	{
		
		//$get_holidays_list = $this->Holidays_model->get_holidays_list($this->config->item('holidays'),array('status'=>1));
			$data=array('current_page'=>'holidays_list','page_title'=>'Holidays List','parent_menu'=>'','template'=>'holidays_calander');
			$this->load->view('theme/template',$data);
	}
	public function Activity()
	{
		$month=$this->input->get('month');
		$year=$this->input->get('year');
		$holidays=array();
	 	
		if ($year!=null && ($month==null || $month=="")) {
			$holidays=$this->Holidays_model->get_holidays_list($this->config->item('holidays'),array('YEAR(from_date)'=>$year));
		}
		else if ($year!=null && $month!=null) {
			$holidays=$this->Holidays_model->get_holidays_list($this->config->item('holidays'),array('YEAR(from_date)'=>$year,'MONTHNAME(from_date)'=>$month));
		}
		$data=array('current_page'=>'holidays_list','page_title'=>'Holidays List','parent_menu'=>'','template'=>'holidays_calander');
		if (count($holidays)) 
		{
			$data['get_holidays_list'] = $holidays;
		}

			$this->load->view('theme/template',$data);

	}
	public function yearBasedList()
	{
		$year=$this->input->post('year');
		$year_based_list=$this->Holidays_model->year_based_list($year);
		print_r($year_based_list);
	}
}
?>