<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'controllers/Global_System.php';
class Dashboard extends Global_System {
	
	public function __construct()
	{
		parent::__construct();
		$this->logincheck();
		$this->load->model("Dashboard_model");
	}
	public function index()
	{
		$data=array('current_page'=>'Dashboard','page_title'=>'Dashboard','parent_menu'=>'','template'=>'main');
		$data['employees_count']=$this->Dashboard_model->allEmployeesCount();
		$data['departments']=$this->Dashboard_model->deptCount();
		$data['designations']=$this->Dashboard_model->designationCount();
		$data['esiEmployees']=$this->Dashboard_model->esiEmpCount();
		$data['pfEmpCount']=$this->Dashboard_model->pfEmpCount();
		$data['tdsEmpCount']=$this->Dashboard_model->tdsEmpCount();
		$data['bioCurrentDayCount']=$this->Dashboard_model->bioCurrentDayCount();
		$data['DeptCurrentDayCount']=$this->Dashboard_model->DeptCurrentDayCount();
		$data['MonthWiseSalaries']=$this->Dashboard_model->MonthWiseSalaries();
		$SalariesDeductions=$this->Dashboard_model->SalariesDeductions();
		$paymentModeSalaries=$this->Dashboard_model->PaymentModeSalaries();
		$data['SalariesDeductions']=$SalariesDeductions[0];
		$data['paymentModeSalaries']=$paymentModeSalaries[0];

		$this->load->view('theme/template',$data);
	}
	
}
